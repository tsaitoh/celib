# CELib
A software library for simulations of chemical evolution.

# Download CELib
You can clone this repository by using the following command:
```
> git clone git@bitbucket.org:tsaitoh/celib.git
```

# Compile CELib

CELib is written in C, in particular, C99. This software requires just a C
compiler which can compile C99 sources and there is no need to prepare other
libraries.

When you clone/download this library, just type make in the directory src:
```
> cd src
> make
```
Then you have "libCELib.a".


# How to compile your simulation code with CELib

See below.


# Sample program using CELib 

A sample program which is used for the code paper is included in this repository.
Please see the directory "test". 
This sample program requires PGPlot in order to make figures. If necessary,
please edit Makefile in the directory so that the include/lib directories for
PGPlot point correctly. 

By typing the make command, the sample program is compiled.
```
> cd test
> make
```

This program reads the parameter file, "param.txt", at the beginning of the run.

Makefile of this sample program would be a good example to compile your
simulation code with CELib.