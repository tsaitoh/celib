// CELib example program.
// Get AGB feedback results by CELib

#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "math.h"
#include "Astro.h"
#include "CELib.h"

#define CELibSolarMetallicity   (0.0134)


static void AGBFBPrimordial(void){
    
    double Mass = 100.0;  // Mass in units of Msun.
    double Metallicity = 0.e0; // Metallicity  
    int NEvent = 0; // NEvent-th AGB event
    double Elements[CELibYield_Number];
    
    CELibSetPrimordialMetallicity(Mass,Elements);

    struct CELibStructFeedbackOutput Output
        = CELibGetFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Metallicity,
                .MassConversionFactor = 1.0,
                .Elements = Elements,
                .Count = NEvent,
                },CELibFeedbackType_AGB);

    fprintf(stderr,"Primordial case\n");
    fprintf(stderr,"Energy %g [erg]\n",Output.Energy);
    fprintf(stderr,"Ejecta mass %g [Msun]\n",Output.EjectaMass);
    fprintf(stderr,"Remnant mass %g [Msun]\n",Output.RemnantMass);
    for(int i=0;i<CELibYield_Number;i++){
        char yname[MaxCharactersInLine];
        CELibGetYieldElementName(i,yname);
        // This value ``Output.Elements[i]'' is the released mass
        //   in units of Msun.
        fprintf(stderr,"%3s: %g Msun\n",yname,Output.Elements[i]); 
    }
    fprintf(stderr,"\n");

    return ;
}

static void AGBFBPrimordialUsingSimulationUnit(void){

    double Mass = 1.0;  // Mass in units of 100 Msun.
    double MassConversionFactor = 100.0; // Mass*MassConversionFactor makes 100 Msun in this case.
    double Metallicity = 0.e0; // Metallicity  
    int NEvent = 0; // NEvent-th AGB event
    double Elements[CELibYield_Number];
    
    CELibSetPrimordialMetallicity(Mass*MassConversionFactor,Elements);

    for(int i=0;i<CELibYield_Number;i++){
        Elements[i] /= MassConversionFactor;
    }

    struct CELibStructFeedbackOutput Output
        = CELibGetFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Metallicity,
                .MassConversionFactor = MassConversionFactor,
                .Elements = Elements,
                .Count = NEvent,
                },CELibFeedbackType_AGB);

    fprintf(stderr,"Solar abudance pattern with Z = %g case\n",Metallicity);
    fprintf(stderr,"Energy %g [erg]\n",Output.Energy);
    fprintf(stderr,"Ejecta mass %g [Msun]\n",Output.EjectaMass);
    fprintf(stderr,"Remnant mass %g [Msun]\n",Output.RemnantMass);
    for(int i=0;i<CELibYield_Number;i++){
        char yname[MaxCharactersInLine];
        CELibGetYieldElementName(i,yname);
        // This value ``Output.Elements[i]'' is the released mass
        //   in units of Msun.
        fprintf(stderr,"%3s: %g Msun\n",yname,Output.Elements[i]);
    }
    fprintf(stderr,"\n");

    return ;
}

static void AGBFBSolarMetallicity(void){
    
    double Mass = 100.0;  // Mass in units of Msun.
    double Metallicity = CELibSolarMetallicity; // Metallicity  
    int NEvent = 0; // NEvent-th AGB event
    double Elements[CELibYield_Number];
    
    CELibSetMetallicityWithSolarAbundancePattern(Mass,Elements,Metallicity);

    struct CELibStructFeedbackOutput Output
        = CELibGetFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Metallicity,
                .MassConversionFactor = 1.0,
                .Elements = Elements,
                .Count = NEvent,
                },CELibFeedbackType_AGB);

    fprintf(stderr,"Solar abudance pattern with Z = %g case\n",Metallicity);
    fprintf(stderr,"Energy %g [erg]\n",Output.Energy);
    fprintf(stderr,"Ejecta mass %g [Msun]\n",Output.EjectaMass);
    fprintf(stderr,"Remnant mass %g [Msun]\n",Output.RemnantMass);
    for(int i=0;i<CELibYield_Number;i++){
        char yname[MaxCharactersInLine];
        CELibGetYieldElementName(i,yname);
        // This value ``Output.Elements[i]'' is the released mass
        //   in units of Msun.
        fprintf(stderr,"%3s: %g Msun\n",yname,Output.Elements[i]); 
    }
    fprintf(stderr,"\n");

    return ;
}

static void AGBFBSolarMetallicityUsingSimulationUnit(void){

    double Mass = 1.0;  // Mass in units of 100 Msun.
    double MassConversionFactor = 100.0; // Mass*MassConversionFactor makes 100 Msun in this case.

    double Metallicity = CELibSolarMetallicity; // Metallicity  
    int NEvent = 0; // NEvent-th AGB event
    double Elements[CELibYield_Number];
    
    CELibSetMetallicityWithSolarAbundancePattern(Mass*MassConversionFactor,Elements,Metallicity);

    for(int i=0;i<CELibYield_Number;i++){
        Elements[i] /= MassConversionFactor;
    }

    struct CELibStructFeedbackOutput Output
        = CELibGetFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Metallicity,
                .MassConversionFactor = MassConversionFactor,
                .Elements = Elements,
                .Count = NEvent,
                },CELibFeedbackType_AGB);

    fprintf(stderr,"Solar abudance pattern with Z = %g case\n",Metallicity);
    fprintf(stderr,"Energy %g [erg]\n",Output.Energy);
    fprintf(stderr,"Ejecta mass %g [Msun]\n",Output.EjectaMass);
    fprintf(stderr,"Remnant mass %g [Msun]\n",Output.RemnantMass);
    for(int i=0;i<CELibYield_Number;i++){
        char yname[MaxCharactersInLine];
        CELibGetYieldElementName(i,yname);
        // This value ``Output.Elements[i]'' is the released mass
        //   in units of Msun.
        fprintf(stderr,"%3s: %g Msun\n",yname,Output.Elements[i]);
    }
    fprintf(stderr,"\n");

    return ;
}

static void AGBFBIncrimentNEvent(void){

    double Mass = 1.0;  // Mass in units of 100 Msun.
    double MassConversionFactor = 100.0; // Mass*MassConversionFactor makes 100 Msun in this case.

    double Metallicity = CELibSolarMetallicity; // Metallicity  
    double Elements[CELibYield_Number];
 
    for(int k=0;k<10;k++){
        CELibSetMetallicityWithSolarAbundancePattern(Mass*MassConversionFactor,Elements,Metallicity);

        for(int i=0;i<CELibYield_Number;i++){
            Elements[i] /= MassConversionFactor;
        }

        struct CELibStructFeedbackOutput Output
            = CELibGetFeedback((struct CELibStructFeedbackInput){
                    .Mass = Mass,
                    .Metallicity = Metallicity,
                    .MassConversionFactor = MassConversionFactor,
                    .Elements = Elements,
                    .Count = k,
                    },CELibFeedbackType_AGB);

        fprintf(stderr,"Solar abudance pattern with Z = %g case [NEvent = %d]\n",Metallicity,k);
        fprintf(stderr,"Energy %g [erg]\n",Output.Energy);
        fprintf(stderr,"Ejecta mass %g [Msun]\n",Output.EjectaMass);
        fprintf(stderr,"Remnant mass %g [Msun]\n",Output.RemnantMass);
        for(int i=0;i<CELibYield_Number;i++){
            char yname[MaxCharactersInLine];
            CELibGetYieldElementName(i,yname);
            // This value ``Output.Elements[i]'' is the released mass
            //   in units of Msun.
            fprintf(stderr,"%3s: %g Msun\n",yname,Output.Elements[i]);
        }
        fprintf(stderr,"\n");
    }

    return ;
}

int main(int argc, char **argv){

    CELibSetRunParameterIntegrationSteps(1000); // for test mode.
    CELibInit();

    AGBFBPrimordial();
    AGBFBPrimordialUsingSimulationUnit();
    AGBFBSolarMetallicity();
    AGBFBSolarMetallicityUsingSimulationUnit();

    AGBFBIncrimentNEvent();

    return EXIT_SUCCESS;
}
