// CELib example program.
// Get SNII feedback results under the star-by-star model

#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "math.h"
#include "Astro.h"
#include "CELib.h"

#define CELibSolarMetallicity   (0.0134)


static void SNeIIFBSbSPrimordial(void){
    
    double Mass = 40.0;  // Mass of a star in units of Msun.
    double Metallicity = 0.e0; // Metallicity 
    double Elements[CELibYield_Number];
    
    CELibSetPrimordialMetallicity(Mass,Elements);

    struct CELibStructFeedbackStarbyStarOutput Output
        = CELibGetFeedbackStarbyStar((struct CELibStructFeedbackStarbyStarInput){
                .Mass = Mass,
                .Metallicity = Metallicity,
                .MassConversionFactor = 1.0,
                .Elements = Elements,
                },CELibFeedbackType_SNII);

    fprintf(stderr,"Primordial case\n");
    fprintf(stderr,"Energy %g [erg]\n",Output.Energy);
    fprintf(stderr,"Ejecta mass %g [Msun]\n",Output.EjectaMass);
    fprintf(stderr,"Remnant mass %g [Msun]\n",Output.RemnantMass);
    for(int i=0;i<CELibYield_Number;i++){
        char yname[MaxCharactersInLine];
        CELibGetYieldElementName(i,yname);
        // This value ``Output.Elements[i]'' is the released mass
        //   in units of Msun.
        fprintf(stderr,"%3s: %g Msun\n",yname,Output.Elements[i]); 
    }
    fprintf(stderr,"\n");

    return ;
}

static void SNeIIFBSbSPrimordialUsingSimulationUnit(void){

    double Mass = 0.4;  // Mass of a star in units of 100 Msun.
    double MassConversionFactor = 100.0; // Mass*MassConversionFactor makes 100 Msun in this case.

    double Metallicity = 0.e0; // Metallicity  
    double Elements[CELibYield_Number];
    
    CELibSetPrimordialMetallicity(Mass*MassConversionFactor,Elements);

    for(int i=0;i<CELibYield_Number;i++){
        Elements[i] /= MassConversionFactor;
    }

    struct CELibStructFeedbackStarbyStarOutput Output
        = CELibGetFeedbackStarbyStar((struct CELibStructFeedbackStarbyStarInput){
                .Mass = Mass,
                .Metallicity = Metallicity,
                .MassConversionFactor = MassConversionFactor,
                .Elements = Elements,
                },CELibFeedbackType_SNII);

    fprintf(stderr,"Solar abudance pattern with Z = %g case\n",Metallicity);
    fprintf(stderr,"Energy %g [erg]\n",Output.Energy);
    fprintf(stderr,"Ejecta mass %g [Msun]\n",Output.EjectaMass);
    fprintf(stderr,"Remnant mass %g [Msun]\n",Output.RemnantMass);
    for(int i=0;i<CELibYield_Number;i++){
        char yname[MaxCharactersInLine];
        CELibGetYieldElementName(i,yname);
        // This value ``Output.Elements[i]'' is the released mass
        //   in units of Msun.
        fprintf(stderr,"%3s: %g Msun\n",yname,Output.Elements[i]);
    }
    fprintf(stderr,"\n");


    return ;
}

static void SNeIIFBSbSSolarMetallicity(void){
    
    double Mass = 40.0;  // Mass of a star in units of Msun.
    double Metallicity = CELibSolarMetallicity; // Metallicity  
    double Elements[CELibYield_Number];
    
    CELibSetMetallicityWithSolarAbundancePattern(Mass,Elements,Metallicity);

    struct CELibStructFeedbackStarbyStarOutput Output
        = CELibGetFeedbackStarbyStar((struct CELibStructFeedbackStarbyStarInput){
                .Mass = Mass,
                .Metallicity = Metallicity,
                .MassConversionFactor = 1.0,
                .Elements = Elements,
                },CELibFeedbackType_SNII);

    fprintf(stderr,"Solar abudance pattern with Z = %g case\n",Metallicity);
    fprintf(stderr,"Energy %g [erg]\n",Output.Energy);
    fprintf(stderr,"Ejecta mass %g [Msun]\n",Output.EjectaMass);
    fprintf(stderr,"Remnant mass %g [Msun]\n",Output.RemnantMass);
    for(int i=0;i<CELibYield_Number;i++){
        char yname[MaxCharactersInLine];
        CELibGetYieldElementName(i,yname);
        // This value ``Output.Elements[i]'' is the released mass
        //   in units of Msun.
        fprintf(stderr,"%3s: %g Msun\n",yname,Output.Elements[i]); 
    }
    fprintf(stderr,"\n");



    return ;
}

static void SNeIIFBSbSSolarMetallicityUsingSimulationUnit(void){

    double Mass = 0.4;  // Mass of a star in units of 100 Msun.
    double MassConversionFactor = 100.0; // Mass*MassConversionFactor makes 100 Msun in this case.

    double Metallicity = CELibSolarMetallicity; // Metallicity  
    double Elements[CELibYield_Number];
    
    CELibSetMetallicityWithSolarAbundancePattern(Mass*MassConversionFactor,Elements,Metallicity);

    for(int i=0;i<CELibYield_Number;i++){
        Elements[i] /= MassConversionFactor;
    }

    struct CELibStructFeedbackStarbyStarOutput Output
        = CELibGetFeedbackStarbyStar((struct CELibStructFeedbackStarbyStarInput){
                .Mass = Mass,
                .Metallicity = Metallicity,
                .MassConversionFactor = MassConversionFactor,
                .Elements = Elements,
                },CELibFeedbackType_SNII);

    fprintf(stderr,"Solar abudance pattern with Z = %g case\n",Metallicity);
    fprintf(stderr,"Energy %g [erg]\n",Output.Energy);
    fprintf(stderr,"Ejecta mass %g [Msun]\n",Output.EjectaMass);
    fprintf(stderr,"Remnant mass %g [Msun]\n",Output.RemnantMass);
    for(int i=0;i<CELibYield_Number;i++){
        char yname[MaxCharactersInLine];
        CELibGetYieldElementName(i,yname);
        // This value ``Output.Elements[i]'' is the released mass
        //   in units of Msun.
        fprintf(stderr,"%3s: %g Msun\n",yname,Output.Elements[i]);
    }
    fprintf(stderr,"\n");


    return ;
}

int main(int argc, char **argv){

    CELibSetRunParameterIntegrationSteps(1000); // for test mode.
    CELibInit();


    SNeIIFBSbSPrimordial();
    SNeIIFBSbSPrimordialUsingSimulationUnit();

    SNeIIFBSbSSolarMetallicity();
    SNeIIFBSbSSolarMetallicityUsingSimulationUnit();




    return EXIT_SUCCESS;
}
