// CELib example program.
// Get SNIa feedback results by CELib

#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "math.h"
#include "Astro.h"
#include "CELib.h"

#define CELibSolarMetallicity   (0.0134)

static void AskSNIaNAssociation(void){
    
    int Nc = CELibGetRunParameterSNIaNassociation();
    fprintf(stderr,"SNIa Nassociation = %d\n",Nc);

    return ;
}

static void SNeIaFB(void){
    
    double Mass = 100.0;  // Mass in units of Msun.
    double Metallicity = CELibSolarMetallicity; // Metallicity  
    double Elements[CELibYield_Number];
    
    CELibSetMetallicityWithSolarAbundancePattern(Mass,Elements,Metallicity);

    struct CELibStructFeedbackOutput Output
        = CELibGetFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Metallicity,
                .MassConversionFactor = 1.0,
                .Elements = Elements,
                .Count = 0,
                },CELibFeedbackType_SNIa);

    fprintf(stderr,"Solar abudance pattern with Z = %g case\n",Metallicity);
    fprintf(stderr,"Energy %g [erg]\n",Output.Energy);
    fprintf(stderr,"Ejecta mass %g [Msun]\n",Output.EjectaMass);
    fprintf(stderr,"Remnant mass %g [Msun]\n",Output.RemnantMass);
    for(int i=0;i<CELibYield_Number;i++){
        char yname[MaxCharactersInLine];
        CELibGetYieldElementName(i,yname);
        // This value ``Output.Elements[i]'' is the released mass
        //   in units of Msun.
        fprintf(stderr,"%3s: %g Msun\n",yname,Output.Elements[i]); 
    }
    fprintf(stderr,"\n");


    return ;
}

static void SNeIaFBUsingSimulationUnit(void){

    double Mass = 1.0;  // Mass in units of 100 Msun.
    double MassConversionFactor = 100.0; // Mass*MassConversionFactor makes 100 Msun in this case.

    double Metallicity = CELibSolarMetallicity; // Metallicity  
    double Elements[CELibYield_Number];
    
    CELibSetMetallicityWithSolarAbundancePattern(Mass*MassConversionFactor,Elements,Metallicity);

    for(int i=0;i<CELibYield_Number;i++){
        Elements[i] /= MassConversionFactor;
    }

    struct CELibStructFeedbackOutput Output
        = CELibGetFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Metallicity,
                .MassConversionFactor = MassConversionFactor,
                .Elements = Elements,
                .Count = 0, 
                },CELibFeedbackType_SNIa);

    fprintf(stderr,"Solar abudance pattern with Z = %g case\n",Metallicity);
    fprintf(stderr,"Energy %g [erg]\n",Output.Energy);
    fprintf(stderr,"Ejecta mass %g [Msun]\n",Output.EjectaMass);
    fprintf(stderr,"Remnant mass %g [Msun]\n",Output.RemnantMass);
    for(int i=0;i<CELibYield_Number;i++){
        char yname[MaxCharactersInLine];
        CELibGetYieldElementName(i,yname);
        // This value ``Output.Elements[i]'' is the released mass
        //   in units of Msun.
        fprintf(stderr,"%3s: %g Msun\n",yname,Output.Elements[i]);
    }
    fprintf(stderr,"\n");


    return ;
}

int main(int argc, char **argv){

    CELibSetRunParameterIntegrationSteps(1000); // for test mode.
    CELibInit();

    AskSNIaNAssociation();

    SNeIaFB();
    SNeIaFBUsingSimulationUnit();

    return EXIT_SUCCESS;
}
