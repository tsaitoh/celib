// CELib example program.
// Get event time by CELib

#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "math.h"
#include "CELib.h"

#define CELibSolarMetallicity   (0.0134)

static void GetEventTimeSNII(void){

    double R = 0.0;
    double Z = 0.e0;
    double EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = 1000,
                    .Metallicity = Z,
                    // .Count = 0, // Type II SNe do not use this member.
                    },CELibFeedbackType_SNII);

    fprintf(stderr,"SNII Event time for (R=%g,Z=%g) = %g [year]\n",R,Z,EventTime);

    R = 1.0;
    Z = 0.e0;
    EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = 1000,
                    .Metallicity = Z,
                    // .Count = 0, // Type II SNe do not use this member.
                    },CELibFeedbackType_SNII);

    fprintf(stderr,"SNII Event time for (R=%g,Z=%g) = %g [year]\n",R,Z,EventTime);


    R = 0.0;
    Z = CELibSolarMetallicity;
    EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = 1000,
                    .Metallicity = Z,
                    // .Count = 0, // Type II SNe do not use this member.
                    },CELibFeedbackType_SNII);

    fprintf(stderr,"SNII Event time for (R=%g,Z=%g) = %g [year]\n",R,Z,EventTime);

    R = 1.0;
    Z = CELibSolarMetallicity;
    EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = 1000,
                    .Metallicity = Z,
                    // .Count = 0, // Type II SNe do not use this member.
                    },CELibFeedbackType_SNII);

    fprintf(stderr,"SNII Event time for (R=%g,Z=%g) = %g [year]\n",R,Z,EventTime);

    return ;
}

static void GetEventTimeSNIa(void){

    double R = 0.0;
    double Z = 0.e0;
    double Mass = 1.e5;
    int Count = 0;
    double EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = Mass,
                    .Metallicity = Z,
                    .Count = Count, 
                    },CELibFeedbackType_SNIa);

    fprintf(stderr,"SNIa Event time for (R=%g,Z=%g,Count=%d) = %g [year]\n",R,Z,Count,EventTime);

    R = 1.0;
    Z = 0.e0;
    Count = 0;
    EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = Mass,
                    .Metallicity = Z,
                    .Count = Count, 
                    },CELibFeedbackType_SNIa);

    fprintf(stderr,"SNIa Event time for (R=%g,Z=%g,Count=%d) = %g [year]\n",R,Z,Count,EventTime);

    R = 0.0;
    Z = 0.e0;
    Count = 10;
    EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = Mass,
                    .Metallicity = Z,
                    .Count = Count, 
                    },CELibFeedbackType_SNII);

    fprintf(stderr,"SNIa Event time for (R=%g,Z=%g,Count=%d) = %g [year]\n",R,Z,Count,EventTime);

    R = 1.0;
    Z = 0.e0;
    Count = 10;
    EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = Mass,
                    .Metallicity = Z,
                    .Count = Count, 
                    },CELibFeedbackType_SNIa);

    fprintf(stderr,"SNIa Event time for (R=%g,Z=%g,Count=%d) = %g [year]\n",R,Z,Count,EventTime);


    return ;
}

static void GetEventTimeAGB(void){

    double R = 0.0;
    double Z = 0.e0;
    double Mass = 1.e5;
    int Count = 0;
    double EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = Mass,
                    .Metallicity = Z,
                    .Count = Count, 
                    },CELibFeedbackType_AGB);

    fprintf(stderr,"AGB Event time for (R=%g,Z=%g,Count=%d) = %g [year]\n",R,Z,Count,EventTime);

    R = 1.0;
    Z = 0.e0;
    Mass = 1.e5;
    Count = 0;
    EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = Mass,
                    .Metallicity = Z,
                    .Count = Count, 
                    },CELibFeedbackType_AGB);

    fprintf(stderr,"AGB Event time for (R=%g,Z=%g,Count=%d) = %g [year]\n",R,Z,Count,EventTime);

    R = 0.0;
    Z = 0.e0;
    Mass = 1.e5;
    Count = 10;
    EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = Mass,
                    .Metallicity = Z,
                    .Count = Count, 
                    },CELibFeedbackType_AGB);

    fprintf(stderr,"AGB Event time for (R=%g,Z=%g,Count=%d) = %g [year]\n",R,Z,Count,EventTime);

    R = 1.0;
    Z = 0.e0;
    Mass = 1.e5;
    Count = 10;
    EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = Mass,
                    .Metallicity = Z,
                    .Count = Count, 
                    },CELibFeedbackType_AGB);

    fprintf(stderr,"AGB Event time for (R=%g,Z=%g,Count=%d) = %g [year]\n",R,Z,Count,EventTime);


    return ;
}


static void GetEventTimeNSM(void){

    double R = 0.0;
    double Mass = 1.e6;
    int Count = 0;
    double EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = Mass,
                    .Count = Count, 
                    },CELibFeedbackType_NSM);

    fprintf(stderr,"NSM Event time for (R=%g,Count=%d) = %g [year]\n",R,Count,EventTime);

    R = 1.0;
    Mass = 1.e6;
    Count = 0;
    EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = Mass,
                    .Count = Count, 
                    },CELibFeedbackType_NSM);

    fprintf(stderr,"NSM Event time for (R=%g,Count=%d) = %g [year]\n",R,Count,EventTime);

    R = 0.0;
    Mass = 1.e6;
    Count = 1;
    EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = Mass,
                    .Count = Count, 
                    },CELibFeedbackType_NSM);

    fprintf(stderr,"NSM Event time for (R=%g,Count=%d) = %g [year]\n",R,Count,EventTime);

    R = 1.0;
    Mass = 1.e6;
    Count = 1;
    EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = Mass,
                    .Count = Count, 
                    },CELibFeedbackType_NSM);

    fprintf(stderr,"NSM Event time for (R=%g,Count=%d) = %g [year]\n",R,Count,EventTime);

    R = 0.0;
    Mass = 1.e6;
    Count = 5;
    EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = Mass,
                    .Count = Count, 
                    },CELibFeedbackType_NSM);

    fprintf(stderr,"NSM Event time for (R=%g,Count=%d) = %g [year]\n",R,Count,EventTime);

    R = 1.0;
    Mass = 1.e6;
    Count = 5;
    EventTime = CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = R,
                    .InitialMass_in_Msun = Mass,
                    .Count = Count, 
                    },CELibFeedbackType_NSM);

    fprintf(stderr,"NSM Event time for (R=%g,Count=%d) = %g [year]\n",R,Count,EventTime);

    return ;
}


int main(int argc, char **argv){


    CELibSetRunParameterIntegrationSteps(1000); // for test mode.
    CELibInit();
    GetEventTimeSNII();
    GetEventTimeSNIa();
    GetEventTimeAGB();
    GetEventTimeNSM();

    return EXIT_SUCCESS;
}
