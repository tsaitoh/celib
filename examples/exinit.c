// CELib example program.
// Initialize CELib

#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "math.h"
#include "CELib.h"

int main(int argc, char **argv){

    CELibInit();

    return EXIT_SUCCESS;
}
