// CELib example program.
// Initialize CELib with various parameters.

#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "math.h"
#include "CELib.h"

static void UseFastMode(void){
    // Decrease integration step.
    // If the value is too small, the results will be unstable.
    // Note that the default value is 10000.
    CELibSetRunParameterIntegrationSteps(3000);
    CELibInit();
    CELibShowCurrentRunParameters();
}

static void TurnOffTestMode(void){
    // Turn off the varbose mode.
    CELibSetRunParameterTestMode(false);
    CELibInit();
    CELibShowCurrentRunParameters();
}

static void TurnOnTestMode(void){
    // Turn on the varbose mode.
    CELibSetRunParameterTestMode(true);
    CELibInit();
    CELibShowCurrentRunParameters();
}

static void ChangeIMF(void){

    CELibSetRunParameterIMFType(CELibIMF_Salpeter);
    CELibInit();
    CELibShowCurrentRunParameters();

    CELibSetRunParameterIMFType(CELibIMF_Kroupa1993);
    CELibInit();
    CELibShowCurrentRunParameters();

    CELibSetRunParameterIMFType(CELibIMF_Chabrier);
    CELibInit();
    CELibShowCurrentRunParameters();
}

static void ChangeSNIIRunParameters(void){

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
    CELibInit();
    CELibShowCurrentRunParameters();


    CELibSetRunParameterSNIIHyperNovaFraction(0.0);
    CELibInit();
    CELibShowCurrentRunParameters();

    CELibSetRunParameterSNIIHyperNovaFraction(0.5);
    CELibInit();
    CELibShowCurrentRunParameters();


    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_P98);
    CELibInit();
    CELibShowCurrentRunParameters();

    CELibSetRunParameterSNIIYieldsModificationP98(0); //Off
    CELibInit();
    CELibShowCurrentRunParameters();

    return ;
}


int main(int argc, char **argv){

    // Save current run parameters
    CELibSaveRunParameter();

    UseFastMode();
    TurnOffTestMode();
    TurnOnTestMode();
    ChangeIMF();
    ChangeSNIIRunParameters();


    // Load run parameters
    CELibLoadRunParameter();

    return EXIT_SUCCESS;
}
