// CELib example program.
// Set initial metallicity

#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "math.h"
#include "Astro.h"
#include "CELib.h"

static void AskAbundancePatternType(void){
    
    int Type = CELibGetSolarAbundancePatternType();
    char name[MaxCharactersInLine];

    if(Type == CELibSolarAbundancePattern_A09){
        Snprintf(name,"%s","CELibSolarAbundancePattern_A09");
    } else if(Type == CELibSolarAbundancePattern_GS98){
        Snprintf(name,"%s","CELibSolarAbundancePattern_GS98");
    } else if(Type == CELibSolarAbundancePattern_AG89){
        Snprintf(name,"%s","CELibSolarAbundancePattern_AG89");
    }
    fprintf(stderr,"Current solar abundance pattern is %s\n",name);

    return ;
}

static void SetPrimordialMetallicity(void){

    double Mass = 100.0; // Mass of an SSP particle in units of Msun
    double Elements[CELibYield_Number]; // Array to store data
    CELibSetPrimordialMetallicity(Mass,Elements);


    fprintf(stderr,"Primordial case\n");
    for(int i=0;i<CELibYield_Number;i++){
        char yname[MaxCharactersInLine];
        CELibGetYieldElementName(i,yname);
        fprintf(stderr,"%3s: %g Msun\n",yname,Elements[i]);
    }
    fprintf(stderr,"\n");

    return ;
}

static void SetSolarMetallicity(void){

    double Mass = 100.0; // Mass of an SSP particle in units of Msun
    double Elements[CELibYield_Number]; // Array to store data
    CELibSetSolarMetallicity(Mass,Elements);


    fprintf(stderr,"Solar metallicity case\n");
    for(int i=0;i<CELibYield_Number;i++){
        char yname[MaxCharactersInLine];
        CELibGetYieldElementName(i,yname);
        fprintf(stderr,"%3s: %g Msun\n",yname,Elements[i]);
    }
    fprintf(stderr,"\n");

    return ;
}

static void SetMetallicityWithSolarAbudancePattern(void){

    double Mass = 100.0; // Mass of an SSP particle in units of Msun
    double Elements[CELibYield_Number]; // Array to store data
    double Metallicity = 0.001; // Z = 0.001
    CELibSetMetallicityWithSolarAbundancePattern(Mass,Elements,Metallicity);


    fprintf(stderr,"Solar abudance pattern with Z = %g case\n",Metallicity);
    for(int i=0;i<CELibYield_Number;i++){
        char yname[MaxCharactersInLine];
        CELibGetYieldElementName(i,yname);
        fprintf(stderr,"%3s: %g Msun\n",yname,Elements[i]);
    }
    fprintf(stderr,"\n");

    return ;
}

int main(int argc, char **argv){

    CELibSetRunParameterIntegrationSteps(1000);
    CELibInit();

    AskAbundancePatternType();
    SetPrimordialMetallicity();
    SetSolarMetallicity();
    SetMetallicityWithSolarAbudancePattern();

    return EXIT_SUCCESS;
}
