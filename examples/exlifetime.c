// CELib example program.
// How to use lifetime functions in CELib

#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "math.h"
#include "CELib.h"

#define CELibSolarMetallicity   (0.0134)

static void TestStellarLifeTime(void){

    double Mass = 100;
    double Z = CELibSolarMetallicity;
    double Time = CELibGetLifeTimeofStar(Mass,Z);
    fprintf(stderr,"Lifetime of a star whose mass is %g Msun and Z = %g is %g [year]\n",
            Mass,Z,Time);

    Mass = 10;
    Z = CELibSolarMetallicity;
    Time = CELibGetLifeTimeofStar(Mass,Z);
    fprintf(stderr,"Lifetime of a star whose mass is %g Msun and Z = %g is %g [year]\n",
            Mass,Z,Time);

    Mass = 1;
    Z = CELibSolarMetallicity;
    Time = CELibGetLifeTimeofStar(Mass,Z);
    fprintf(stderr,"Lifetime of a star whose mass is %g Msun and Z = %g is %g [year]\n",
            Mass,Z,Time);

    Mass = 300;
    Z = 0;
    Time = CELibGetLifeTimeofStar(Mass,Z);
    fprintf(stderr,"Lifetime of a star whose mass is %g Msun and Z = %g is %g [year]\n",
            Mass,Z,Time);

    Mass = 100;
    Z = 0;
    Time = CELibGetLifeTimeofStar(Mass,Z);
    fprintf(stderr,"Lifetime of a star whose mass is %g Msun and Z = %g is %g [year]\n",
            Mass,Z,Time);

    Mass = 10;
    Z = 0;
    Time = CELibGetLifeTimeofStar(Mass,Z);
    fprintf(stderr,"Lifetime of a star whose mass is %g Msun and Z = %g is %g [year]\n",
            Mass,Z,Time);

    Mass = 1;
    Z = 0;
    Time = CELibGetLifeTimeofStar(Mass,Z);
    fprintf(stderr,"Lifetime of a star whose mass is %g Msun and Z = %g is %g [year]\n",
            Mass,Z,Time);

    // Out of range cases.
    // Thanks to fitting functions, CELib can answer ages even at the out-of-range.
    // But these are incorrect.
    // If the table interpolation data is used, CELib returns upper/lower value
    // in the case of the out-of-range.
    Mass = 1000;
    Z = 0;
    Time = CELibGetLifeTimeofStar(Mass,Z);
    fprintf(stderr,"Lifetime of a star whose mass is %g Msun and Z = %g is %g [year]\n",
            Mass,Z,Time);

    Mass = 0.01;
    Z = 0;
    Time = CELibGetLifeTimeofStar(Mass,Z);
    fprintf(stderr,"Lifetime of a star whose mass is %g Msun and Z = %g is %g [year]\n",
            Mass,Z,Time);


    return ;
}

static void TestDyingStellarMass(void){

    double LifeTime = 3.e6;
    double Z = CELibSolarMetallicity;
    double Mass = CELibGetDyingStellarMass(LifeTime,Z);
    fprintf(stderr,"Mass whose lifetime is %g [year] and Z = %g is %g [Msun]\n",
            LifeTime,Z,Mass);

    LifeTime = 1.e7;
    Z = CELibSolarMetallicity;
    Mass = CELibGetDyingStellarMass(LifeTime,Z);
    fprintf(stderr,"Mass whose lifetime is %g [year] and Z = %g is %g [Msun]\n",
            LifeTime,Z,Mass);

    LifeTime = 1.e8;
    Z = CELibSolarMetallicity;
    Mass = CELibGetDyingStellarMass(LifeTime,Z);
    fprintf(stderr,"Mass whose lifetime is %g [year] and Z = %g is %g [Msun]\n",
            LifeTime,Z,Mass);

    LifeTime = 1.e9;
    Z = CELibSolarMetallicity;
    Mass = CELibGetDyingStellarMass(LifeTime,Z);
    fprintf(stderr,"Mass whose lifetime is %g [year] and Z = %g is %g [Msun]\n",
            LifeTime,Z,Mass);

    LifeTime = 1.e10;
    Z = CELibSolarMetallicity;
    Mass = CELibGetDyingStellarMass(LifeTime,Z);
    fprintf(stderr,"Mass whose lifetime is %g [year] and Z = %g is %g [Msun]\n",
            LifeTime,Z,Mass);

    LifeTime = 5.e10;
    Z = CELibSolarMetallicity;
    Mass = CELibGetDyingStellarMass(LifeTime,Z);
    fprintf(stderr,"Mass whose lifetime is %g [year] and Z = %g is %g [Msun]\n",
            LifeTime,Z,Mass);


    LifeTime = 2.25e6;
    Z = 0;
    Mass = CELibGetDyingStellarMass(LifeTime,Z);
    fprintf(stderr,"Mass whose lifetime is %g [year] and Z = %g is %g [Msun]\n",
            LifeTime,Z,Mass);

    LifeTime = 1.e8;
    Z = 0;
    Mass = CELibGetDyingStellarMass(LifeTime,Z);
    fprintf(stderr,"Mass whose lifetime is %g [year] and Z = %g is %g [Msun]\n",
            LifeTime,Z,Mass);

    LifeTime = 1.e9;
    Z = 0;
    Mass = CELibGetDyingStellarMass(LifeTime,Z);
    fprintf(stderr,"Mass whose lifetime is %g [year] and Z = %g is %g [Msun]\n",
            LifeTime,Z,Mass);

    LifeTime = 5.e10;
    Z = 0;
    Mass = CELibGetDyingStellarMass(LifeTime,Z);
    fprintf(stderr,"Mass whose lifetime is %g [year] and Z = %g is %g [Msun]\n",
            LifeTime,Z,Mass);

    // Out of range cases.
    // Thanks to fitting functions, CELib can answer the stellar mass even at the out-of-range.
    // But these are incorrect.
    // If the table interpolation data is used, CELib returns -1 in the case of
    // the out-of-range.
    LifeTime = 1.e4;
    Z = 0;
    Mass = CELibGetDyingStellarMass(LifeTime,Z);
    fprintf(stderr,"Mass whose lifetime is %g [year] and Z = %g is %g [Msun]\n",
            LifeTime,Z,Mass);

    LifeTime = 1.e12;
    Z = 0;
    Mass = CELibGetDyingStellarMass(LifeTime,Z);
    fprintf(stderr,"Mass whose lifetime is %g [year] and Z = %g is %g [Msun]\n",
            LifeTime,Z,Mass);

    return ;
}


int main(int argc, char **argv){

    // Reduce computational time.
    // I do not recommend using this value for the product run.
    CELibSetRunParameterIntegrationSteps(1000);  
    // Explicitly set the default value.
    CELibSetRunParameterLifeTimeType(CELibLifeTime_LSF); 
    CELibInit();

    TestStellarLifeTime();
    TestDyingStellarMass();

    CELibSetRunParameterLifeTimeType(CELibLifeTime_Original);
    CELibInit();
    TestStellarLifeTime();
    TestDyingStellarMass();


    return EXIT_SUCCESS;
}
