#include "config.h"
#include "ReadParameters.h"
#include "RandomNumberGenerator.h"
#include "PGPlotOperations.h"
#include "../src/SNIIYields.h"
#include <cpgplot.h>

struct StructOneZoneData{ // Data for each time step.
    double Time; //!< Current time
    double Mgas; // Current gas mass.
    double Mstar; // Current stellar mass.
    double MstarBorn; // Stellar mass born in this step.
    double Mejecta;   // Ejecta mass in this step.
    double MejectaSNII;   // Ejecta mass in this step.
    double MejectaSNIa;   // Ejecta mass in this step.
    double MejectaAGB;   // Ejecta mass in this step.
    double MejectaNSM;   // Ejecta mass in this step.
    double MejectaTotal;      // Total ejecta mass.
    double MejectaSNIITotal;  // Total ejecta mass.
    double MejectaSNIaTotal;  // Total ejecta mass.
    double MejectaAGBTotal;   // Total ejecta mass.
    double MejectaNSMTotal;   // Total ejecta mass.

    double Energy;       // Released energy in this step.
    double EnergySNII;   // Released energy in this step.
    double EnergySNIa;   // Released energy in this step.
    double EnergyAGB;    // Released energy in this step.
    double EnergyNSM;    // Released energy in this step.
    double EnergyTotal;      // Total released energy.
    double EnergySNIITotal;  // Total released energy.
    double EnergySNIaTotal;  // Total released energy.
    double EnergyAGBTotal;   // Total released energy.
    double EnergyNSMTotal;   // Total released energy.

    double Metallicity; 
    double Elements[CELibYield_Number]; // Elements in gas phase
    double StellarElements[CELibYield_Number]; // ELements in stars
    double EjectaElements[CELibYield_Number];  // Elements in Ejecta 
    double SFR; // Msun/year
    double ExplosionTimeSNII;
    double ExplosionTimeSNIa;
    double ExplosionTimeAGB;
    double ExplosionTimeNSM;
    int CountSNII;
    int CountSNIa;
    int CountAGB;
    int CountNSM;
    double NSNII;
    double NSNIa;
    double NAGB;
    double NNSM;
} *OneZoneData;

static void PlotMassEvolutionTwo(const int CL, const int LS);

/*!
 * Initialize zone.
 */
static void InitOneZoneData(void){

    OneZoneData = malloc(sizeof(struct StructOneZoneData)*ThisRun.OZTimeBin);
    memset(OneZoneData,0,sizeof(struct StructOneZoneData)*ThisRun.OZTimeBin);

    OneZoneData[0].Time = 0.e0;
    OneZoneData[0].Mgas = ThisRun.OZMgas;
    OneZoneData[0].Mstar = 0.e0;
    OneZoneData[0].Metallicity = 0.e0;

    OneZoneData[0].CountSNII = 0;
    OneZoneData[0].CountSNIa = 0;
    OneZoneData[0].CountAGB = 0;
    OneZoneData[0].CountNSM = 0;

    OneZoneData[0].NSNII = 0.0;
    OneZoneData[0].NSNIa = 0.0;
    OneZoneData[0].NAGB = 0.0;
    OneZoneData[0].NNSM = 0.0;

    OneZoneData[0].Mejecta = 0.e0;
    OneZoneData[0].MejectaSNII = 0.e0;
    OneZoneData[0].MejectaSNIa = 0.e0;
    OneZoneData[0].MejectaAGB = 0.e0;
    OneZoneData[0].MejectaNSM = 0.e0;

    OneZoneData[0].EnergySNII = 0.e0;
    OneZoneData[0].EnergySNIa = 0.e0;
    OneZoneData[0].EnergyAGB = 0.e0;
    
    OneZoneData[0].EnergySNIITotal = 0.e0;
    OneZoneData[0].EnergySNIaTotal = 0.e0;
    OneZoneData[0].EnergyAGBTotal = 0.e0;

    OneZoneData[0].MejectaTotal = 0.e0;
    OneZoneData[0].MejectaSNIITotal = 0.e0;
    OneZoneData[0].MejectaSNIaTotal = 0.e0;
    OneZoneData[0].MejectaAGBTotal = 0.e0;
    OneZoneData[0].MejectaNSMTotal = 0.e0;

    OneZoneData[0].Energy = 0.e0;
    OneZoneData[0].EnergySNII = 0.e0;
    OneZoneData[0].EnergySNIa = 0.e0;
    OneZoneData[0].EnergyAGB = 0.e0;
    OneZoneData[0].EnergyNSM = 0.e0;

    OneZoneData[0].EnergyTotal = 0.e0;
    OneZoneData[0].EnergySNIITotal = 0.e0;
    OneZoneData[0].EnergySNIaTotal = 0.e0;
    OneZoneData[0].EnergyAGBTotal = 0.e0;
    OneZoneData[0].EnergyNSMTotal = 0.e0;


    CELibSetPrimordialMetallicity(OneZoneData[0].Mgas,OneZoneData[0].Elements);

    for(int i=0;i<CELibYield_Number;i++)
        OneZoneData[0].EjectaElements[i] = 0.e0;

    return ;
}

/*!
 * Release zone.
 */
static void ReleaseOneZoneData(void){
    free(OneZoneData);
    return ;
}

/*!
 * Calc normalization of SFR.
 */
static void InitOneZoneSFR(void){

    double dt = ThisRun.OZTime/ThisRun.OZTimeBin;
    if(ThisRun.OZSFRType == 0){
        double SFR =  ThisRun.OZFinalMstar/ThisRun.OZTime;
        for(int i=0;i<ThisRun.OZTimeBin;i++){
            OneZoneData[i].SFR = SFR;
        }
    } else if(ThisRun.OZSFRType == 1){
        double Norm = ThisRun.OZFinalMstar/
            (ThisRun.OZExpScale*(1.0-exp(-ThisRun.OZTime/ThisRun.OZExpScale)));
        gprint(Norm);
        for(int i=0;i<ThisRun.OZTimeBin;i++){
            double time = dt*i;
            OneZoneData[i].SFR = Norm*exp(-time/ThisRun.OZExpScale);
        }
    } else if(ThisRun.OZSFRType == 2){
    }


    FILE *fp;
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/OZ_sfr.dat",ThisRun.OutDir);
    FileOpen(fp,fname,"w");
    fprintf(fp,"#SFRType = %d\n",ThisRun.OZSFRType);

    double Ms = 0.e0; 
    for(int i=0;i<ThisRun.OZTimeBin;i++){
        Ms += dt*OneZoneData[i].SFR;
        fprintf(fp,"%g %g %g\n",dt*i,OneZoneData[i].SFR,Ms);
    }
    fclose(fp);

    return ;
}

/*!
 * This function calls just after a star particle forms and a Type II or Ia
 * SN explosion takes place in order to compute the next explosion time.
 */
static double StellarFeedbackGetNextExplosionTime(const int mode, const double Metallicity, const double InitialMass_in_Msun, const int Count){

    double R = gsl_rng_uniform(RandomGenerator);
    if(mode == CELibFeedbackType_SNII){
        double Time = CELibGetSNIIExplosionTime(R,Metallicity);
        if(Time < 0.0){
            return 0.e0;
        } else { 
            return Time;
        }
    } else if(mode == CELibFeedbackType_SNIa){
        double Time = CELibGetSNIaExplosionTime(R,Metallicity,InitialMass_in_Msun,Count);   
        return Time;
    } else if(mode == CELibFeedbackType_AGB){
        double Time = CELibGetAGBFeedbackTime(R,Metallicity,Count);   
        return Time;
    } else if(mode == CELibFeedbackType_NSM){
        double Time = CELibGetNSMFeedbackTime(R,Metallicity,InitialMass_in_Msun,Count);   
        return Time;
    }
    return 0.e0;
}

/*!
 * Add released metals.
 */
static void AddElements(double src[], double dest[]){

    dest[CELibYield_H]  += src[CELibYield_H];
    dest[CELibYield_He] += src[CELibYield_He];
    dest[CELibYield_C]  += src[CELibYield_C];
    dest[CELibYield_N]  += src[CELibYield_N];
    dest[CELibYield_O]  += src[CELibYield_O];
    dest[CELibYield_Ne] += src[CELibYield_Ne];
    dest[CELibYield_Mg] += src[CELibYield_Mg];
    dest[CELibYield_Si] += src[CELibYield_Si];
    dest[CELibYield_S]  += src[CELibYield_S];
    dest[CELibYield_Ca] += src[CELibYield_Ca];
    dest[CELibYield_Fe] += src[CELibYield_Fe];
    dest[CELibYield_Ni] += src[CELibYield_Ni];
    dest[CELibYield_Eu] += src[CELibYield_Eu];

    return ;
}

static void UpdateSSP_i(const int Index, double Tnow, const int CurrentIndex){

    double EjectaElements[CELibYield_Number];
    memset(EjectaElements,0,sizeof(double)*CELibYield_Number);
    
    double EjectaMassSNII = 0.e0;
    double EnergySNII = 0.e0;
    if((ThisRun.OZUseSNII)&&(OneZoneData[Index].CountSNII == 0)&&(OneZoneData[Index].ExplosionTimeSNII<Tnow)&&(OneZoneData[Index].MstarBorn>0.e0)){
        struct CELibStructFeedbackOutput SNII = 
            CELibGetFeedback((struct CELibStructFeedbackInput){
                    .Mass = OneZoneData[Index].MstarBorn,
                    .Metallicity = OneZoneData[Index].Metallicity,
                    .MassConversionFactor = 1.0,
                    .Elements = OneZoneData[Index].StellarElements,
                    }, CELibFeedbackType_SNII);
            /*
            CELibGetSNIIFeedback((struct CELibStructFeedbackInput){
                .Mass = OneZoneData[Index].MstarBorn,
                .Metallicity = OneZoneData[Index].Metallicity,
                .MassConversionFactor = 1.0,
                .Elements = OneZoneData[Index].StellarElements,
                });
                */

        EjectaMassSNII += SNII.EjectaMass;
        EnergySNII += SNII.Energy;

        OneZoneData[Index].CountSNII ++; 
        OneZoneData[Index].NSNII += SNII.Energy/CELibRunParameters.SNIIEnergy; 

        AddElements(SNII.Elements, EjectaElements);

        OneZoneData[Index].ExplosionTimeSNIa = OneZoneData[Index].Time+
            CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = gsl_rng_uniform(RandomGenerator),
                    .InitialMass_in_Msun = OneZoneData[Index].MstarBorn,
                    .Metallicity = OneZoneData[Index].Metallicity,
                    .Count = OneZoneData[Index].CountSNIa,
                    },CELibFeedbackType_SNIa);
            /*
            StellarFeedbackGetNextExplosionTime(CELibFeedbackType_SNIa,
                    OneZoneData[Index].Metallicity,OneZoneData[Index].MstarBorn,OneZoneData[Index].CountSNIa);
                    */

        OneZoneData[Index].ExplosionTimeAGB = OneZoneData[Index].Time+
            CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = gsl_rng_uniform(RandomGenerator),
                    .InitialMass_in_Msun = OneZoneData[Index].MstarBorn,
                    .Metallicity = OneZoneData[Index].Metallicity,
                    .Count = OneZoneData[Index].CountAGB,
                    },CELibFeedbackType_AGB);

            /*
            StellarFeedbackGetNextExplosionTime(CELibFeedbackType_AGB,
                    OneZoneData[Index].Metallicity,OneZoneData[Index].MstarBorn,OneZoneData[Index].CountAGB);
                    */

        OneZoneData[Index].ExplosionTimeNSM = OneZoneData[Index].Time+
            CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                    .R = gsl_rng_uniform(RandomGenerator),
                    .InitialMass_in_Msun = OneZoneData[Index].MstarBorn,
                    .Metallicity = OneZoneData[Index].Metallicity,
                    .Count = OneZoneData[Index].CountNSM,
                    },CELibFeedbackType_NSM);
            /*
            StellarFeedbackGetNextExplosionTime(CELibFeedbackType_NSM,
                    OneZoneData[Index].Metallicity,OneZoneData[Index].MstarBorn,OneZoneData[Index].CountNSM);
                    */
    }

    OneZoneData[CurrentIndex].MejectaSNII += EjectaMassSNII;
    OneZoneData[CurrentIndex].EnergySNII += EnergySNII;

    double EjectaMassSNIa = 0.e0;
    double EnergySNIa = 0.e0;

    if((ThisRun.OZUseSNIa)&&(OneZoneData[Index].CountSNII > 0)){
        if(OneZoneData[Index].ExplosionTimeSNIa<Tnow){
            struct CELibStructFeedbackOutput SNIa = 
                CELibGetFeedback((struct CELibStructFeedbackInput){
                        .Mass = OneZoneData[Index].MstarBorn,
                        .Metallicity = OneZoneData[Index].Metallicity,
                        .MassConversionFactor = 1.0,
                        }, CELibFeedbackType_SNIa);
                /*
                CELibGetSNIaFeedback((struct CELibStructFeedbackInput){
                    .Mass = OneZoneData[Index].MstarBorn,
                    .MassConversionFactor = 1.0,
                    .Metallicity = OneZoneData[Index].Metallicity,
                    });
                    */

            EjectaMassSNIa += SNIa.EjectaMass;
            EnergySNIa += SNIa.Energy;

            OneZoneData[Index].CountSNIa ++;

            OneZoneData[Index].NSNIa += ThisRun.NSNIa; 

            AddElements(SNIa.Elements, EjectaElements);

            OneZoneData[Index].ExplosionTimeSNIa = OneZoneData[Index].Time+
                CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                        .R = gsl_rng_uniform(RandomGenerator),
                        .InitialMass_in_Msun = OneZoneData[Index].MstarBorn,
                        .Metallicity = OneZoneData[Index].Metallicity,
                        .Count = OneZoneData[Index].CountSNIa,
                        },CELibFeedbackType_SNIa);
                /*
                StellarFeedbackGetNextExplosionTime(CELibFeedbackType_SNIa,
                    OneZoneData[Index].Metallicity,OneZoneData[Index].MstarBorn,OneZoneData[Index].CountSNIa);
                    */
        }
    }

    OneZoneData[CurrentIndex].MejectaSNIa += EjectaMassSNIa;
    OneZoneData[CurrentIndex].EnergySNIa += EnergySNIa;

    double EjectaMassAGB = 0.e0;
    double EnergyAGB = 0.e0;
    if((ThisRun.OZUseAGB)&&(OneZoneData[Index].CountSNII > 0)){
        if(OneZoneData[Index].ExplosionTimeAGB<Tnow){

            struct CELibStructFeedbackOutput AGB =
                CELibGetFeedback((struct CELibStructFeedbackInput){
                        .Mass = OneZoneData[Index].MstarBorn,
                        .Metallicity = OneZoneData[Index].Metallicity,
                        .MassConversionFactor = 1.0,
                        .Elements = OneZoneData[Index].StellarElements,
                        .Count = OneZoneData[Index].CountAGB,
                        }, CELibFeedbackType_AGB);
            /*
                CELibGetAGBFeedback((struct CELibStructFeedbackInput){
                        .Mass = OneZoneData[Index].MstarBorn,
                        .MassConversionFactor = 1.0,
                        .Metallicity = OneZoneData[Index].Metallicity,
                        .Count = OneZoneData[Index].CountAGB,
                        .Elements = OneZoneData[Index].StellarElements,
                        });
                        */

            EjectaMassAGB += AGB.EjectaMass;
            EnergyAGB += AGB.Energy;

            AddElements(AGB.Elements, EjectaElements);

            OneZoneData[Index].CountAGB ++;
            OneZoneData[Index].ExplosionTimeAGB = OneZoneData[Index].Time+
                CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                        .R = gsl_rng_uniform(RandomGenerator),
                        .InitialMass_in_Msun = OneZoneData[Index].MstarBorn,
                        .Metallicity = OneZoneData[Index].Metallicity,
                        .Count = OneZoneData[Index].CountAGB,
                        },CELibFeedbackType_AGB);
                /*
                StellarFeedbackGetNextExplosionTime(CELibFeedbackType_AGB,
                        OneZoneData[Index].Metallicity,OneZoneData[Index].Mstar,OneZoneData[Index].CountAGB);
                        */
        }
    }
    OneZoneData[CurrentIndex].MejectaAGB += EjectaMassAGB;
    OneZoneData[CurrentIndex].EnergyAGB += EnergyAGB;


    double EjectaMassNSM = 0.e0;
    double EnergyNSM = 0.e0;

    if((ThisRun.OZUseNSM)&&(OneZoneData[Index].CountSNII > 0)){
        if(OneZoneData[Index].ExplosionTimeNSM<Tnow){
            struct CELibStructFeedbackOutput NSM = 
                CELibGetFeedback((struct CELibStructFeedbackInput){
                        .Mass = OneZoneData[Index].MstarBorn,
                        .MassConversionFactor = 1.0,
                        }, CELibFeedbackType_NSM);
                /*
                CELibGetNSMFeedback((struct CELibStructFeedbackInput){
                    .Mass = OneZoneData[Index].MstarBorn,
                    .MassConversionFactor = 1.0,
                    });
                    */

            EjectaMassNSM += NSM.EjectaMass;
            EnergyNSM += NSM.Energy;
            OneZoneData[Index].CountNSM ++;

            OneZoneData[Index].NNSM += ThisRun.NNSM; 

            AddElements(NSM.Elements, EjectaElements);

            OneZoneData[Index].ExplosionTimeNSM = OneZoneData[Index].Time+
                CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                        .R = gsl_rng_uniform(RandomGenerator),
                        .InitialMass_in_Msun = OneZoneData[Index].MstarBorn,
                        .Metallicity = OneZoneData[Index].Metallicity,
                        .Count = OneZoneData[Index].CountNSM,
                        },CELibFeedbackType_NSM);
            /*
                StellarFeedbackGetNextExplosionTime(CELibFeedbackType_NSM,
                    OneZoneData[Index].Metallicity,OneZoneData[Index].MstarBorn,OneZoneData[Index].CountNSM);
                    */
        }
    }

    OneZoneData[CurrentIndex].MejectaNSM += EjectaMassNSM;
    OneZoneData[CurrentIndex].EnergyNSM += EnergyNSM;

    double EjectaMass = EjectaMassSNII+EjectaMassSNIa+EjectaMassAGB+EjectaMassNSM;
    OneZoneData[CurrentIndex].Mejecta += EjectaMass;
    OneZoneData[CurrentIndex].Mgas += EjectaMass;
    OneZoneData[CurrentIndex].Mstar -= EjectaMass;

    OneZoneData[Index].MejectaTotal += EjectaMass;
    OneZoneData[Index].MejectaSNIITotal += EjectaMassSNII;
    OneZoneData[Index].MejectaSNIaTotal += EjectaMassSNIa;
    OneZoneData[Index].MejectaAGBTotal += EjectaMassAGB;
    OneZoneData[Index].MejectaNSMTotal += EjectaMassNSM;

    double Energy = EnergySNII+EnergySNIa+EnergyAGB+EnergyNSM;
    OneZoneData[CurrentIndex].Energy += Energy;

    OneZoneData[Index].EnergyTotal += Energy;
    OneZoneData[Index].EnergySNIITotal += EnergySNII;
    OneZoneData[Index].EnergySNIaTotal += EnergySNIa;
    OneZoneData[Index].EnergyAGBTotal += EnergyAGB;
    OneZoneData[Index].EnergyNSMTotal += EnergyNSM;


    for(int i=0;i<CELibYield_Number;i++){
        OneZoneData[CurrentIndex].EjectaElements[i] += EjectaElements[i];
    }

    return ;
}


static void UpdateGasMetallicity(const int Index){

    for(int i=0;i<CELibYield_Number;i++){
        OneZoneData[Index].Elements[i] = OneZoneData[Index-1].Elements[i]
                                        +OneZoneData[Index-1].EjectaElements[i];
    }

    double LightElements = OneZoneData[Index].Elements[CELibYield_H]
                          +OneZoneData[Index].Elements[CELibYield_He]; // Sum of H and He.

    double Mgas = 0.e0;
    for(int i=0;i<CELibYield_Number;i++){
        Mgas += OneZoneData[Index].Elements[i];
    }
    double HeavyElements = Mgas-LightElements;

    fprintf(stderr," Light and Heavy elements = %g %g, Mgas %g\n",
            LightElements,HeavyElements,Mgas);

    OneZoneData[Index].Metallicity = HeavyElements/Mgas;

    return ;
}

static void MakeStars(const int Index, const double dt, const double CurrentTime){

    double Ms = dt*OneZoneData[Index].SFR;

    OneZoneData[Index].MstarBorn = Ms;
    OneZoneData[Index].Mgas = OneZoneData[Index-1].Mgas-Ms;
    OneZoneData[Index].Mstar = OneZoneData[Index-1].Mstar+Ms;

    // Evaluate sum of elements in gas.
    double Mgas = 0.e0;
    for(int i=0;i<CELibYield_Number;i++){
        Mgas += OneZoneData[Index].Elements[i];
    }

    // record elements in stars
    double factor = Ms/Mgas;
    for(int i=0;i<CELibYield_Number;i++){
        OneZoneData[Index].StellarElements[i] = factor*OneZoneData[Index].Elements[i];
    }
    // update elements in gas
    for(int i=0;i<CELibYield_Number;i++){
        OneZoneData[Index].Elements[i] = (1-factor)*OneZoneData[Index].Elements[i];
    }
    // clear elements for ejecta (use later).
    for(int i=0;i<CELibYield_Number;i++){
        OneZoneData[Index].EjectaElements[i] = 0.e0;
    }

    // evaluate the timing of type II SN.
    OneZoneData[Index].ExplosionTimeSNII = CurrentTime+
        CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                .R = gsl_rng_uniform(RandomGenerator),
                .InitialMass_in_Msun = OneZoneData[Index].MstarBorn,
                .Metallicity = OneZoneData[Index].Metallicity,
                .Count = 0,
                },CELibFeedbackType_SNII);

    // clear variables.
    OneZoneData[Index].CountSNII = 0;
    OneZoneData[Index].CountSNIa = 0;
    OneZoneData[Index].CountAGB = 0;

    OneZoneData[Index].NSNII = 0.0;
    OneZoneData[Index].NSNIa = 0.0;
    OneZoneData[Index].NAGB = 0.0;

    OneZoneData[Index].Mejecta = 0.e0;
    OneZoneData[Index].MejectaSNII = 0.e0;
    OneZoneData[Index].MejectaSNIa = 0.e0;
    OneZoneData[Index].MejectaAGB = 0.e0;

    OneZoneData[Index].MejectaTotal = 0.e0;
    OneZoneData[Index].MejectaSNIITotal = 0.e0;
    OneZoneData[Index].MejectaSNIaTotal = 0.e0;
    OneZoneData[Index].MejectaAGBTotal = 0.e0;

    return;
}

/*!
 *
 */
static void SystemUpdate(void){

    double dt = ThisRun.OZTime/ThisRun.OZTimeBin;
    for(int i=1;i<ThisRun.OZTimeBin;i++){
        double time = dt*i;
        OneZoneData[i].Time = time;

        // Update metallicity in gas
        UpdateGasMetallicity(i);

        // Make stars, update gas mass and clear variables.
        MakeStars(i,dt,time);

        fprintf(stderr,"[%d] time %g, Mgas = %g, Mstar = %g, MstarBorn = %g, Z = %g\n",
                i,time,OneZoneData[i].Mgas,OneZoneData[i].Mstar,OneZoneData[i].MstarBorn,
                OneZoneData[i].Metallicity);

        for(int k=0;k<i;k++){ // Operations for SSPs formed before this steps.
            UpdateSSP_i(k,time,i);
        }
        fprintf(stderr," MejSNII = %g, MejSNIa = %g, MejAGB = %g, MejNSM = %g\n",
                OneZoneData[i].MejectaSNII,
                OneZoneData[i].MejectaSNIa,
                OneZoneData[i].MejectaAGB,
                OneZoneData[i].MejectaNSM);
        fprintf(stderr," EnergySNII = %g, EnergySNIa = %g, EnergyAGB = %g, EnergyNSM = %g\n",
                OneZoneData[i].EnergySNII,
                OneZoneData[i].EnergySNIa,
                OneZoneData[i].EnergyAGB,
                OneZoneData[i].EnergyNSM);
    }


    return ;
}

static void ShowLog(void){

    fprintf(stderr,"### Show log ###\n");
    fprintf(stderr,"\n");

    fprintf(stderr," Mgas = %g, Mstar %g\n",
            OneZoneData[ThisRun.OZTimeBin-1].Mgas,
            OneZoneData[ThisRun.OZTimeBin-1].Mstar);

    double MejectaSNII = 0.e0;
    double MejectaSNIa = 0.e0;
    double MejectaAGB = 0.e0;
    double MejectaNSM = 0.e0;
    for(int i=0;i<ThisRun.OZTimeBin;i++){
        MejectaSNII += OneZoneData[i].MejectaSNII;
        MejectaSNIa += OneZoneData[i].MejectaSNIa;
        MejectaAGB += OneZoneData[i].MejectaAGB;
        MejectaNSM += OneZoneData[i].MejectaNSM;
    }

    fprintf(stderr," MejectaSNII = %g, MejectaSNIa = %g, MejectaAGB = %g MejectaNSM = %g\n",
            MejectaSNII,MejectaSNIa,MejectaAGB,MejectaNSM);
    
    fprintf(stderr,"\n");
    double NSNII = 0.e0;
    double NSNIa = 0.e0;
    double NAGB = 0.e0;
    double NNSM = 0.e0;
    for(int i=0;i<ThisRun.OZTimeBin;i++){
        NSNII += OneZoneData[i].NSNII;
        NSNIa += OneZoneData[i].NSNIa;
        NAGB += OneZoneData[i].NAGB;
        NNSM += OneZoneData[i].NNSM;
    }

    fprintf(stderr," NSNII = %g, NSNIa = %g, NAGB = %g, NNSM = %g\n",
            NSNII,NSNIa,NAGB,NNSM);

    
    fprintf(stderr,"\n");
    int CountSNIa = 0;
    for(int i=0;i<ThisRun.OZTimeBin;i++)
        CountSNIa += OneZoneData[i].CountSNIa;

    double Minit  = 0;
    for(int i=0;i<ThisRun.OZTimeBin;i++)
        Minit += OneZoneData[i].MstarBorn;

    fprintf(stderr," SNIa count = %d\n",CountSNIa);
    fprintf(stderr," Expected SNIa event Number = %g\n",
            0.002*Minit);

    double EnergySNII = 0.e0;
    double EnergySNIa = 0.e0;
    double EnergyAGB = 0.e0;
    double EnergyNSM = 0.e0;
    for(int i=0;i<ThisRun.OZTimeBin;i++){
        EnergySNII += OneZoneData[i].EnergySNII;
        EnergySNIa += OneZoneData[i].EnergySNIa;
        EnergyAGB += OneZoneData[i].EnergyAGB;
        EnergyNSM += OneZoneData[i].EnergyAGB;
    }   


    fprintf(stderr," EnergySNII = %g, EnergySNIa = %g, EnergyAGB = %g, EnergyNSM = %g, Energy = %g\n",
            EnergySNII,
            EnergySNIa,
            EnergyAGB,
            EnergyNSM,
            EnergySNII+EnergySNIa+EnergyAGB+EnergyNSM);

    return ;
}

static void PlotOneZoneResults(void);

void OneZoneModel(void){

    CELibSaveRunParameter();
    InitializeRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();

    SystemUpdate();

    ShowLog();

    PlotOneZoneResults();

    CELibLoadRunParameter();

    return ;
}

static void PlotLabelOnMultiModels(void){
    cpgsubp(3,3);
    cpgpanl(1,1);
    cpgswin(ThisRun.OZFeHMin,ThisRun.OZFeHMax,ThisRun.OZAlphaFeMin,ThisRun.OZAlphaFeMax);

    float x[] = {-3.9,-3.9};
    float y[] = {-0.5,-3};

    int CID[] = {4,2,1,8,8};
    char label1[][MaxCharactersInLine] = {"SNeII","SNeII+SNeIa","SNeII+SNeIa+AGBs+NSMs","SNeII+SNeIa+AGBs+NSMs","                 +PopIIIs"};
    char label2[][MaxCharactersInLine] = {"SNeII","SNeII+SNeIa","SNeII+SNeIa+AGBs+NSMs","SNeII\\dori\\u+SNeIa+AGBs+NSMs"};
    char (*label)[MaxCharactersInLine];

    if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_N13){
        label = label1;
    } else {
        label = label2;
    }

    float ystep = 0.18;
    if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_N13){
        for(int i=0;i<5;i++){
            cpgsci(CID[i]);
            cpgptxt(-3.8,y[0]-ystep*i,0.0,0.0,label[i]);
        }
    } else if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_P98){
        for(int i=0;i<4;i++){
            cpgsci(CID[i]);
            cpgptxt(-3.8,y[0]-ystep*i,0.0,0.0,label[i]);
        }
    }

    return ;
}

static void PlotLabelOnMultiModelsTen(void){
    cpgsubp(4,4);
    cpgpanl(3,3);
    cpgswin(ThisRun.OZFeHMin,ThisRun.OZFeHMax,ThisRun.OZAlphaFeMin,ThisRun.OZAlphaFeMax);

    float x[] = {-3.9,-3.9};
    float y[] = {1.0,-3};

    int CID[] = {4,2,1,8,12};
    char label1[][MaxCharactersInLine] = {"SNeII","SNeII+SNeIa","SNeII+SNeIa+AGBs+NSMs","SNeII+SNeIa+AGBs+NSMs+PopIIIs"
        ,"SNeII+SNeIa+AGBs+NSMs+PopIIIs+HNe"};
    char label2[][MaxCharactersInLine] = {"SNeII","SNeII+SNeIa","SNeII+SNeIa+AGBs+NSMs","SNeII\\dori\\u+SNeIa+AGBs+NSMs"};
    char (*label)[MaxCharactersInLine];

    if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_N13){
        label = label1;
    } else if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_P98){
        label = label2;
    }

    cpgsch(3.5);

    float ystep = 0.3;
    if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_N13){
        for(int i=0;i<5;i++){
            cpgsci(CID[i]);
            cpgptxt(-4.5,y[0]-ystep*i,0.0,0.0,label[i]);
        }
    } else if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_P98){
        for(int i=0;i<4;i++){
            cpgsci(CID[i]);
            cpgptxt(-4.5,y[0]-ystep*i,0.0,0.0,label[i]);
        }
    }
    cpgsch(2);

    return ;
}

static void PlotMultiModelsBody(const int ColorID);
static void PlotMultiModelsBody10(const int ColorID);


void OneZoneModelMultiModelsOneModel(void){

    char fname[MaxCharactersInLine];
    if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_N13){
        Snprintf(fname,"%s/OZ_MetalDist_MultiAllN13",ThisRun.OutDir);
    } else if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_P98){
        Snprintf(fname,"%s/OZ_MetalDist_MultiAllP98",ThisRun.OutDir);
    }

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_P98){
        InitializeRandomGenerator(1977);
        InitOneZoneData();
        InitOneZoneSFR();
        ThisRun.OZUseSNII = 1;
        ThisRun.OZUseSNIa = 1;
        ThisRun.OZUseAGB = 1;
        ThisRun.OZUseNSM = 1;
        CELibRunParameters.SNIIYieldsModificationP98 = 0;
        CELibShowCurrentStatus();
        CELibInit();

        SystemUpdate();
        ShowLog();
        cpgsls(4);
        if(ThisRun.OZPlotTen){
            PlotMultiModelsBody10(8);
        } else {
            PlotMultiModelsBody(8);
        }
        cpgsci(1);
        ReleaseOneZoneData();

        CELibRunParameters.SNIIYieldsModificationP98 = 1;
        CELibInit();
    } else {
        InitializeRandomGenerator(1977);
        InitOneZoneData();
        InitOneZoneSFR();
        ThisRun.OZUseSNII = 1;
        ThisRun.OZUseSNIa = 1;
        ThisRun.OZUseAGB = 1;
        ThisRun.OZUseNSM = 1;
        CELibRunParameters.SNIIHyperNovaFraction = 0.0;
        CELibRunParameters.PopIIIIMF = 1;
        CELibRunParameters.PopIIISNe = 1;
        CELibRunParameters.PopIIIAGB = 1;
        CELibRunParameters.PopIIIAGBYieldsTableID = CELibAGBZ0YieldsTableID_CL08G13;
        CELibShowCurrentStatus();
        CELibInit();

        SystemUpdate();
        ShowLog();
        cpgsls(4);
        if(ThisRun.OZPlotTen){
            PlotMultiModelsBody10(8);
        } else {
            PlotMultiModelsBody(8);
        }
        cpgsci(1);
        ReleaseOneZoneData();

        /////////////////////////////////////////////////////////////////////////////////////

        InitializeRandomGenerator(1977);
        int Log = ThisRun.OZTimeBin; 
        // ThisRun.OZTimeBin *= 4; 
        InitOneZoneData();
        InitOneZoneSFR();
        ThisRun.OZUseSNII = 1;
        ThisRun.OZUseSNIa = 1;
        ThisRun.OZUseAGB = 1;
        ThisRun.OZUseNSM = 1;
        CELibRunParameters.SNIIHyperNovaFraction = 0.5;
        //CELibRunParameters.PopIIIMetallicity = 1.e-6;
        CELibRunParameters.PopIIIIMF = 1;
        CELibRunParameters.PopIIISNe = 1;
        CELibRunParameters.PopIIIAGB = 1;
        CELibRunParameters.PopIIIAGBYieldsTableID = CELibAGBZ0YieldsTableID_CL08G13;
        CELibShowCurrentStatus();
        CELibInit();

        SystemUpdate();
        ShowLog();
        cpgsls(9);
        if(ThisRun.OZPlotTen){
            PlotMultiModelsBody10(12);
        } else {
            PlotMultiModelsBody(12);
        }
        cpgsci(1);
        ReleaseOneZoneData();
        ThisRun.OZTimeBin = Log;
        /////////////////////////////////////////////////////////////////////////////////////

        CELibRunParameters.SNIIYieldsModificationP98 = 1;
        CELibRunParameters.SNIIHyperNovaFraction = 0;
        CELibRunParameters.PopIIIIMF = 0;
        CELibRunParameters.PopIIISNe = 0;
        CELibRunParameters.PopIIIAGB = 0;

        CELibInit();
    }
    CELibShowCurrentStatus();


    InitializeRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 0;
    ThisRun.OZUseAGB = 0;
    ThisRun.OZUseNSM = 0;
    SystemUpdate();
    ShowLog();
    cpgsls(1);
    if(ThisRun.OZPlotTen){
        PlotMultiModelsBody10(4);
    } else {
        PlotMultiModelsBody(4);
    }
    cpgsci(1);
    ReleaseOneZoneData();
    
    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 0;
    ThisRun.OZUseNSM = 0;
    SystemUpdate();
    ShowLog();
    cpgsls(2);
    if(ThisRun.OZPlotTen){
        PlotMultiModelsBody10(2);
    } else {
        PlotMultiModelsBody(2);
    }
    cpgsci(1);
    ReleaseOneZoneData();

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;
    ThisRun.OZUseNSM = 1;
    SystemUpdate();
    ShowLog();
    cpgsls(9);
    if(ThisRun.OZPlotTen){
        PlotMultiModelsBody10(1);
    } else {
        PlotMultiModelsBody(1);
    }
    cpgsci(1);
    ReleaseOneZoneData();

    // Label
    if(ThisRun.OZPlotTen){
        PlotLabelOnMultiModelsTen();
    } else {
        PlotLabelOnMultiModels();
    }

    ClosePGPlot();


    return ;
}

void OneZoneModelMultiModels(void){

    CELibSaveRunParameter();

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
    CELibInit();
    OneZoneModelMultiModelsOneModel();

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_P98);
    CELibInit();
    OneZoneModelMultiModelsOneModel();

    CELibLoadRunParameter();
}


static void PlotMassEvolution(const int mode);

void OneZoneModelMultiModelsMassEvolutionOneModel(void){

    //CELibSaveRunParameter();

    char fname[MaxCharactersInLine];
    if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_N13){
        Snprintf(fname,"%s/OZ_MetalDist_MassEvolutionN",ThisRun.OutDir);
    } else if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_P98){
        Snprintf(fname,"%s/OZ_MetalDist_MassEvolutionP",ThisRun.OutDir);
    }

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgslw(4);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgswin(ThisRun.OZAgeMin,ThisRun.OZAgeMax,ThisRun.OZMassMin,ThisRun.OZMassMax);
    cpgbox("BCTSN",0,0.,"BCTSNL",0.0,0.0);

    InitializeRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 0;
    ThisRun.OZUseSNIa = 0;
    ThisRun.OZUseAGB = 0;
    SystemUpdate();
    ShowLog();
    cpgslw(4);
    PlotMassEvolutionTwo(8,9);
    ReleaseOneZoneData();

    InitializeRandomGenerator(1977);
    CELibSetRunParameterSNIIHyperNovaFraction(0.0);
    CELibSetRunParameterPopIIISNe(0);
    CELibSetRunParameterPopIIIIMF(0);
    CELibSetRunParameterPopIIIAGB(0);
    CELibInitSNIIYields();

    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 0;
    ThisRun.OZUseAGB = 0;
    SystemUpdate();
    ShowLog();
    cpgsci(4);
    cpgslw(4);
    PlotMassEvolution(4);
    ReleaseOneZoneData();
    
    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 0;
    SystemUpdate();
    ShowLog();
    cpgsci(2);
    cpgslw(4);
    PlotMassEvolution(2);
    ReleaseOneZoneData();

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;
    SystemUpdate();
    ShowLog();
    cpgsci(1);
    cpgslw(4);
    PlotMassEvolution(1);
    ReleaseOneZoneData();

    /// Caption
    cpgsch(1.5);
    int LS[] = {9,4,2,1,9,5};
    int LC[] = {8,4,2,1,8,12};
    char label[][MaxCharactersInLine] ={"No Feedback","SNeII","SNeII+SNeIa",
                    "SNeII+SNeIa+AGBs","SNeII+SNeIa+AGBs+PopIIIs","SNeII+SNeIa+AGBs+PopIIIs+HNe"};
    cpgslw(4);

    int NPlot;
    if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_N13){
        NPlot = 4;
    } else {
        NPlot = 4;
    }
    for(int i=0;i<NPlot;i++){
        cpgsls(LS[i]);
        cpgsci(LC[i]);
        float y_ = 10.9-0.07*i;
        cpgptxt(2.5,y_,0.0,0.0,label[i]);
        float xl[2] = {0.7,2.2};
        float yl[2] = {y_+0.015,y_+0.015};
        cpgline(2,xl,yl);
    }
    //cpgslw(2);


    cpgsch(2.0);
    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Time [Gyr]","Mass [M\\d\\(2281)\\u]","");

    ClosePGPlot();

    //CELibLoadRunParameter();

    return ;
}

void OneZoneModelMultiModelsMassEvolution(void){

    CELibSaveRunParameter();

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
    CELibInit();
    OneZoneModelMultiModelsMassEvolutionOneModel();

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_P98);
    CELibInit();
    OneZoneModelMultiModelsMassEvolutionOneModel();

    CELibLoadRunParameter();

    return ;
}


static void PlotMetalDistributionBody(const int mode);
void OneZoneModelMultiModelsMetalDistribution(void){

    CELibSaveRunParameter();

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/OZ_MetalDist_Multi",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgslw(4);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgswin(ThisRun.OZFeHMin,ThisRun.OZFeHMax,ThisRun.OZFracMin,ThisRun.OZFracMax);
    cpgbox("BCTSN",0,0.,"BCTSN",0.0,0.0);

    InitializeRandomGenerator(1977);

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
    CELibInit();

    InitOneZoneData();
    InitOneZoneSFR();
    SystemUpdate();
    ShowLog();
    cpgsci(1);
    PlotMetalDistributionBody(1);
    ReleaseOneZoneData();
    
    ResetRandomSeedForRandomGenerator(1977);

#if 1
    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_P98);
    CELibRunParameters.SNIIYieldsModificationP98 = 1;
    CELibInit();

    InitOneZoneData();
    InitOneZoneSFR();
    SystemUpdate();
    ShowLog();
    cpgsci(2);
    PlotMetalDistributionBody(2);
    ReleaseOneZoneData();

    ResetRandomSeedForRandomGenerator(1977);

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_P98);
    CELibRunParameters.SNIIYieldsModificationP98 = 0;
    CELibInit();

    InitOneZoneData();
    InitOneZoneSFR();
    SystemUpdate();
    ShowLog();
    cpgsci(4);
    PlotMetalDistributionBody(4);
    ReleaseOneZoneData();
#endif

    int LS[] = {1,2,3,9};
    int LC[] = {1,2,4,8};
    char label[][MaxCharactersInLine] ={"Nomoto+13","Portinari+98","Portinari+98 (Original)",""};
    cpgslw(4);
    for(int i=0;i<3;i++){
        cpgsls(LS[i]);
        cpgsci(LC[i]);
        float y_ = 0.185-0.013*i;
        cpgsch(1.5);
        cpgptxt(-3.,y_,0.0,0.0,label[i]);
        cpgsch(2);

        float xl[2] = {-3.8,-3.2};
        float yl[2] = {y_+0.004,y_+0.004};
        cpgline(2,xl,yl);
    }
    cpgslw(2);

    cpgsci(1);
    cpgsls(1);
    cpgslw(4);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("[Fe/H]","Fraction of stars","");

    ClosePGPlot();

    CELibLoadRunParameter();

    return ;
}

static void PlotAlphaFeBody(const int mode, const int ColorID, const int Type);

static void PlotNi(const int ColorID, const int Type){
    PlotAlphaFeBody(CELibYield_Ni,ColorID,Type);
    return ;
}
static void PlotC(const int ColorID, const int Type){
    PlotAlphaFeBody(CELibYield_C,ColorID,Type);
    return ;
}
static void PlotN(const int ColorID, const int Type){
    PlotAlphaFeBody(CELibYield_N,ColorID,Type);
    return ;
}
static void PlotO(const int ColorID, const int Type){
    PlotAlphaFeBody(CELibYield_O,ColorID,Type);
    return ;
}
static void PlotMg(const int ColorID, const int Type){
    PlotAlphaFeBody(CELibYield_Mg,ColorID,Type);
    return ;
}
static void PlotEu(const int ColorID, const int Type){
    PlotAlphaFeBody(CELibYield_Eu,ColorID,Type);
    return ;
}

void OneZoneModelMultiModelsNiComparison(void){

    CELibSaveRunParameter();

    // CELibRunParameters.PopIIISNe = 0;
    // CELibRunParameters.PopIIIIMF = 0;

    //////////////////////////////////////////////////////
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/OZ_MetalDist_SNIaNiCompY",ThisRun.OutDir);

    CELibSetRunParameterSNIIYieldsTableID(1);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgslw(4);
    cpgsvp(0.2,0.9,0.2,0.9);

    InitializeRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;
    
    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N1);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(1);
    cpgslw(4);
    PlotNi(1,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(2);
    cpgslw(4);
    PlotNi(2,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    
    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N1600);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(3);
    cpgslw(4);
    PlotNi(3,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100ZDepend);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(4);
    cpgslw(4);
    PlotNi(4,ThisRun.OZPlotType);
    ReleaseOneZoneData();


    cpgsci(1);
    cpgsch(2.0);

    cpgslw(4);
    char Label[][MaxCharactersInLine] = {"N1","N100","N1600","Metal Dep.",};
    int Colors[] = {1,2,3,4};
    int LS[] = {1,2,3,4};
    for(int i=0;i<4;i++){
        float y_ = 1.1-0.13*i;
        cpgsci(Colors[i]);
        cpgsch(1.6);
        if(ThisRun.OZPlotType == 0){
            cpgptxt(-3.5,y_,0.0,0.0,Label[i]);
            cpgsch(2.0);
            float xl = -3.7;
            float yl = y_+0.05;
            cpgpt1(xl,yl,1);
        } else {
            cpgptxt(-3.0,y_,0.0,0.0,Label[i]);
            cpgsch(2.0);
            float xl[] = {-3.7,-3.1};
            float yl[] = {y_+0.05,y_+0.05};
            cpgsls(LS[i]);
            cpgline(2,xl,yl);
        }
    }
    ClosePGPlot();
    //////////////////////////////////////////////////////

    //////////////////////////////////////////////////////
    Snprintf(fname,"%s/OZ_MetalDist_SNIaNiCompYDim",ThisRun.OutDir);

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgslw(4);
    cpgsvp(0.2,0.9,0.2,0.9);

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_I99);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_I99_W7);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(1);
    cpgslw(4);
    PlotNi(1,ThisRun.OZPlotType);
    ReleaseOneZoneData();

#if 1
    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_T04);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_T04_b303d768);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(2);
    cpgslw(4);
    PlotNi(2,ThisRun.OZPlotType);
    ReleaseOneZoneData();
#endif

    
    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_M10);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_M10_ODDT);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(3);
    cpgslw(4);
    PlotNi(3,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(4);
    cpgslw(4);
    PlotNi(4,ThisRun.OZPlotType);
    ReleaseOneZoneData();


    cpgsci(1);
    cpgsch(2.0);

    cpgslw(4);
    char Label2[][MaxCharactersInLine] = {"Iwamoto+99 W7","Travaglio+04 b30_3d_768","Maeda+10 O-DDT","Seitenzahl+13 N100"};
    int Colors2[] = {1,2,3,4};
    int LS2[] = {1,2,3,4};
    for(int i=0;i<4;i++){
        float y_ = 1.1-0.13*i;
        cpgsci(Colors2[i]);
        cpgsch(1.6);
        if(ThisRun.OZPlotType == 0){
            cpgptxt(-3.5,y_,0.0,0.0,Label2[i]);
            cpgsch(2.0);
            float xl = -3.7;
            float yl = y_+0.05;
            cpgpt1(xl,yl,1);
        } else {
            cpgptxt(-3.0,y_,0.0,0.0,Label2[i]);
            cpgsch(2.0);
            float xl[] = {-3.7,-3.1};
            float yl[] = {y_+0.05,y_+0.05};
            cpgsls(LS2[i]);
            cpgline(2,xl,yl);
        }
    }
    ClosePGPlot();
    //////////////////////////////////////////////////////

    /// SNIa DTD model
    //////////////////////////////////////////////////////
    Snprintf(fname,"%s/OZ_MetalDist_SNIaNiCompDTD",ThisRun.OutDir);

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgslw(4);
    cpgsvp(0.2,0.9,0.2,0.9);


    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(1);
    cpgslw(4);
    PlotNi(1,ThisRun.OZPlotType);
    cpgsls(1);
    ReleaseOneZoneData();

    
    InitializeRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;
    
    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_GR83);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(2);
    cpgslw(4);
    PlotNi(2,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    cpgsci(1);
    cpgsch(2.0);

    char Label3[][MaxCharactersInLine] = {"Power law","Greggio&Renzini 83"};
    int Colors3[] = {1,2};
    int LS3[] = {1,2};
    for(int i=0;i<2;i++){
        float y_ = 1.1-0.13*i;
        cpgsci(Colors3[i]);
        cpgsch(1.6);
        if(ThisRun.OZPlotType == 0){
            cpgptxt(-3.5,y_,0.0,0.0,Label3[i]);
            cpgsch(2.0);
            float xl = -3.7;
            float yl = y_+0.05;
            cpgpt1(xl,yl,1);
        } else {
            cpgptxt(-3.0,y_,0.0,0.0,Label3[i]);
            cpgsch(2.0);
            float xl[] = {-3.7,-3.1};
            float yl[] = {y_+0.05,y_+0.05};
            cpgsls(LS3[i]);
            cpgline(2,xl,yl);
        }
    }
    ClosePGPlot();
    //////////////////////////////////////////////////////

    CELibLoadRunParameter();

    return ;
}

void OneZoneModelMultiModelsMgComparison(void){

    CELibSaveRunParameter();

    //////////////////////////////////////////////////////
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/OZ_MetalDist_SNIaMgCompY",ThisRun.OutDir);

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgslw(4);
    cpgsvp(0.2,0.9,0.2,0.9);

    InitializeRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;
    
    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N1);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(1);
    cpgslw(4);
    PlotMg(1,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(2);
    cpgslw(4);
    PlotMg(2,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    
    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N1600);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(3);
    cpgslw(4);
    PlotMg(3,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100ZDepend);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(4);
    cpgslw(4);
    PlotMg(4,ThisRun.OZPlotType);
    ReleaseOneZoneData();


    cpgsci(1);
    cpgsch(2.0);
    cpgslw(4);

    char Label[][MaxCharactersInLine] = {"N1","N100","N1600","Metal Dep.",};
    int Colors[] = {1,2,3,4};
    int LS[] = {1,2,3,4};
    for(int i=0;i<4;i++){
        float y_ = -0.6-0.13*i;
        cpgsci(Colors[i]);
        cpgsch(1.6);
        if(ThisRun.OZPlotType == 0){
            cpgptxt(-3.5,y_,0.0,0.0,Label[i]);
            cpgsch(2.0);
            float xl = -3.7;
            float yl = y_+0.05;
            cpgpt1(xl,yl,1);
        } else {
            cpgptxt(-3.0,y_,0.0,0.0,Label[i]);
            cpgsch(2.0);
            float xl[] = {-3.7,-3.1};
            float yl[] = {y_+0.05,y_+0.05};
            cpgsls(LS[i]);
            cpgline(2,xl,yl);
        }
    }
    ClosePGPlot();
    //////////////////////////////////////////////////////

    //////////////////////////////////////////////////////
    Snprintf(fname,"%s/OZ_MetalDist_SNIaMgCompYDim",ThisRun.OutDir);

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_I99);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_I99_W7);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(1);
    cpgslw(2);
    PlotMg(1,ThisRun.OZPlotType);
    ReleaseOneZoneData();

#if 1
    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_T04);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_T04_b303d768);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(2);
    cpgslw(2);
    PlotMg(2,ThisRun.OZPlotType);
    ReleaseOneZoneData();
#endif

    
    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_M10);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_M10_ODDT);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(3);
    cpgslw(2);
    PlotMg(3,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(4);
    cpgslw(2);
    PlotMg(4,ThisRun.OZPlotType);
    ReleaseOneZoneData();


    cpgsci(1);
    cpgsch(2.0);

    char Label2[][MaxCharactersInLine] = {"Iwamoto+99,W7","Travaglio+04,b30_3d_768","Maeda+10,O-DDT","Seitenzahl+13,N100"};
    int Colors2[] = {1,2,3,4};
    int LS2[] = {1,2,3,4};
    for(int i=0;i<4;i++){
        float y_ = -0.6-0.13*i;
        cpgsci(Colors2[i]);
        cpgsch(1.6);
        if(ThisRun.OZPlotType == 0){
            cpgptxt(-3.5,y_,0.0,0.0,Label2[i]);
            cpgsch(2.0);
            float xl = -3.7;
            float yl = y_+0.05;
            cpgpt1(xl,yl,1);
        } else {
            cpgptxt(-3.0,y_,0.0,0.0,Label2[i]);
            cpgsch(2.0);
            float xl[] = {-3.7,-3.1};
            float yl[] = {y_+0.05,y_+0.05};
            cpgsls(LS2[i]);
            cpgline(2,xl,yl);
        }
    }
    ClosePGPlot();
    //////////////////////////////////////////////////////

    /// SNIa DTD model
    //////////////////////////////////////////////////////
    Snprintf(fname,"%s/OZ_MetalDist_SNIaMgCompDTD",ThisRun.OutDir);

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);


    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(1);
    cpgslw(2);
    PlotMg(1,ThisRun.OZPlotType);
    cpgsls(1);
    ReleaseOneZoneData();

    
    InitializeRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;
    
    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_GR83);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(2);
    cpgslw(2);
    PlotMg(2,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    cpgsci(1);
    cpgsch(2.0);

    char Label3[][MaxCharactersInLine] = {"Power law","Greggio & Renzini 83"};
    int Colors3[] = {1,2};
    int LS3[] = {1,2};
    for(int i=0;i<2;i++){
        float y_ = -0.6-0.13*i;
        cpgsci(Colors3[i]);
        cpgsch(1.6);
        if(ThisRun.OZPlotType == 0){
            cpgptxt(-3.5,y_,0.0,0.0,Label3[i]);
            cpgsch(2.0);
            float xl = -3.7;
            float yl = y_+0.05;
            cpgpt1(xl,yl,1);
        } else {
            cpgptxt(-3.0,y_,0.0,0.0,Label3[i]);
            cpgsch(2.0);
            float xl[] = {-3.7,-3.1};
            float yl[] = {y_+0.05,y_+0.05};
            cpgsls(LS3[i]);
            cpgline(2,xl,yl);
        }
    }
    ClosePGPlot();
    //////////////////////////////////////////////////////

    CELibLoadRunParameter();

    return ;
}

void OneZoneModelMultiModelsCComparison(void){

    CELibSaveRunParameter();

    //////////////////////////////////////////////////////
    char fname[MaxCharactersInLine];
#if 0
    Snprintf(fname,"%s/OZ_MetalDist_SNIaCCompY",ThisRun.OutDir);

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    InitializeRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;
    
    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N1);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsci(4);
    PlotC(4,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsci(3);
    PlotC(3,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    
    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N1600);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsci(2);
    PlotC(2,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100ZDepend);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsci(1);
    PlotC(1,ThisRun.OZPlotType);
    ReleaseOneZoneData();


    cpgsci(1);
    cpgsch(2.0);

    char Label[][MaxCharactersInLine] = {"N1","N100","N1600","Metal Dep.",};
    int Colors[] = {4,3,2,1};
    for(int i=0;i<4;i++){
        float y_ = 0.8-0.13*i;
        cpgsci(Colors[i]);
        cpgptxt(-3.5,y_,0.0,0.0,Label[i]);
        float xl = -3.7;
        float yl = y_+0.05;
        cpgpt1(xl,yl,1);
    }
    ClosePGPlot();
    //////////////////////////////////////////////////////

    //////////////////////////////////////////////////////
    Snprintf(fname,"%s/OZ_MetalDist_SNIaCCompYDim",ThisRun.OutDir);

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_I99);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_I99_W7);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsci(4);
    PlotC(4,ThisRun.OZPlotType);
    ReleaseOneZoneData();

#if 1
    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_T04);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_T04_b303d768);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsci(3);
    PlotC(3,ThisRun.OZPlotType);
    ReleaseOneZoneData();
#endif

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_M10);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_M10_ODDT);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsci(2);
    PlotC(2,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsci(1);
    PlotC(1,ThisRun.OZPlotType);
    ReleaseOneZoneData();


    cpgsci(1);
    cpgsch(2.0);

    char Label2[][MaxCharactersInLine] = {"Iwamoto+99,W7","Travaglio+04,b30_3d_768","Maeda+10,O-DDT","Seitenzahl+13,N100"};
    int Colors2[] = {4,3,2,1};
    for(int i=0;i<4;i++){
        float y_ = 0.87-0.13*i;
        cpgsci(Colors2[i]);
        cpgptxt(-3.5,y_,0.0,0.0,Label2[i]);
        float xl = -3.7;
        float yl = y_+0.04;
        cpgpt1(xl,yl,1);
    }
    ClosePGPlot();
    //////////////////////////////////////////////////////
#endif

    /// SNIa DTD model
    //////////////////////////////////////////////////////
    Snprintf(fname,"%s/OZ_MetalDist_SNIaCCompDTD",ThisRun.OutDir);

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    InitializeRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;
    
    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsci(2);
    PlotC(2,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_GR83);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100ZDepend);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsci(1);
    PlotC(1,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    cpgsci(1);
    cpgsch(2.0);

    char Label3[][MaxCharactersInLine] = {"Power law","Greggio & Renzini 83"};
    int Colors3[] = {1,2};
    for(int i=0;i<2;i++){
        float y_ = 0.8-0.13*i;
        cpgsci(Colors3[i]);
        cpgptxt(-3.5,y_,0.0,0.0,Label3[i]);
        float xl = -3.7;
        float yl = y_+0.05;
        cpgpt1(xl,yl,1);
    }
    ClosePGPlot();
    //////////////////////////////////////////////////////

    CELibLoadRunParameter();

    return ;
}


void OneZoneModelMultiModelsAGBComparison(void){

    CELibSaveRunParameter();

    //////////////////////////////////////////////////////
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/OZ_MetalDist_AGBsCCompY",ThisRun.OutDir);

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgslw(4);
    cpgsvp(0.2,0.9,0.2,0.9);

    InitializeRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;
    
    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100);
    CELibSetRunParameterAGBYieldsTableID(CELibAGBYieldsTableID_K10);

    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(1);
    cpgslw(4);
    PlotN(1,ThisRun.OZPlotType);
    cpgsci(1);
    ReleaseOneZoneData();



    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100);
    CELibSetRunParameterAGBYieldsTableID(CELibAGBYieldsTableID_K10D14);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgsls(2);
    cpgslw(4);
    PlotN(2,ThisRun.OZPlotType);
    cpgsci(1);
    ReleaseOneZoneData();

    cpgsci(1);
    cpgsch(2.0);
    cpgslw(4);

    //char Label[][MaxCharactersInLine] = {"Karakas 10","Karakas 10"," & Doherty+14"};
    char Label[][MaxCharactersInLine] = {"Karakas 10","Karakas 10&Doherty+14"};
    int Colors[] = {1,2,2};
    int LS[] = {1,2};
    for(int i=0;i<2;i++){
        float y_ = 1.1-0.15*i;
        cpgsci(Colors[i]);
        cpgsch(1.6);
        if(ThisRun.OZPlotType == 0){
            cpgptxt(-3.5,y_,0.0,0.0,Label[i]);
            cpgsch(2.0);
            float xl = -3.7;
            float yl = y_+0.05;
            cpgpt1(xl,yl,1);
        } else {
            cpgptxt(-3.0,y_,0.0,0.0,Label[i]);
            cpgsch(2.0);
            float xl[] = {-3.7,-3.1};
            float yl[] = {y_+0.05,y_+0.05};
            cpgsls(LS[i]);
            if(i<2)
                cpgline(2,xl,yl);
        }
    }
    ClosePGPlot();
    //////////////////////////////////////////////////////

    CELibLoadRunParameter();

    return ;
}


void OneZoneModelMultiModelsEuComparison(void){

    CELibSaveRunParameter();

    //////////////////////////////////////////////////////
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/OZ_MetalDist_NSMCompEu",ThisRun.OutDir);

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgslw(4);
    cpgsvp(0.2,0.9,0.2,0.9);

    InitializeRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;
    ThisRun.OZUseNSM = 1;
    
    CELibSetRunParameterNSMDTDPowerLawIndex(-0.5);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgslw(4);
    cpgsls(3);
    PlotEu(3,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    
    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;
    ThisRun.OZUseNSM = 1;

    CELibSetRunParameterNSMDTDPowerLawIndex(-2.0);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgslw(4);
    cpgsls(2);
    PlotEu(2,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    ResetRandomSeedForRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;
    ThisRun.OZUseNSM = 1;

    CELibSetRunParameterNSMDTDPowerLawIndex(-1.0);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgslw(4);
    cpgsls(1);
    PlotEu(1,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    ////////////////////////////////////////////////////////

    InitializeRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;
    ThisRun.OZUseNSM = 1;

    CELibSetRunParameterNSMDTDPowerLawIndex(-1.0);
    CELibSetRunParameterNSMDTDOffsetForPower(1.e+7);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgslw(4);
    cpgsls(4);
    PlotEu(4,ThisRun.OZPlotType);
    ReleaseOneZoneData();


    InitializeRandomGenerator(1977);
    InitOneZoneData();
    InitOneZoneSFR();
    ThisRun.OZUseSNII = 1;
    ThisRun.OZUseSNIa = 1;
    ThisRun.OZUseAGB = 1;
    ThisRun.OZUseNSM = 1;

    CELibSetRunParameterNSMDTDPowerLawIndex(-1.0);
    CELibSetRunParameterNSMDTDOffsetForPower(1.e+9);
    CELibInit();

    SystemUpdate();
    ShowLog();
    cpgslw(4);
    cpgsls(9);
    PlotEu(8,ThisRun.OZPlotType);
    ReleaseOneZoneData();

    ////////////////////////////////////////////////////////

    cpgsci(1);
    cpgsch(1.7);
    cpgsls(1);

    cpgslw(4);
    char Label[][MaxCharactersInLine] = 
        {"p\\dNSM\\u=-1","p\\dNSM\\u=-2","p\\dNSM\\u=-0.5",
        "p\\dNSM\\u=-1 (\\gt\\dNSM,min\\u=10\\u7\\dyr)",
        "p\\dNSM\\u=-1 (\\gt\\dNSM,min\\u=10\\u9\\dyr)"};
    //int Colors[] = {4,3,2,1};
    int Colors[] = {1,2,3,4,8};
    int LS[] = {1,2,3,4,9};
    for(int i=0;i<5;i++){
        float y_ = 2.2-0.33*i;
        cpgsci(Colors[i]);
        cpgsch(1.6);
        if(ThisRun.OZPlotType == 0){
            cpgptxt(-3.5,y_,0.0,0.0,Label[i]);
            cpgsch(2.0);
            float xl = -3.7;
            float yl = y_+0.05;
            cpgpt1(xl,yl,1);
        } else {
            cpgptxt(-3.0,y_,0.0,0.0,Label[i]);
            cpgsch(2.0);
            float xl[] = {-3.7,-3.1};
            float yl[] = {y_+0.05,y_+0.05};
            cpgsls(LS[i]);
            cpgline(2,xl,yl);
        }
    }
    ClosePGPlot();
    //////////////////////////////////////////////////////

    CELibLoadRunParameter();

    return ;
}


static double CC_SolarFeH(void){
    double H = CELibGetElementNumberDensitySolar(CELibYield_H);
    double Fe = CELibGetElementNumberDensitySolar(CELibYield_Fe);
    return (Fe/H);
}

static double CC_FeH(const int Index){
    double H = OneZoneData[Index].StellarElements[CELibYield_H]/CELibGetElementWeight(CELibYield_H);
    double Fe = OneZoneData[Index].StellarElements[CELibYield_Fe]/CELibGetElementWeight(CELibYield_Fe);
    return (Fe/H);
}

static double CC_SolarCFe(void){
    double C = CELibGetElementNumberDensitySolar(CELibYield_C);
    double Fe = CELibGetElementNumberDensitySolar(CELibYield_Fe);
    return (C/Fe);
}

static double CC_CFe(const int Index){
    double C = OneZoneData[Index].StellarElements[CELibYield_C]/CELibGetElementWeight(CELibYield_C);
    double Fe = OneZoneData[Index].StellarElements[CELibYield_Fe]/CELibGetElementWeight(CELibYield_Fe);
    return (C/Fe);
}

static double CC_SolarNFe(void){
    double N = CELibGetElementNumberDensitySolar(CELibYield_N);
    double Fe = CELibGetElementNumberDensitySolar(CELibYield_Fe);
    return (N/Fe);
}

static double CC_NFe(const int Index){
    double N = OneZoneData[Index].StellarElements[CELibYield_N]/CELibGetElementWeight(CELibYield_N);
    double Fe = OneZoneData[Index].StellarElements[CELibYield_Fe]/CELibGetElementWeight(CELibYield_Fe);
    return (N/Fe);
}

static double CC_SolarOFe(void){
    double O = CELibGetElementNumberDensitySolar(CELibYield_O);
    double Fe = CELibGetElementNumberDensitySolar(CELibYield_Fe);
    return (O/Fe);
}

static double CC_OFe(const int Index){
    double O = OneZoneData[Index].StellarElements[CELibYield_O]/CELibGetElementWeight(CELibYield_O);
    double Fe = OneZoneData[Index].StellarElements[CELibYield_Fe]/CELibGetElementWeight(CELibYield_Fe);
    return (O/Fe);
}

static double CC_SolarNeFe(void){
    double Ne = CELibGetElementNumberDensitySolar(CELibYield_Ne);
    double Fe = CELibGetElementNumberDensitySolar(CELibYield_Fe);
    return (Ne/Fe);
}

static double CC_NeFe(const int Index){
    double Ne = OneZoneData[Index].StellarElements[CELibYield_Ne]/CELibGetElementWeight(CELibYield_Ne);
    double Fe = OneZoneData[Index].StellarElements[CELibYield_Fe]/CELibGetElementWeight(CELibYield_Fe);
    return (Ne/Fe);
}

static double CC_SolarMgFe(void){
    double Mg = CELibGetElementNumberDensitySolar(CELibYield_Mg);
    double Fe = CELibGetElementNumberDensitySolar(CELibYield_Fe);
    return (Mg/Fe);
}

static double CC_MgFe(const int Index){
    double Mg = OneZoneData[Index].StellarElements[CELibYield_Mg]/CELibGetElementWeight(CELibYield_Mg);
    double Fe = OneZoneData[Index].StellarElements[CELibYield_Fe]/CELibGetElementWeight(CELibYield_Fe);
    return (Mg/Fe);
}

static double CC_SolarSiFe(void){
    double Si = CELibGetElementNumberDensitySolar(CELibYield_Si);
    double Fe = CELibGetElementNumberDensitySolar(CELibYield_Fe);
    return (Si/Fe);
}

static double CC_SiFe(const int Index){
    double Si = OneZoneData[Index].StellarElements[CELibYield_Si]/CELibGetElementWeight(CELibYield_Si);
    double Fe = OneZoneData[Index].StellarElements[CELibYield_Fe]/CELibGetElementWeight(CELibYield_Fe);
    return (Si/Fe);
}

static double CC_SolarSFe(void){
    double S = CELibGetElementNumberDensitySolar(CELibYield_S);
    double Fe = CELibGetElementNumberDensitySolar(CELibYield_Fe);
    return (S/Fe);
}

static double CC_SFe(const int Index){
    double S = OneZoneData[Index].StellarElements[CELibYield_S]/CELibGetElementWeight(CELibYield_S);
    double Fe = OneZoneData[Index].StellarElements[CELibYield_Fe]/CELibGetElementWeight(CELibYield_Fe);
    return (S/Fe);
}

static double CC_SolarCaFe(void){
    double Ca = CELibGetElementNumberDensitySolar(CELibYield_Ca);
    double Fe = CELibGetElementNumberDensitySolar(CELibYield_Fe);
    return (Ca/Fe);
}

static double CC_CaFe(const int Index){
    double Ca = OneZoneData[Index].StellarElements[CELibYield_Ca]/CELibGetElementWeight(CELibYield_Ca);
    double Fe = OneZoneData[Index].StellarElements[CELibYield_Fe]/CELibGetElementWeight(CELibYield_Fe);
    return (Ca/Fe);
}

static double CC_SolarNiFe(void){
    double Ni = CELibGetElementNumberDensitySolar(CELibYield_Ni);
    double Fe = CELibGetElementNumberDensitySolar(CELibYield_Fe);
    return (Ni/Fe);
}

static double CC_NiFe(const int Index){
    double Ni = OneZoneData[Index].StellarElements[CELibYield_Ni]/CELibGetElementWeight(CELibYield_Ni);
    double Fe = OneZoneData[Index].StellarElements[CELibYield_Fe]/CELibGetElementWeight(CELibYield_Fe);
    return (Ni/Fe);
}

static double CC_SolarEuFe(void){
    double Eu = CELibGetElementNumberDensitySolar(CELibYield_Eu);
    double Fe = CELibGetElementNumberDensitySolar(CELibYield_Fe);
    return (Eu/Fe);
}

static double CC_EuFe(const int Index){
    double Eu = OneZoneData[Index].StellarElements[CELibYield_Eu]/CELibGetElementWeight(CELibYield_Eu);
    double Fe = OneZoneData[Index].StellarElements[CELibYield_Fe]/CELibGetElementWeight(CELibYield_Fe);
    return (Eu/Fe);
}

static double CC_Gas_log12OH(const int Index){ //
    double O = OneZoneData[Index].Elements[CELibYield_O]/CELibGetElementWeight(CELibYield_O);
    double H = OneZoneData[Index].Elements[CELibYield_H]/CELibGetElementWeight(CELibYield_H);
    if(O == 0) return (-100);
    double logOH = log10(O/H);
    return (12+logOH);
}

static void PlotMetalDistribution(void){

    // FILE *fp;
    char fname[MaxCharactersInLine];
    if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_N13){
        Snprintf(fname,"%s/OZ_MetalDistN",ThisRun.OutDir);
    } else if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_P98){
        Snprintf(fname,"%s/OZ_MetalDistP",ThisRun.OutDir);
    }

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgswin(ThisRun.OZFeHMin,ThisRun.OZFeHMax,ThisRun.OZFracMin,ThisRun.OZFracMax);
    cpgbox("BCTSN",0,0.,"BCTSN",0.0,0.0);

    ////////////////////////
    double dFeH = (ThisRun.OZFeHMax-ThisRun.OZFeHMin)/ThisRun.OZFeHBin;

    ///  Clear Bin;
    float MassBin[ThisRun.OZFeHBin];
    float FeHBin[ThisRun.OZFeHBin];

    for(int i=0;i<ThisRun.OZFeHBin;i++){
        MassBin[i] = 0.e0;
        FeHBin[i] = ThisRun.OZFeHMin+dFeH*i;
    }

    for(int i=1;i<ThisRun.OZTimeBin;i++){
        double mass = OneZoneData[i].MstarBorn-OneZoneData[i].MejectaTotal;
        if(CC_FeH(i)==0.e0) continue;

        double FeH = log10(CC_FeH(i)/CC_SolarFeH());
        int IntFeH = (FeH-ThisRun.OZFeHMin)/dFeH;
        if(IntFeH < 0) continue;
        if(IntFeH >= ThisRun.OZFeHBin) continue;

        MassBin[IntFeH] += mass;

    }

    // Normalized;
    double Mnormalized = 0.e0;
    for(int i=0;i<ThisRun.OZFeHBin;i++){
        Mnormalized += MassBin[i];
    }
    Mnormalized = 1.0/Mnormalized;


    for(int i=0;i<ThisRun.OZFeHBin;i++){

        if(MassBin[i] == 0.e0) continue;

        float x[2] = {ThisRun.OZFeHMin+dFeH*i,ThisRun.OZFeHMin+dFeH*(i+1)};
        float y[2] = {Mnormalized*MassBin[i],Mnormalized*MassBin[i]};

        cpgline(2,x,y);


        float x_l[2] = {ThisRun.OZFeHMin+dFeH*i,ThisRun.OZFeHMin+dFeH*i};
        float y_l[2] = {i==0?0:Mnormalized*MassBin[i-1],Mnormalized*MassBin[i]};
        cpgline(2,x_l,y_l);

        float x_r[2] = {ThisRun.OZFeHMin+dFeH*(i+1),ThisRun.OZFeHMin+dFeH*(i+1)};
        float y_r[2] = {i==ThisRun.OZFeHBin-1?0:Mnormalized*MassBin[i+1],Mnormalized*MassBin[i]};
        cpgline(2,x_r,y_r);
    }


    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.2,0.9);
    cpglab("[Fe/H]","Fraction of stars","");

    ClosePGPlot();


    return ;
}

static void PlotAlphaFe(const int mode){

    char suffix[][MaxCharactersInLine] 
        = {"H","He","C","N","O","Ne","Mg","Si","S","Ca","Fe","Ni","Eu","All"};

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/OZ_MetalDist_%s",ThisRun.OutDir,suffix[mode]);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgswin(ThisRun.OZFeHMin,ThisRun.OZFeHMax,ThisRun.OZAlphaFeMin,ThisRun.OZAlphaFeMax);
    cpgbox("BCTSN",0,0.,"BCTSN",0.0,0.0);

    float FeH[ThisRun.OZTimeBin];
    float AlphaFe[ThisRun.OZTimeBin];
    int counter = 0;
    for(int i=1;i<ThisRun.OZTimeBin;i++){
        if(mode == CELibYield_H){
        } else if(mode == CELibYield_He){
        } else if(mode == CELibYield_C){
            if(CC_CFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_CFe(i)/CC_SolarCFe());
        } else if(mode == CELibYield_N){
            if(CC_NFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_NFe(i)/CC_SolarNFe());
        } else if(mode == CELibYield_O){
            if(CC_OFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_OFe(i)/CC_SolarOFe());
        } else if(mode == CELibYield_Ne){
            if(CC_NeFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_NeFe(i)/CC_SolarNeFe());
        } else if(mode == CELibYield_Mg){
            if(CC_MgFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_MgFe(i)/CC_SolarMgFe());
        } else if(mode == CELibYield_Si){
            if(CC_SiFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_SiFe(i)/CC_SolarSiFe());
        } else if(mode == CELibYield_S){
            if(CC_SFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_SFe(i)/CC_SolarSFe());
        } else if(mode == CELibYield_Ca){
            if(CC_CaFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_CaFe(i)/CC_SolarCaFe());
        } else if(mode == CELibYield_Fe){
            AlphaFe[counter] = 1;
        } else if(mode == CELibYield_Ni){
            if(CC_NiFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_NiFe(i)/CC_SolarNiFe());
        } else if(mode == CELibYield_Eu){
            // if(CC_EuFe(i)==0) continue;
            // AlphaFe[counter] = log10(CC_EuFe(i)/CC_SolarEuFe());
        }
        FeH[counter] = log10(CC_FeH(i)/CC_SolarFeH());
        counter ++;
    }

    cpgpt(counter,FeH,AlphaFe,1);

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.2,0.9);
    char labely[MaxCharactersInLine];
    Snprintf(labely,"[%s/Fe]",suffix[mode]);
    cpglab("[Fe/H]",labely,"");

    ClosePGPlot();

    return ;
}

static void PlotAlphaFeBody(const int mode, const int ColorID, const int Type){

    cpgsvp(0.2,0.95,0.2,0.95);
    char suffix[][MaxCharactersInLine] 
        = {"H","He","C","N","O","Ne","Mg","Si","S","Ca","Fe","Ni","Eu","All"};

    int LS;
    cpgqls(&LS);
    cpgsls(1);
    cpgsci(1);
    int LW;
    cpgqlw(&LW);
    //cpgslw(1);
    cpgslw(4);

    if(mode == CELibYield_N){
        cpgswin(ThisRun.OZFeHMin,ThisRun.OZFeHMax,ThisRun.OZAlphaFeMin,ThisRun.OZAlphaFeMax);
    } else if(mode == CELibYield_Eu){
        cpgswin(ThisRun.OZFeHMin,ThisRun.OZFeHMax,-2.5,2.5);
    } else {
        cpgswin(ThisRun.OZFeHMin,ThisRun.OZFeHMax,ThisRun.OZAlphaFeMin,ThisRun.OZAlphaFeMax);
    }
    //cpgslw(2);
    cpgslw(4);
    cpgbox("BCTSN",0,0.,"BCTSN",0.0,0.0);

    float FeH[ThisRun.OZTimeBin];
    float AlphaFe[ThisRun.OZTimeBin];
    int counter = 0;
    for(int i=1;i<ThisRun.OZTimeBin;i+=ThisRun.OZPlotStep){
        if(mode == CELibYield_H){
        } else if(mode == CELibYield_He){
        } else if(mode == CELibYield_C){
            if(CC_CFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_CFe(i)/CC_SolarCFe());
        } else if(mode == CELibYield_N){
            if(CC_NFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_NFe(i)/CC_SolarNFe());
        } else if(mode == CELibYield_O){
            if(CC_OFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_OFe(i)/CC_SolarOFe());
        } else if(mode == CELibYield_Ne){
            if(CC_NeFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_NeFe(i)/CC_SolarNeFe());
        } else if(mode == CELibYield_Mg){
            if(CC_MgFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_MgFe(i)/CC_SolarMgFe());
        } else if(mode == CELibYield_Si){
            if(CC_SiFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_SiFe(i)/CC_SolarSiFe());
        } else if(mode == CELibYield_S){
            if(CC_SFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_SFe(i)/CC_SolarSFe());
        } else if(mode == CELibYield_Ca){
            if(CC_CaFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_CaFe(i)/CC_SolarCaFe());
        } else if(mode == CELibYield_Fe){
            AlphaFe[counter] = 1;
        } else if(mode == CELibYield_Ni){
            if(CC_NiFe(i)==0) continue;
            AlphaFe[counter] = log10(CC_NiFe(i)/CC_SolarNiFe());
        } else if(mode == CELibYield_Eu){
            if(ThisRun.OZShowEuFe == 1){
                if(CC_EuFe(i)==0) continue;
                AlphaFe[counter] = log10(CC_EuFe(i)/CC_SolarEuFe());
            }
        }
        FeH[counter] = log10(CC_FeH(i)/CC_SolarFeH());
        counter ++;
    }

    cpgsci(ColorID);
    if(1){
        if(Type == 0){
            cpgpt(counter,FeH,AlphaFe,1);
        } else {
            cpgsls(LS);
            cpgslw(LW);
            cpgline(counter,FeH,AlphaFe);
            cpgsls(1);
            cpgslw(1);
        }
    }

    cpgsci(1);
    cpgsls(1);
    cpgslw(2);
    cpgslw(4);
    cpgsvp(0.2,0.95,0.23,0.95);
    char labely[MaxCharactersInLine];
    Snprintf(labely,"[%s/Fe]",suffix[mode]);
    cpglab("[Fe/H]",labely,"");

    return ;
}

static void PlotAllFe(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/OZ_MetalDist_All",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsubp(3,3);

    cpgpanl(1,1);
    PlotAlphaFeBody(CELibYield_C,1,ThisRun.OZPlotType);
    cpgpanl(2,1);
    PlotAlphaFeBody(CELibYield_N,1,ThisRun.OZPlotType);
    cpgpanl(3,1);
    PlotAlphaFeBody(CELibYield_O,1,ThisRun.OZPlotType);
    cpgpanl(1,2);
    PlotAlphaFeBody(CELibYield_Mg,1,ThisRun.OZPlotType);
    cpgpanl(2,2);
    PlotAlphaFeBody(CELibYield_Si,1,ThisRun.OZPlotType);
    cpgpanl(3,2);
    PlotAlphaFeBody(CELibYield_S,1,ThisRun.OZPlotType);
    cpgpanl(1,3);
    PlotAlphaFeBody(CELibYield_Ca,1,ThisRun.OZPlotType);
    cpgpanl(2,3);
    PlotAlphaFeBody(CELibYield_Ni,1,ThisRun.OZPlotType);
    cpgpanl(3,3);
    PlotAlphaFeBody(CELibYield_Eu,1,ThisRun.OZPlotType);

    ClosePGPlot();

    return ;
}

static void PlotMassEvolution(const int mode){

    float Mass[4][ThisRun.OZTimeBin];

    float Age[ThisRun.OZTimeBin];
    int counter = 0;
    for(int i=0;i<ThisRun.OZTimeBin;i+=ThisRun.OZPlotStep){
        Age[counter] = OneZoneData[i].Time*1.e-9; // to be giga year.

        Mass[0][counter] = (OneZoneData[i].Mgas>0)?log10(OneZoneData[i].Mgas):-10;
        Mass[1][counter] = (OneZoneData[i].Mstar>0)?log10(OneZoneData[i].Mstar):-10;
        Mass[2][counter] = (OneZoneData[i].MstarBorn>0)?log10(OneZoneData[i].MstarBorn):-10;

        counter ++;
    }

    cpgsci(mode); cpgsls(mode);
    cpgline(counter,Age,Mass[1]);

    cpgslw(2);
    return ;
}

static void PlotMassEvolutionTwo(const int CL, const int LS){

    float Mass[4][ThisRun.OZTimeBin];

    float Age[ThisRun.OZTimeBin];
    int counter = 0;
    for(int i=0;i<ThisRun.OZTimeBin;i+=ThisRun.OZPlotStep){
        Age[counter] = OneZoneData[i].Time*1.e-9; // to be giga year.

        Mass[0][counter] = (OneZoneData[i].Mgas>0)?log10(OneZoneData[i].Mgas):-10;
        Mass[1][counter] = (OneZoneData[i].Mstar>0)?log10(OneZoneData[i].Mstar):-10;
        Mass[2][counter] = (OneZoneData[i].MstarBorn>0)?log10(OneZoneData[i].MstarBorn):-10;

        counter ++;
    }


    cpgsci(CL); cpgsls(LS);
    cpgline(counter,Age,Mass[1]);

    cpgslw(2);
    return ;
}

static void MassMetallicityRelation(void){

    cpgsci(2);

    double m_min = 8.5;
    double m_max = 11.5;
    
    int Nbin = 20;

    float x[Nbin];
    float y[Nbin];
    double dm = (m_max-m_min)/Nbin;
    for(int i=0;i<Nbin;i++){
        double m = m_min+dm*i;
        
        x[i] = m;
        y[i] = -1.492+1.837*m-0.08026*m*m;
    }

    cpgline(Nbin,x,y);

    cpgsci(1);

    return ;
}

static void PlotMassMetallicity(const int mode){

    char fname[MaxCharactersInLine];
    if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_N13){
        Snprintf(fname,"%s/OZ_MassZN",ThisRun.OutDir);
    } else if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_P98){
        Snprintf(fname,"%s/OZ_MassZP",ThisRun.OutDir);
    }

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgswin(ThisRun.OZMstarMin,ThisRun.OZMstarMax,ThisRun.OZ12OHMin,ThisRun.OZ12OHMax);
    cpgbox("BCTSN",0,0.,"BCTSN",0.0,0.0);

    MassMetallicityRelation();

    float Mstar[ThisRun.OZTimeBin];
    float logOH[ThisRun.OZTimeBin];
    int counter = 0;
    for(int i=1;i<ThisRun.OZTimeBin;i+=ThisRun.OZPlotStep){
        logOH[counter] = CC_Gas_log12OH(i);
        Mstar[counter] = log10(OneZoneData[i].Mstar);
        counter ++;
    }

    cpgpt(counter,Mstar,logOH,1);



    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.2,0.9);
    cpglab("Mass [M\\d\\(2281)\\u]","12+log\\d10\\u(O/H)","");

    ClosePGPlot();

    return ;
}

static void PlotOneZoneResults(void){

    // Metallicity distribution function.
    PlotMetalDistribution();

    PlotAlphaFe(CELibYield_Mg);
    PlotAlphaFe(CELibYield_O);

    PlotAllFe();

    PlotMassMetallicity(0);

    return ;
}


static void PlotMultiModelsBody(const int ColorID){

    cpgsubp(3,3);
    int LS;
    cpgqls(&LS);

    cpgpanl(1,1);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_C,ColorID,ThisRun.OZPlotType);
    cpgpanl(2,1);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_N,ColorID,ThisRun.OZPlotType);
    cpgpanl(3,1);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_O,ColorID,ThisRun.OZPlotType);
    cpgpanl(1,2);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_Mg,ColorID,ThisRun.OZPlotType);
    cpgpanl(2,2);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_Si,ColorID,ThisRun.OZPlotType);
    cpgpanl(3,2);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_S,ColorID,ThisRun.OZPlotType);
    cpgpanl(1,3);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_Ca,ColorID,ThisRun.OZPlotType);
    cpgpanl(2,3);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_Ni,ColorID,ThisRun.OZPlotType);
    if(ThisRun.OZShowEuFe == 1){
        cpgpanl(3,3);
        cpgslw(2);
        cpgsls(LS);
        PlotAlphaFeBody(CELibYield_Eu,ColorID,ThisRun.OZPlotType);
    }

    return ;
}

static void PlotMultiModelsBody10(const int ColorID){

    cpgsubp(4,4);
    int LS;
    cpgqls(&LS);

    cpgpanl(1,1);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_C,ColorID,ThisRun.OZPlotType);
    cpgpanl(2,1);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_N,ColorID,ThisRun.OZPlotType);
    cpgpanl(3,1);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_O,ColorID,ThisRun.OZPlotType);
    cpgpanl(4,1);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_Ne,ColorID,ThisRun.OZPlotType);
    cpgpanl(1,2);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_Mg,ColorID,ThisRun.OZPlotType);
    cpgpanl(2,2);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_Si,ColorID,ThisRun.OZPlotType);
    cpgpanl(3,2);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_S,ColorID,ThisRun.OZPlotType);
    cpgpanl(4,2);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_Ca,ColorID,ThisRun.OZPlotType);
    cpgpanl(1,3);
    cpgslw(2);
    cpgsls(LS);
    PlotAlphaFeBody(CELibYield_Ni,ColorID,ThisRun.OZPlotType);
    if(ThisRun.OZShowEuFe == 1){
        cpgpanl(2,3);
        cpgslw(2);
        cpgsls(LS);
        PlotAlphaFeBody(CELibYield_Eu,ColorID,ThisRun.OZPlotType);
    }

    return ;
}

static void PlotMetalDistributionBody(const int mode){


    ////////////////////////
    double dFeH = (ThisRun.OZFeHMax-ThisRun.OZFeHMin)/ThisRun.OZFeHBin;

    ///  Clear Bin;
    float MassBin[ThisRun.OZFeHBin];
    float FeHBin[ThisRun.OZFeHBin];

    for(int i=0;i<ThisRun.OZFeHBin;i++){
        MassBin[i] = 0.e0;
        FeHBin[i] = ThisRun.OZFeHMin+dFeH*i;
    }

    for(int i=1;i<ThisRun.OZTimeBin;i++){
        double mass = OneZoneData[i].MstarBorn-OneZoneData[i].MejectaTotal;

        if(CC_FeH(i)==0.e0) continue;

        double FeH = log10(CC_FeH(i)/CC_SolarFeH());
        int IntFeH = (FeH-ThisRun.OZFeHMin)/dFeH;
        if(IntFeH < 0) continue;
        if(IntFeH >= ThisRun.OZFeHBin) continue;

        MassBin[IntFeH] += mass;
    }

    // Normalized;
    double Mnormalized = 0.e0;
    for(int i=0;i<ThisRun.OZFeHBin;i++){
        Mnormalized += MassBin[i];
    }
    Mnormalized = 1.0/Mnormalized;

    if(mode == 1){
        cpgsls(1);
    } else {
        cpgsls(2);
    }
    for(int i=0;i<ThisRun.OZFeHBin;i++){

        if(MassBin[i] == 0.e0) continue;

        float x[2] = {ThisRun.OZFeHMin+dFeH*i,ThisRun.OZFeHMin+dFeH*(i+1)};
        float y[2] = {Mnormalized*MassBin[i],Mnormalized*MassBin[i]};

        cpgline(2,x,y);


        float x_l[2] = {ThisRun.OZFeHMin+dFeH*i,ThisRun.OZFeHMin+dFeH*i};
        float y_l[2] = {i==0?0:Mnormalized*MassBin[i-1],Mnormalized*MassBin[i]};
        cpgline(2,x_l,y_l);

        float x_r[2] = {ThisRun.OZFeHMin+dFeH*(i+1),ThisRun.OZFeHMin+dFeH*(i+1)};
        float y_r[2] = {i==ThisRun.OZFeHBin-1?0:Mnormalized*MassBin[i+1],Mnormalized*MassBin[i]};
        cpgline(2,x_r,y_r);
    }
    cpgsls(1);


    return ;
}
