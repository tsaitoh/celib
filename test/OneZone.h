/* ./OneZone.c */
void OneZoneModel(void);
void OneZoneModelMultiModels(void);
void OneZoneModelMultiModelsMassEvolution(void);
void OneZoneModelMultiModelsMetalDistribution(void);
void OneZoneModelMultiModelsNiComparison(void);
void OneZoneModelMultiModelsMgComparison(void);
void OneZoneModelMultiModelsCComparison(void);
void OneZoneModelMultiModelsAGBComparison(void);
void OneZoneModelMultiModelsEuComparison(void);
