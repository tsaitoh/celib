#include "config.h"
#include <cpgplot.h>

void OpenPGPlot(char WriteFileName[], const int Current_WRITE_MODE){

    char fname[MaxCharactersInLine];
    if(Current_WRITE_MODE == 0){
        sprintf(fname,"/XSERVE");
    } else if(Current_WRITE_MODE == 1){
        Snprintf(fname,"./%s.eps/vcps",WriteFileName);
    } else if(Current_WRITE_MODE == 2){
        Snprintf(fname,"./%s.png/png",WriteFileName);
    } else {
        sprintf(fname,"?");
    }

    if (cpgopen(fname) <1){
        fprintf(stderr, "can't open X window.\n");
        exit(1);
    }
    //cpgpage();

    ResetColors();

    return ;
}

void OpenPGPlotWithID(char WriteFileName[], const int FileID, const int Current_WRITE_MODE){

    char fname[MaxCharactersInLine];
    if(Current_WRITE_MODE == 0){
        sprintf(fname,"/XSERVE");
    } else if(Current_WRITE_MODE == 1){
        Snprintf(fname,"./%s.%04d.eps/vcps",WriteFileName,FileID);
    } else if(Current_WRITE_MODE == 2){
        Snprintf(fname,"./%s.%04d.png/png",WriteFileName,FileID);
    } else {
        sprintf(fname,"?");
    }

    if (cpgopen(fname) <1){
        fprintf(stderr, "can't open X window.\n");
        exit(1);
    }
    cpgpage();

    return ;
}


void ClosePGPlot(void){
    cpgclos();
    return;
}

