#include "config.h"
#include "PGPlotOperations.h"
#include "../src/AGBMassLoss.h"
#include <cpgplot.h>

extern struct CELibStructAGBYields *CELibAGBYieldsIntegrated;

static void PlotXlabel(void){

    char label[][MaxCharactersInLine] ={"H","He","C","N","O","Ne","Mg","Si","S","Ca","Fe","Ni"};

    for(int i=0;i<12;i++){
        cpgptxt(i+1,-10.0,45,0.5,label[i]);
    }

    return; 
}


void PlotAGBYieldsMass(void){

    // FILE *fp;
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/AGBMassLoss",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgswin(0,13,-7.5,0);
    cpgbox("BCTS",0,0.,"BCTSNL2",0.0,0.0);

    cpgsch(1.5);
    PlotXlabel();
    cpgsch(2);

    char label[][MaxCharactersInLine] ={"Z=0.0001","Z=0.004","Z=0.008","Z=0.02"};

    //int TableSize = sizeof(TableID)/sizeof(int);
    for(int i=0;i<4;i++){
        float x[CELibYield_Number];
        float y[CELibYield_Number];
        for(int k=0;k<CELibYield_Number;k++){
            x[k] = k+1;
            y[k] = CELibAGBYieldsIntegrated[i].Elements[k];
            if(y[k] > 0.e0) y[k] = log10(y[k]);
            if(k >= CELibYield_Number-2) x[k]++;
        }
        cpgsci(i+1);
        cpgsls(i+1);
        //cpgline(TableSize,x,y);
        cpgline(CELibYield_Number-2,x,y);
        cpgline(2,x+CELibYield_Number-2,y+CELibYield_Number-2);

        //float y_ = -0.5-0.35*i;
        float y_ = -0.5-0.4*i;
        cpgptxt(8.5,y_,0.0,0.0,label[i]);
        float xl[2] = {7,8};
        float yl[2] = {y_+0.1,y_+0.1};
        cpgline(2,xl,yl);

    }

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.2,0.9);
    cpglab("Elements","Mass [M\\d\\(2281)\\u]","");


    ClosePGPlot();


    return ;
}


void PlotAGBIntegratedYieldsPop12(const int TableID){


    CELibRunParameters.AGBYieldsTableID = TableID;
    CELibRunParameters.PopIIIAGB = 1;
    CELibRunParameters.PopIIIAGBYieldsTableID = CELibAGBZ0YieldsTableID_CL08G13;
    CELibRunParameters.PopIIIIMF = 1;
    CELibInit();

    // FILE *fp;
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/AGBMassLossPop12.%d",ThisRun.OutDir,CELibRunParameters.AGBYieldsTableID);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgslw(4);
    cpgswin(0,13,-9.5,0);
    //cpgswin(0,13,-7.5,+1);
    cpgbox("BCTS",0,0.,"BCTSNL2",0.0,0.0);

    cpgsch(1.5);
    PlotXlabel();
    cpgsch(2);

    char label[][MaxCharactersInLine] ={"Z=0.0001","Z=0.004","Z=0.008","Z=0.02"};
    int CI[] = {1,2,3,4,8};

    //int TableSize = sizeof(TableID)/sizeof(int);
    for(int i=0;i<4;i++){
#if 1
        float x_p[CELibYield_Number];
        float x_m[CELibYield_Number];
        float y_p[CELibYield_Number];
        float y_m[CELibYield_Number];

        int counter_p = 0;
        int counter_m = 0;

        //cpgsci(i+1);
        cpgsci(CI[i]);
        cpgsls(i+1);

        double xshift = -0.2;
        double dx = 0.1;

        for(int k=0;k<CELibYield_Number;k++){

            //double x = k+1;
            double x = k+1+i*dx+xshift;
            double y = CELibAGBYieldsIntegrated[i+1].Elements[k];

            if(y>0){
                x_p[counter_p] = x;
                y_p[counter_p] = log10(y);
                //if(k >= CELibYield_Number-2) x_p[counter_p] ++;
                //cpgsfs(1);
                //cpgcirc(x_p[counter_p],y_p[counter_p],0.2);
                counter_p ++;
            } else {
                x_m[counter_m] = x;
                y_m[counter_m] = log10(fabs(y));
                //if(k >= CELibYield_Number-2) x_m[counter_m] ++;
                //cpgsfs(2);
                //cpgcirc(x_m[counter_m],y_m[counter_m],0.2);
                counter_m ++;
            }

        }
        cpgsch(2);
        cpgpt(counter_p,x_p,y_p,4);
        cpgpt(counter_m,x_m,y_m,-8);
        cpgsfs(1);

        //cpgsch(1.3);
        cpgsch(1.5);
        float y_ = -0.7-0.5*i;
        cpgptxt(8.0,y_,0.0,0.0,label[i]);
        cpgsch(2.0);
        cpgpt1(7.5,y_+0.15,4);
        //float xl[2] = {7,8};
        //float yl[2] = {y_+0.1,y_+0.1};
        //cpgline(2,xl,yl);
#else
        float x[TableSize];
        float y[TableSize];
        for(int k=0;k<TableSize;k++){
            x[k] = k+1;
            y[k] = CELibIntegratedAGBMassLoss[i].Elements[TableID[k]];

            y[k] = fabs(y[k]);

            if(y[k] > 0.e0) y[k] = log10(y[k]);
            if(k >= TableSize-2) x[k]++;
        }
        cpgsci(i+1);
        cpgsls(i+1);
        //cpgline(TableSize,x,y);
        cpgline(TableSize-2,x,y);
        cpgline(2,x+TableSize-2,y+TableSize-2);

        //float y_ = -0.5-0.35*i;
        float y_ = -0.5-0.4*i;
        cpgptxt(8.5,y_,0.0,0.0,label[i]);
        float xl[2] = {7,8};
        float yl[2] = {y_+0.15,y_+0.1};
        cpgline(2,xl,yl);
#endif

    }

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Elements","Mass [M\\d\\(2281)\\u]","");


    ClosePGPlot();

    return ;
}

void PlotAGBIntegratedYieldsPopIII(void){

    CELibRunParameters.AGBYieldsTableID = CELibAGBYieldsTableID_K10D14;
    CELibRunParameters.PopIIIAGB = 1;
    CELibRunParameters.PopIIIAGBYieldsTableID = CELibAGBZ0YieldsTableID_CL08;
    CELibRunParameters.PopIIIIMF = 1;
    CELibInit();

    // FILE *fp;
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/AGBMassLossPopIII",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgslw(4);
    cpgswin(0,13,-9.5,0);
    //cpgswin(0,13,-7.5,+1);
    cpgbox("BCTS",0,0.,"BCTSNL2",0.0,0.0);

    cpgsch(1.5);
    PlotXlabel();
    cpgsch(2);

    int NZ;
    char label[][MaxCharactersInLine] ={"Campbell&Lattanzio 08","Campbell&Lattanzio 08","        +Gil-Pons+13"};

    int CI[] = {1,2,3,4,8};
    {
        float x_p[CELibYield_Number];
        float x_m[CELibYield_Number];
        float y_p[CELibYield_Number];
        float y_m[CELibYield_Number];

        int counter_p = 0;
        int counter_m = 0;

        cpgsci(1);
        cpgsls(1);

        double xshift = -0.05;
        double dx = 0.1;

        for(int k=0;k<CELibYield_Number;k++){

            //double x = k+1;
            double x = k+1+0*dx+xshift;
            double y = CELibAGBYieldsIntegrated[0].Elements[k];

            if(y>0){
                x_p[counter_p] = x;
                y_p[counter_p] = log10(y);
                counter_p ++;
            } else {
                x_m[counter_m] = x;
                y_m[counter_m] = log10(fabs(y));
                counter_m ++;
            }

        }
        cpgsch(2);
        cpgpt(counter_p,x_p,y_p,4);
        cpgpt(counter_m,x_m,y_m,-8);
        cpgsfs(1);

        //cpgsch(1.3);
        cpgsch(1.5);
        float y_ = -0.7-0.5*0;
        cpgptxt(4.0,y_,0.0,0.0,label[0]);
        cpgsch(2.0);
        cpgpt1(3.5,y_+0.15,4);
    }

    CELibRunParameters.AGBYieldsTableID = CELibAGBYieldsTableID_K10D14;
    CELibRunParameters.PopIIIAGB = 1;
    CELibRunParameters.PopIIIAGBYieldsTableID = CELibAGBZ0YieldsTableID_CL08G13;
    CELibRunParameters.PopIIIIMF = 1;
    CELibInit();

    {
        float x_p[CELibYield_Number];
        float x_m[CELibYield_Number];
        float y_p[CELibYield_Number];
        float y_m[CELibYield_Number];

        int counter_p = 0;
        int counter_m = 0;

        cpgsci(2);
        cpgsls(1);

        double xshift = -0.025;
        double dx = 0.05;

        for(int k=0;k<CELibYield_Number;k++){

            double x = k+1+1*dx+xshift;
            double y = CELibAGBYieldsIntegrated[0].Elements[k];

            if(y>0){
                x_p[counter_p] = x;
                y_p[counter_p] = log10(y);
                counter_p ++;
            } else {
                x_m[counter_m] = x;
                y_m[counter_m] = log10(fabs(y));
                counter_m ++;
            }

        }
        cpgsch(2);
        cpgpt(counter_p,x_p,y_p,4);
        cpgpt(counter_m,x_m,y_m,-8);
        cpgsfs(1);

        //cpgsch(1.3);
        cpgsch(1.5);
        float y_ = -0.7-0.5*1;
        cpgptxt(4.0,y_,0.0,0.0,label[1]);
        cpgptxt(4.0,y_-0.5,0.0,0.0,label[2]);
        cpgsch(2.0);
        cpgpt1(3.5,y_+0.15,4);
    }

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Elements","Mass [M\\d\\(2281)\\u]","");


    ClosePGPlot();

    return ;
}

void PlotAGBReturnMassFractionOneModel(void){

    // FILE *fp;
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/AGBMassLossEr.%d",ThisRun.OutDir,CELibRunParameters.AGBYieldsTableID);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgslw(4);
    cpgswin(7,log10(1.38e+10),0,0.3);
    //cpgswin(0,13,-7.5,+1);
    cpgbox("BCTSLN2",0,0.,"BCTSN",0.0,0.0);

    cpgsch(2.0);
    PlotXlabel();
    cpgsch(2);
    cpgslw(4);

    char label[][MaxCharactersInLine] ={"Z=0","Z=0.0001","Z=0.004","Z=0.008","Z=0.02"};
    double Z[] = {0,0.0001,0.004,0.008,0.02};
    int CI[] = {1,2,3,4,8};

#define Nstep (100)
    for(int i=0;i<5;i++){

        float x[Nstep];
        float y[Nstep];

        //double dT = 1.4e+10/Nstep;
        double tstart = CELibGetLifeTimeofStar(100.0,Z[i]);
        double tend = 1.38e+10;
        double dT = (log10(tend)-log10(tstart))/Nstep;
        // gprintl(tstart);
        // exit(1);
        for(int k=0;k<Nstep;k++){
            //double tend = dT*(k+1);
            double tend = pow(10.0,dT*(k+1)+log10(tstart));
            x[k] = tend;
            y[k] = CELibGetAGBYieldsReturnMassInGivenTimeRange(tstart,fmax(tstart,tend),Z[i]);
            // gprintl(y[k]);
            // gprintl(x[k]);
            x[k] = log10(x[k]);
        }
        //cpgsci(i+1);
        cpgsci(CI[i]);
        cpgsls(i+1);

        cpgline(Nstep,x,y);

        //cpgsch(1.3);
        cpgsch(1.5);
        float y_ = 0.28-0.015*i;
        cpgptxt(7.6,y_,0.0,0.0,label[i]);
        cpgsch(2.0);
        float xl[2] = {7.2,7.5};
        float yl[2] = {y_+0.005,y_+0.005};
        cpgline(2,xl,yl);

    }

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Age [Year]","Cumulative return mass fraction","");


    ClosePGPlot();

    return ;
}

void PlotAGBReturnMassFraction(void){
    CELibSaveRunParameter();
    CELibSetRunParameterAGBYieldsTableID(CELibAGBYieldsTableID_K10);
    CELibInit();
    PlotAGBReturnMassFractionOneModel();

    CELibSetRunParameterAGBYieldsTableID(CELibAGBYieldsTableID_K10D14);
    CELibInit();
    PlotAGBReturnMassFractionOneModel();

    CELibLoadRunParameter();
    return ;
}

void PlotAGBIntegratedYields(void){

    CELibSaveRunParameter();

    PlotAGBIntegratedYieldsPop12(CELibAGBYieldsTableID_K10);
    PlotAGBIntegratedYieldsPop12(CELibAGBYieldsTableID_K10D14);
    PlotAGBIntegratedYieldsPopIII();

    CELibLoadRunParameter();
    return ;
}
static void PlotLinelabel(void){

    char label[][MaxCharactersInLine] ={"H","He","C","N","O","Ne","Mg","Si","S","Fe","Ni"};

    cpgsch(2.0);
    cpgsvp(0.0,1.0,0.3,0.9);

    cpgsls(1);
    cpgswin(0,1,0,1);
    cpgbox("",0,0.,"",0.0,0.0);

    // Palett # 11
    int symbol[] = {2,4,5,3,6,7,10,11,12,13,15};

    //palett(24,1,0.5);
    int MinInd,MaxInd;
    cpgqcir(&MinInd,&MaxInd);
    int CurrentColorNumber = (MaxInd-MinInd);
    float dcolor = 11.0/(float)CurrentColorNumber;

    cpgsch(1.5);
    for(int i=0;i<11;i++){
        int color_table = i/dcolor;
        if(color_table >= CurrentColorNumber)
            color_table = CurrentColorNumber-1;

        cpgsci(color_table+MinInd);
        dprint(color_table);

        float x[] = {0.82,0.87};
        float y[] = {0.8+0.02-0.07*i,0.8+0.02-0.07*i};
        cpgline(2,x,y);
        cpgptxt(0.9,0.8-0.07*i,0,0,label[i]);

        float _x = 0.845;
        float _y = 0.8+0.02-0.07*i;
        cpgpt1(_x,_y,symbol[i]);
    }
    cpgsci(1);

    return; 
}
