#include "config.h"
#include "RandomNumberGenerator.h"

static void RunSimulation(void);
static void SNIIFeedbackTest(void);
static void SNIIFeedbackTest2(void);
static void SNIaYieldTest(void);


static void CheckSNIIFeedback(void){

    double Zmin = 1.e-7; 
    double Zmax = 0.02;
    int IntZ = 10;

    double Mmin = 0.1; 
    double Mmax = 10;
    int IntM = 10;

    double Mass = 1.0;
    double Z = 0.0;
    double MassConversionFactor = 1.0;
    double Elements[CELibYield_Number];

    struct CELibStructFeedbackOutput SNII = CELibGetSNIIFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Z,
                .MassConversionFactor = MassConversionFactor,
                .Elements = Elements,
                });
    fprintf(stderr,"Energy = %g (%g,%g)\n",SNII.Energy,Mass,Z);
    fprintf(stderr,"Mr,Me = %g, %g\n",SNII.RemnantMass,SNII.EjectaMass);

    Z = CELibRunParameters.PopIIIMetallicity;
    SNII = CELibGetSNIIFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Z,
                .MassConversionFactor = MassConversionFactor,
                .Elements = Elements,
                });
    fprintf(stderr,"Energy = %g (%g,%g)\n",SNII.Energy,Mass,Z);
    fprintf(stderr,"Mr,Me = %g, %g\n",SNII.RemnantMass,SNII.EjectaMass);

    Z = 1.2*CELibRunParameters.PopIIIMetallicity;
    SNII = CELibGetSNIIFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Z,
                .MassConversionFactor = MassConversionFactor,
                .Elements = Elements,
                });
    fprintf(stderr,"Energy = %g (%g,%g)\n",SNII.Energy,Mass,Z);
    fprintf(stderr,"Mr,Me = %g, %g\n",SNII.RemnantMass,SNII.EjectaMass);

    Z = 0.02;
    SNII = CELibGetSNIIFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Z,
                .MassConversionFactor = MassConversionFactor,
                .Elements = Elements,
                });
    fprintf(stderr,"Energy = %g (%g,%g)\n",SNII.Energy,Mass,Z);
    fprintf(stderr,"Mr,Me = %g, %g\n",SNII.RemnantMass,SNII.EjectaMass);

    Z = 0.0; Mass = 10;
    SNII = CELibGetSNIIFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Z,
                .MassConversionFactor = MassConversionFactor,
                .Elements = Elements,
                });
    fprintf(stderr,"Energy = %g (%g,%g)\n",SNII.Energy,Mass,Z);
    fprintf(stderr,"Mr,Me = %g, %g\n",SNII.RemnantMass,SNII.EjectaMass);

    Z = 0.02; Mass = 10;
    SNII = CELibGetSNIIFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Z,
                .MassConversionFactor = MassConversionFactor,
                .Elements = Elements,
                });
    fprintf(stderr,"Energy = %g (%g,%g)\n",SNII.Energy,Mass,Z);
    fprintf(stderr,"Mr,Me = %g, %g\n",SNII.RemnantMass,SNII.EjectaMass);


    //////////////////////////////////////////////////////////////// 

    Mass = 1.0; Z = 0.0;
    SNII = CELibGetFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Z,
                .MassConversionFactor = MassConversionFactor,
                .Elements = Elements,
                },CELibFeedbackType_SNII);
    fprintf(stderr,"UAPIs: Energy = %g (%g,%g)\n",SNII.Energy,Mass,Z);
    fprintf(stderr,"UAPIs: Mr,Me = %g, %g\n",SNII.RemnantMass,SNII.EjectaMass);

    Z = CELibRunParameters.PopIIIMetallicity;
    SNII = CELibGetFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Z,
                .MassConversionFactor = MassConversionFactor,
                .Elements = Elements,
                },CELibFeedbackType_SNII);
    fprintf(stderr,"UAPIs: Energy = %g (%g,%g)\n",SNII.Energy,Mass,Z);
    fprintf(stderr,"UAPIs: Mr,Me = %g, %g\n",SNII.RemnantMass,SNII.EjectaMass);

    Z = 1.2*CELibRunParameters.PopIIIMetallicity;
    SNII = CELibGetFeedback((struct CELibStructFeedbackInput){
                .Mass = Mass,
                .Metallicity = Z,
                .MassConversionFactor = MassConversionFactor,
                .Elements = Elements,
                },CELibFeedbackType_SNII);
    fprintf(stderr,"UAPIs: Energy = %g (%g,%g)\n",SNII.Energy,Mass,Z);
    fprintf(stderr,"UAPIs: Mr,Me = %g, %g\n",SNII.RemnantMass,SNII.EjectaMass);

    return ;
}

static void CheckSNIIFeedbackEnergy(void){

    double Z = 0.0;
    fprintf(stderr,"Integrated energy = %g (%g)\n",CELibGetSNIIIntegratedEnergy(Z),Z);

    Z = CELibRunParameters.PopIIIMetallicity;
    fprintf(stderr,"Integrated energy = %g (%g)\n",CELibGetSNIIIntegratedEnergy(Z),Z);

    Z = 1.05*CELibRunParameters.PopIIIMetallicity;
    fprintf(stderr,"Integrated energy = %g (%g)\n",CELibGetSNIIIntegratedEnergy(Z),Z);

    Z = 1.2*CELibRunParameters.PopIIIMetallicity;
    fprintf(stderr,"Integrated energy = %g (%g)\n",CELibGetSNIIIntegratedEnergy(Z),Z);

    Z = 0.001;
    fprintf(stderr,"Integrated energy = %g (%g)\n",CELibGetSNIIIntegratedEnergy(Z),Z);

    Z = 0.01;
    fprintf(stderr,"Integrated energy = %g (%g)\n",CELibGetSNIIIntegratedEnergy(Z),Z);

    Z = 0.02;
    fprintf(stderr,"Integrated energy = %g (%g)\n",CELibGetSNIIIntegratedEnergy(Z),Z);

    return ;
}

void PlotFB(void){

    InitializeRandomGenerator(1977);

    CheckSNIIFeedback();

    CheckSNIIFeedbackEnergy();

    // RunSimulation();
// 
    // CELibWriteSNIaRate(ThisRun.OutDir);

    // SNIIFeedbackTest();
    // SNIIFeedbackTest2();

    return ;
}


static void PrintSNIIFeedback(const double Mass, struct CELibStructFeedbackOutput SNII){

    fprintf(stderr,"\n");
    fprintf(stderr,"Mass of SSP = %g\n",Mass);
    fprintf(stderr,"Ejecta Mass = %g\n",SNII.EjectaMass);
    fprintf(stderr,"Remnant Mass = %g\n",SNII.RemnantMass);
    fprintf(stderr,"Energy = %g\n",SNII.Energy);


    double ElementsS[CELibYield_Number];
    CELibSetSolarMetallicity(1.0,ElementsS);

    for(int i=0;i<CELibYield_Number;i++){
        fprintf(stderr,"Element[%d] = %g / [x/Fe] = %g\n",
                i,SNII.Elements[i],
                //log10(SNII.Elements[i]/SNII.Elements[CELibYield_Fe]));
                //log10(ElementsS[i]/ElementsS[CELibYield_Fe]));
                log10(SNII.Elements[i]/SNII.Elements[CELibYield_Fe])-
                log10(ElementsS[i]/ElementsS[CELibYield_Fe]));
    }

    return ;
}

static void SNIIFeedbackTest(void){

    //
    double LogMassMin = log10(ThisRun.SSPMassMin);
    double LogMassMax = log10(ThisRun.SSPMassMax);
    double dMass = (LogMassMax-LogMassMin)/ThisRun.SSPNMassBin;


    for(int i=0;i<ThisRun.SSPNMassBin;i++){
        double Mass = pow(10.0,(i+0.5)*dMass+LogMassMin);
        gprint(Mass);

        struct CELibStructFeedbackOutput SNII;
        if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_P98){
            double Elements[CELibYield_Number];
            CELibSetPrimordialMetallicity(Mass,Elements);
            SNII = CELibGetSNIIFeedback((struct CELibStructFeedbackInput){
                    .Mass = Mass,
                    .Metallicity = ThisRun.SSPMetallicity,
                    .MassConversionFactor = 1.0,
                    .Elements = Elements,
                    });
        } else if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_N13){
            double Elements[CELibYield_Number];
            //CELibSetSolarMetallicity(Mass,Elements);
            //CELibSetSolarMetallicity(1.0,Elements);
            CELibSetPrimordialMetallicity(Mass,Elements);

            SNII = CELibGetSNIIFeedback((struct CELibStructFeedbackInput){
                    .Mass = Mass,
                    .Metallicity = ThisRun.SSPMetallicity,
                    .MassConversionFactor = 1.0,
                    .Elements = Elements,
                    });
        }

        PrintSNIIFeedback(Mass,SNII);
    }

    return ;
}

static void PrintSNIIFeedback_xFe(const double Z, struct CELibStructFeedbackOutput SNII, const int mode){

    FILE *fp;
    //char fname[MaxCharactersInLine];
    if(mode == 0){
        FileOpen(fp,"./xFe.dat","w");
    } else {
        FileOpen(fp,"./xFe.dat","a");
    }

    fprintf(fp,"%g ",log10(Z));

    double ElementsS[CELibYield_Number];
    CELibSetSolarMetallicity(1.0,ElementsS);

    for(int i=0;i<CELibYield_Number;i++){
#if 1
        fprintf(fp,"%g ",
                log10(SNII.Elements[i]/SNII.Elements[CELibYield_Fe])-
                log10(ElementsS[i]/ElementsS[CELibYield_Fe]));
#else
        fprintf(fp,"%g %g %g ",
                log10(SNII.Elements[i]/SNII.Elements[CELibYield_Fe])-
                log10(ElementsS[i]/ElementsS[CELibYield_Fe]),
                log10(SNII.Elements[i]/SNII.Elements[CELibYield_Fe]),
                log10(ElementsS[i]/ElementsS[CELibYield_Fe]));
#endif
    }
    fprintf(fp,"\n");

    fclose(fp);

    return ;
}

/*
 * Check [x/Fe] as a function of metallicity.
 */
static void SNIIFeedbackTest2(void){

    double LogZMin = log10(ThisRun.SSPZMin);
    double LogZMax = log10(ThisRun.SSPZMax);
    double dZ = (LogZMax-LogZMin)/ThisRun.SSPNZBin;

    for(int i=0;i<ThisRun.SSPNZBin;i++){
        double Z = pow(10.0,(i+0.5)*dZ+LogZMin);
        gprint(Z);

        struct CELibStructFeedbackOutput SNII;

        if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_P98){
            double Elements[CELibYield_Number];
            CELibSetMetallicityWithSolarAbundancePattern(1.0,Elements,Z);
            SNII = CELibGetSNIIFeedback((struct CELibStructFeedbackInput){
                    .Mass = 1.0,
                    .Metallicity = Z,
                    .MassConversionFactor = 1.0,
                    .Elements = Elements,
                    });
        } else if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_N13){
            double Elements[CELibYield_Number];
            // CELibSetSolarMetallicity(1.0,Elements);
            CELibSetMetallicityWithSolarAbundancePattern(1.0,Elements,Z);

            SNII = CELibGetSNIIFeedback((struct CELibStructFeedbackInput){
                    .Mass = 1.0,
                    .Metallicity = Z,
                    .MassConversionFactor = 1.0,
                    .Elements = Elements,
                    });
        }
        PrintSNIIFeedback_xFe(Z,SNII,i);
    }

    double Elements[CELibYield_Number];
    CELibSetMetallicityWithSolarAbundancePattern(1.0,Elements,0.013);
    //CELibSetSolarMetallicity(1.0,Elements);
    
    struct CELibStructFeedbackOutput SNII2;
    for(int i=0;i<CELibYield_Number;i++){
        //fprintf(stderr,"|| %d %g // %g\n",i,Elements[i],Elements[i]/Elements[10]);
        char name[MaxCharactersInLine];
        CELibGetYieldElementName(i,name);
        fprintf(stderr,"|| %d %g // %g | %s\n",i,Elements[i],
                Elements[i]/Elements[10],name);
        SNII2.Elements[i] = Elements[i];
    }

    //PrintSNIIFeedback_xFe(0.013,SNII2,1);

    return ;
}

void CELibInitSNIaYields(void);
void CELibGetSNIaCurrentYieldModelName(char *ModelName);

static void SNIaYieldTest(void){

    FILE *fp;
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/SNIaYieldTest.dat",ThisRun.OutDir);
    FileOpen(fp,fname,"w");
    for(int i=0;i<CELibSNIaYieldsTableID_Number;i++){
        CELibRunParameters.SNIaYieldsTableID = i;
        CELibInitSNIaYields();

        // 1.0 Msun send particle
        struct CELibStructFeedbackOutput SNIa = CELibGetSNIaFeedback((struct CELibStructFeedbackInput){
                .Mass = 1000,
                .MassConversionFactor = 1,
                });

        // Write Yields
        fprintf(fp,"-----------\n");
        char Name[MaxCharactersInLine];
        CELibGetSNIaCurrentYieldModelName(Name);
        fprintf(fp,"%s\n",Name);
        fprintf(fp,"Energy = %g\n",SNIa.Energy);
        fprintf(fp,"EjectaMass = %g\n",SNIa.EjectaMass);
        fprintf(fp,"RemnantMass = %g\n",SNIa.RemnantMass);
        fprintf(fp,"Elements [C]  = %g\n",SNIa.Elements[CELibYield_C]);
        fprintf(fp,"Elements [N]  = %g\n",SNIa.Elements[CELibYield_N]);
        fprintf(fp,"Elements [O]  = %g\n",SNIa.Elements[CELibYield_O]);
        fprintf(fp,"Elements [Ne] = %g\n",SNIa.Elements[CELibYield_Ne]);
        fprintf(fp,"Elements [Mg] = %g\n",SNIa.Elements[CELibYield_Mg]);
        fprintf(fp,"Elements [Si] = %g\n",SNIa.Elements[CELibYield_Si]);
        fprintf(fp,"Elements [S]  = %g\n",SNIa.Elements[CELibYield_S]);
        fprintf(fp,"Elements [Ca] = %g\n",SNIa.Elements[CELibYield_Ca]);
        fprintf(fp,"Elements [Fe] = %g\n",SNIa.Elements[CELibYield_Fe]);
        fprintf(fp,"Elements [Ni] = %g\n",SNIa.Elements[CELibYield_Ni]);
        fprintf(fp,"Elements [Eu] = %g\n",SNIa.Elements[CELibYield_Eu]);
            

        fprintf(fp,"\n");
    }
    fclose(fp);

    return ;
}

#define MaxSNIaEvent    500

struct StructSamples{
    double InitialMass;
    double Mass;
    double MassLossSNII;
    double MassLossSNIa;
    double MassLossAGB;
    double Metallicity;
    double ExplosionTimeSNII;
    double ExplosionTimeSNIa[MaxSNIaEvent];
    double ExplosionTimeAGB[MaxSNIaEvent];
    int CountSNII;
    int CountSNIa;
    int CountAGB;
    int IMFType;
    int MyID;
} *Samples;


/*!
 * Initialize array of structure for Samples.
 */
static void InitSamples(void){

    Samples = malloc(sizeof(struct StructSamples)*ThisRun.NSample);

    for(int i=0;i<ThisRun.NSample;i++){
        Samples[i].InitialMass = Samples[i].Mass = ThisRun.InitialMass;
        Samples[i].Metallicity = ThisRun.Metallicity;
        Samples[i].MassLossSNII = Samples[i].MassLossSNIa = Samples[i].MassLossAGB = 0.e0;
        Samples[i].CountSNII = Samples[i].CountSNIa = Samples[i].CountAGB = 0;
        Samples[i].IMFType = CELibIMF_Salpeter;
        Samples[i].MyID = i;
        for(int k=0;k<MaxSNIaEvent;k++){
            Samples[i].ExplosionTimeSNIa[k] = Samples[i].ExplosionTimeAGB[k] = 0.e0;
        }
    }

    return ;
}

/*!
 * This function calls just after a star particle forms and a Type II or Ia
 * SN explosion takes place in order to compute the next explosion time.
 */
static double StellarFeedbackGetNextExplosionTime(const int mode, const double Metallicity, const double InitialMass_in_Msun, const int Count){

    double R = gsl_rng_uniform(RandomGenerator);
    if(mode == CELibFeedbackType_SNII){
        double Time = CELibGetSNIIExplosionTime(R,Metallicity);
        if(Time < 0.0){
            return 0.e0;
        } else { 
            return Time;
        }
    } else if(mode == CELibFeedbackType_SNIa){
        double Time = CELibGetSNIaExplosionTime(R,Metallicity,InitialMass_in_Msun,Count);   
        return Time;
    } else if(mode == CELibFeedbackType_AGB){
        double Time = CELibGetAGBFeedbackTime(R,Metallicity,Count);   
        return Time;
    } else if(mode == CELibFeedbackType_NSM){
        double Time = CELibGetNSMFeedbackTime(R,Metallicity,InitialMass_in_Msun,Count);   
        return Time;
    }
    return ;
}


static void UpdateSSP(const double Tnow){

    for(int i=0;i<ThisRun.NSample;i++){
        if(Samples[i].ExplosionTimeSNII == 0.0){
            // Call SNII time.
            Samples[i].ExplosionTimeSNII = 
                CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                        .R = gsl_rng_uniform(RandomGenerator),
                        .InitialMass_in_Msun = Samples[i].InitialMass,
                        .Metallicity = Samples[i].Metallicity,
                        .Count = 0,
                        },CELibFeedbackType_SNII);
                /*
                StellarFeedbackGetNextExplosionTime(CELibFeedbackType_SNII,
                        Samples[i].Metallicity,Samples[i].InitialMass,0);
                        */
        } else {
            if((Samples[i].CountSNII == 0)&&(Samples[i].ExplosionTimeSNII<Tnow)){
                double tmpElements[CELibYield_Number];
                for(int l=0;l<CELibYield_Number;l++) tmpElements[l] = 0.e0;
                struct CELibStructFeedbackOutput SNII = CELibGetSNIIFeedback((struct CELibStructFeedbackInput){
                        .Mass = Samples[i].InitialMass,
                        .Metallicity = Samples[i].Metallicity,
                        .MassConversionFactor = 1.0,
                        .Elements = tmpElements,
                        });

                Samples[i].Mass -= SNII.EjectaMass;
                Samples[i].MassLossSNII += SNII.EjectaMass;

                Samples[i].CountSNII ++; 

                Samples[i].ExplosionTimeSNIa[0] = 
                    CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                            .R = gsl_rng_uniform(RandomGenerator),
                            .InitialMass_in_Msun = Samples[i].InitialMass,
                            .Metallicity = Samples[i].Metallicity,
                            .Count = Samples[i].CountSNIa,
                            },CELibFeedbackType_SNIa);
                    /*
                    StellarFeedbackGetNextExplosionTime(CELibFeedbackType_SNIa,
                            Samples[i].Metallicity,Samples[i].InitialMass,Samples[i].CountSNIa);
                            */

                Samples[i].ExplosionTimeAGB[0] = 
                    CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                            .R = gsl_rng_uniform(RandomGenerator),
                            .InitialMass_in_Msun = Samples[i].InitialMass,
                            .Metallicity = Samples[i].Metallicity,
                            .Count = Samples[i].CountAGB,
                            },CELibFeedbackType_AGB);
                    /*
                    StellarFeedbackGetNextExplosionTime(CELibFeedbackType_AGB,
                            Samples[i].Metallicity,Samples[i].InitialMass,Samples[i].CountAGB);
                            */
            }
        }

        if((Samples[i].CountSNII > 0)&&(Samples[i].CountSNIa < MaxSNIaEvent-1)){
            if(Samples[i].ExplosionTimeSNIa[Samples[i].CountSNIa]<Tnow){
                struct CELibStructFeedbackOutput SNIa = CELibGetSNIaFeedback((struct CELibStructFeedbackInput){
                        .Mass = Samples[i].InitialMass,
                        .MassConversionFactor = 1.0,
                        });

                Samples[i].Mass -= SNIa.EjectaMass;
                Samples[i].MassLossSNIa += SNIa.EjectaMass;

                Samples[i].CountSNIa ++;
                Samples[i].ExplosionTimeSNIa[Samples[i].CountSNIa] = 
                    CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                            .R = gsl_rng_uniform(RandomGenerator),
                            .InitialMass_in_Msun = Samples[i].InitialMass,
                            .Metallicity = Samples[i].Metallicity,
                            .Count = Samples[i].CountSNIa,
                            },CELibFeedbackType_SNIa);
                    /*
                    StellarFeedbackGetNextExplosionTime(CELibFeedbackType_SNIa,
                            Samples[i].Metallicity,Samples[i].InitialMass,Samples[i].CountSNIa);
                            */
            }
        }

        if((Samples[i].CountSNII > 0)&&(Samples[i].CountAGB < MaxSNIaEvent-1)){
            if(Samples[i].ExplosionTimeAGB[Samples[i].CountAGB]<Tnow){
                double tmpElements[CELibYield_Number];
                for(int l=0;l<CELibYield_Number;l++) tmpElements[l] = 0.e0;
                struct CELibStructFeedbackOutput AGB 
                    = CELibGetAGBFeedback((struct CELibStructFeedbackInput){
                            .Mass = Samples[i].InitialMass,
                            .MassConversionFactor = 1.0,
                            .Metallicity = Samples[i].Metallicity,
                            .Count = Samples[i].CountAGB,
                            .Elements = tmpElements,
                            });

                Samples[i].Mass -= AGB.EjectaMass;
                Samples[i].MassLossAGB += AGB.EjectaMass;

                Samples[i].CountAGB ++;
                Samples[i].ExplosionTimeAGB[Samples[i].CountAGB] = 
                    CELibGetNextEventTime((struct CELibStructNextEventTimeInput){
                            .R = gsl_rng_uniform(RandomGenerator),
                            .InitialMass_in_Msun = Samples[i].InitialMass,
                            .Metallicity = Samples[i].Metallicity,
                            .Count = Samples[i].CountAGB,
                            },CELibFeedbackType_AGB);
                    /*
                    StellarFeedbackGetNextExplosionTime(CELibFeedbackType_AGB,
                            Samples[i].Metallicity,Samples[i].InitialMass,Samples[i].CountAGB);
                    */
            }
        }
    }

    return ;
}

static void Integrate(void){

    int counter = 0;
    double Tnow = ThisRun.TStart;
    int iParcent = (ThisRun.TEnd-ThisRun.TStart)/ThisRun.dt/100;
    int counter_log = 0;
    do{

        UpdateSSP(Tnow);

        Tnow += ThisRun.dt;
        counter ++;

#if 0
        if(counter%10000==0){
            // fprintf(stderr,"Now %g [year]\n",Tnow);
        }
#endif
        if(counter%(2*iParcent) == 0){
            if(counter_log%5 == 0){
                fprintf(stderr,"|");
            } else {
                fprintf(stderr,"*");
            }
            counter_log ++;
        }
    }while(Tnow < ThisRun.TEnd);
    fprintf(stderr,"|\n");

    return ;
}

static void WriteSNIIBin(void){

    FILE *fp;
    char fname[MaxCharactersInLine];

    // Show SNII log.
    Snprintf(fname,"%s/SNII.dat",ThisRun.OutDir);
    FileOpen(fp,fname,"w");
    for(int i=0;i<ThisRun.NSample;i++){
        fprintf(fp,"%d %g\n",i,Samples[i].ExplosionTimeSNII);
    }
    fclose(fp);

    const int Nbin = 100;
    const double BinMin = 1e6;
    const double BinMax = 1e8;

    double Bin[Nbin];
    for(int i=0;i<Nbin;i++){
        Bin[i] = 0.e0;
    }

    double BinMin_log = log10(BinMin);
    double BinMax_log = log10(BinMax);
    double dBin = (BinMax_log-BinMin_log)/Nbin; 

    for(int i=0;i<ThisRun.NSample;i++){
        double T_log = log10(Samples[i].ExplosionTimeSNII);
        int Index = (T_log-BinMin_log)/dBin;
        if(Index >= Nbin) continue;
        if(Index < 0) continue;
        Bin[Index] ++;
    }

    Snprintf(fname,"%s/SNIIBin.dat",ThisRun.OutDir);
    FileOpen(fp,fname,"w");
    for(int i=0;i<Nbin;i++){
        double T_log = (i+0.5)*dBin + BinMin_log;
        double T = pow(10.e0,T_log);
        fprintf(fp,"%g %g\n",T,Bin[i]);
    }
    fclose(fp);

    for(int i=1;i<Nbin;i++){
        Bin[i] += Bin[i-1];
    }

    Snprintf(fname,"%s/SNIIBinC.dat",ThisRun.OutDir);
    FileOpen(fp,fname,"w");
    for(int i=0;i<Nbin;i++){
        double T_log = (i+0.5)*dBin + BinMin_log;
        double T = pow(10.e0,T_log);
        fprintf(fp,"%g %g\n",T,Bin[i]);
    }
    fclose(fp);

    return ;
}

static void WriteSNIaBin(void){

    FILE *fp;
    char fname[MaxCharactersInLine];

    double Mtotal = 0.e0;
    for(int i=0;i<ThisRun.NSample;i++){
        Mtotal += Samples[i].InitialMass;
    }

    const int Nbin = 100;
    const double BinMin = 1.e7;
    const double BinMax = 1.4e10;

    double Bin[Nbin];
    for(int i=0;i<Nbin;i++){
        Bin[i] = 0.e0;
    }

    double BinMin_log = log10(BinMin);
    double BinMax_log = log10(BinMax);
    double dBin = (BinMax_log-BinMin_log)/Nbin; 

    for(int i=0;i<ThisRun.NSample;i++){
        for(int k=0;k<Samples[i].CountSNIa;k++){
            double T_log = log10(Samples[i].ExplosionTimeSNIa[k]);
            int Index = (T_log-BinMin_log)/dBin;
            if(Index >= Nbin) continue;
            if(Index < 0) continue;
            Bin[Index] += CELibRunParameters.SNIaNassociation;
        }
    }

    for(int i=0;i<Nbin;i++){
        Bin[i] /= Mtotal;
    }

    Snprintf(fname,"%s/SNIaBin_%03d.dat",ThisRun.OutDir,CELibRunParameters.SNIaNassociation);
    FileOpen(fp,fname,"w");
    for(int i=0;i<Nbin;i++){
        double T_log = (i+0.5)*dBin + BinMin_log;
        double T = pow(10.e0,T_log);
        fprintf(fp,"%g %g\n",T,Bin[i]);
    }
    fclose(fp);

    for(int i=1;i<Nbin;i++){
        Bin[i] += Bin[i-1];
    }


    Snprintf(fname,"%s/SNIaBinC_%03d.dat",ThisRun.OutDir,CELibRunParameters.SNIaNassociation);
    FileOpen(fp,fname,"w");
    for(int i=0;i<Nbin;i++){
        double T_log = (i+0.5)*dBin + BinMin_log;
        double T = pow(10.e0,T_log);
        fprintf(fp,"%g %g\n",T,Bin[i]);
    }
    fclose(fp);

    return ;

}

static void WriteAGBBin(void){

    FILE *fp;
    char fname[MaxCharactersInLine];

    double Mtotal = 0.e0;
    for(int i=0;i<ThisRun.NSample;i++){
        Mtotal += Samples[i].InitialMass;
    }

    const int Nbin = 100;
    const double BinMin = 1.e7;
    const double BinMax = 1.4e10;

    double Bin[Nbin];
    for(int i=0;i<Nbin;i++){
        Bin[i] = 0.e0;
    }

    double BinMin_log = log10(BinMin);
    double BinMax_log = log10(BinMax);
    double dBin = (BinMax_log-BinMin_log)/Nbin; 

    for(int i=0;i<ThisRun.NSample;i++){
        for(int k=0;k<Samples[i].CountAGB;k++){
            double T_log = log10(Samples[i].ExplosionTimeAGB[k]);
            int Index = (T_log-BinMin_log)/dBin;
            if(Index >= Nbin) continue;
            if(Index < 0) continue;
            //Bin[Index] += CELibRunParameters.SNIaNassociation;
            Bin[Index] ++;
        }
    }

    Snprintf(fname,"%s/AGBBin_%03d.dat",ThisRun.OutDir,CELibRunParameters.AGBBinNumber);
    FileOpen(fp,fname,"w");
    for(int i=0;i<Nbin;i++){
        double T_log = (i+0.5)*dBin + BinMin_log;
        double T = pow(10.e0,T_log);
        fprintf(fp,"%g %g\n",T,Bin[i]);
    }
    fclose(fp);

    for(int i=1;i<Nbin;i++){
        Bin[i] += Bin[i-1];
    }


    Snprintf(fname,"%s/AGBBinC_%03d.dat",ThisRun.OutDir,CELibRunParameters.AGBBinNumber);
    FileOpen(fp,fname,"w");
    for(int i=0;i<Nbin;i++){
        double T_log = (i+0.5)*dBin + BinMin_log;
        double T = pow(10.e0,T_log);
        fprintf(fp,"%g %g\n",T,Bin[i]);
    }
    fclose(fp);

    return ;

}

static void ShowLog(void){

    FILE *fp;
    char fname[MaxCharactersInLine];

    // Show Massloss log.
    Snprintf(fname,"%s/MassLoss.dat",ThisRun.OutDir);
    FileOpen(fp,fname,"w");
    for(int i=0;i<ThisRun.NSample;i++){
        fprintf(fp,"%d %g %g %g %g %g %d %d %d\n",i,
                Samples[i].InitialMass,
                Samples[i].Mass,Samples[i].MassLossSNII,Samples[i].MassLossSNIa,Samples[i].MassLossAGB,
                Samples[i].CountSNII,Samples[i].CountSNIa,Samples[i].CountAGB);
    }
    fclose(fp);

    // Show SNII log.
    Snprintf(fname,"%s/SNII.dat",ThisRun.OutDir);
    FileOpen(fp,fname,"w");
    for(int i=0;i<ThisRun.NSample;i++){
        fprintf(fp,"%d %g\n",i,Samples[i].ExplosionTimeSNII);
    }
    fclose(fp);


    // Show SNIa log.
    Snprintf(fname,"%s/SNIa_%03d.dat",ThisRun.OutDir,CELibRunParameters.SNIaNassociation);
    FileOpen(fp,fname,"w");
    for(int i=0;i<ThisRun.NSample;i++){
        fprintf(fp,"%d %d ",i,Samples[i].CountSNIa);
        for(int k=0;k<Samples[i].CountSNIa+1;k++){
            fprintf(fp,"%g ",Samples[i].ExplosionTimeSNIa[k]);
        }
        fprintf(fp,"\n");
    }
    fclose(fp);

    //
    double Mtotal = 0.e0;
    for(int i=0;i<ThisRun.NSample;i++){
        Mtotal += Samples[i].InitialMass;
    }
    gprint(Mtotal);

    WriteSNIIBin();
    WriteSNIaBin();
    WriteAGBBin();

#if 0
#define Nbin (100)
#define BinMin  (1.e7)
#define BinMax  (1.4e10)

    double Bin[Nbin];
    for(int i=0;i<Nbin;i++){
        Bin[i] = 0.e0;
    }

    double BinMin_log = log10(BinMin);
    double BinMax_log = log10(BinMax);
    double dBin = (BinMax_log-BinMin_log)/Nbin; 

    gprintl(BinMin);
    gprintl(BinMax);
    gprintl(dBin);

    for(int i=0;i<ThisRun.NSample;i++){
        for(int k=0;k<Samples[i].CountSNIa;k++){
            double T_log = log10(Samples[i].ExplosionTimeSNIa[k]);
            int Index = (T_log-BinMin_log)/dBin;
            // gprintl(T_log);
            // dprintl(Index);
            if(Index >= Nbin) continue;
            if(Index < 0) continue;
            // Bin[Index] ++;
            Bin[Index] += CELibRunParameters.SNIaNassociation;
        }
    }

    for(int i=1;i<Nbin;i++){
        Bin[i] += Bin[i-1];
    }

    for(int i=0;i<Nbin;i++){
        Bin[i] /= Mtotal;
    }

    Snprintf(fname,"%s/SNIaBin_%03d.dat",ThisRun.OutDir,CELibRunParameters.SNIaNassociation);
    FileOpen(fp,fname,"w");
    for(int i=0;i<Nbin;i++){
        double T_log = (i+0.5)*dBin + BinMin_log;
        double T = pow(10.e0,T_log);
        fprintf(fp,"%g %g\n",T,Bin[i]);
    }
    fclose(fp);
#endif

    return ;
}

/*!
 *
 */
static void RunSimulation(void){

    InitSamples();

    Integrate();

    ShowLog();

    return ;
}

