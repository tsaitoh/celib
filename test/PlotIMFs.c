#include "config.h"
#include "PGPlotOperations.h"
#include "../src/IMFFunctions.h"
#include "../src/IMFTypes.h"
#include <cpgplot.h>


static void WriteIMFNormalizations(void){

    FILE *fp;
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/IMFNorm.dat","w");

    FileOpen(fp,fname,"w");
    for(int i=0;i<CELibIMF_NTypes;i++){
        CELibRunParameters.IMFType = i;
        char IMFName[MaxCharactersInLine];
        CELibGetIMFName(IMFName);
        fprintf(fp,"%s\n",IMFName);
        //fprintf(fp,"Normalization = %g\n",IMF[i].Normalization[0]);
    }

    fclose(fp);

    return ;
}

static double _IntegralSimpson(const double x_0, const double x_1, const int Steps, double (*func)(const double)){

    double h = (x_1-x_0)/(2.0*Steps);
    double Sum_odd = 0.0;
    double Sum_even = 0.0;
    for(int i=1;i<2*Steps;i+=2){
        Sum_odd += func(x_0+h*i);
    }
    for(int i=2;i<2*Steps;i+=2){
        Sum_even += func(x_0+h*i);
    }
    double Sum = (func(x_0)+func(x_1)+4*Sum_odd+2*Sum_even)*h/3.0;

    return Sum;
}

static void GetIMFName(const int ID, char Name[]){

    if(ID == CELibIMF_Salpeter){
        sprintf(Name,"Salpeter");
    } else if(ID == CELibIMF_DietSalpeter){
        sprintf(Name,"Diet Salpeter");
    } else if(ID == CELibIMF_MillerScalo){
        sprintf(Name,"Miller-Scalo");
    } else if(ID == CELibIMF_Kroupa){
        sprintf(Name,"Kroupa");
    } else if(ID == CELibIMF_Kroupa1993){
        sprintf(Name,"Kroupa1993");
    } else if(ID == CELibIMF_Kennicutt){
        sprintf(Name,"Kennicutt");
    } else if(ID == CELibIMF_Chabrier){
        sprintf(Name,"Chabrier");
    } else if(ID == CELibIMF_Susa){
        sprintf(Name,"Susa");
    } else {
        sprintf(Name,"None");
    }

    return ;
}

void PlotIMFsMassFraction(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/IMFs",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(8.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgswin(-1.6,2.7,-4,-1.5);
    cpgslw(4);
    cpgbox("BCTSNL",0,0.,"BCTSNL",0.0,0.0);

    int Nbin = 100;
    for(int i=0;i<CELibIMF_NTypes;i++){

        double dmass = (log10(CELibIMF[i].MassMax)-log10(CELibIMF[i].MassMin))/Nbin;
        float x[Nbin];
        float y[Nbin];

        int counter = 0; 
        for(int k=0;k<Nbin;k++){
            double Mu = pow(10.0,log10(CELibIMF[i].MassMax)-dmass*k);
            double Ml = pow(10.0,log10(CELibIMF[i].MassMax)-dmass*(k+1));

            if(Mu > CELibIMF[i].MassMax) Mu = CELibIMF[i].MassMax;
            if(Ml > CELibIMF[i].MassMax) Ml = CELibIMF[i].MassMax;
            if(Mu < CELibIMF[i].MassMin) Mu = CELibIMF[i].MassMin;
            if(Ml < CELibIMF[i].MassMin) Ml = CELibIMF[i].MassMin;

            x[k] = 0.5*(Mu+Ml);
            y[k] = _IntegralSimpson(Ml,Mu,1000,CELibIMF[i].IMFFunction);

            if(y[k] == 0.e0){
                y[k] = 1.e-10;
            }

            if(x[k] > 0.e0) x[k] = log10(x[k]);
            if(y[k] > 0.e0) y[k] = log10(y[k]);

        }

        cpgsci(i+1);
        cpgsls(i+1);
        if(i==6) cpgsci(10);
        if(i==6) cpgsls(9);
        cpgline(Nbin-counter,x+counter,y+counter);
            // exit(1);


        char Name[MaxCharactersInLine];
        GetIMFName(i,Name);
        cpgsch(1.3);
        float y_ = -2.8-0.12*i;
        cpgptxt(-0.95,y_,0.0,0.0,Name);
        float xl[2] = {-1.5,-1.05};
        float yl[2] = {y_+0.04,y_+0.04};
        cpgline(2,xl,yl);
        cpgsch(2);
    }

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    //cpglab("Mass [M\\d\\(2281)\\u]","\\gc(log M)","");
    cpglab("Mass [M\\d\\(2281)\\u]","Mass fraction","");

    ClosePGPlot();

    return ;
}

void PlotIMFsXi(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/IMFsXi",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(8.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgswin(-1.6,2.7,-4.5,0.6);
    cpgslw(4);
    cpgbox("BCTSNL",0,0.,"BCTSNL",0.0,0.0);

    int Nbin = 100;
    for(int i=0;i<CELibIMF_NTypes;i++){

        double dmass = (log10(CELibIMF[i].MassMax)-log10(CELibIMF[i].MassMin))/Nbin;
        float x[Nbin];
        float y[Nbin];

        int counter = 0; 
        for(int k=0;k<Nbin;k++){
            double Mu = pow(10.0,log10(CELibIMF[i].MassMax)-dmass*k);
            double Ml = pow(10.0,log10(CELibIMF[i].MassMax)-dmass*(k+1));

            if(Mu > CELibIMF[i].MassMax) Mu = CELibIMF[i].MassMax;
            if(Ml > CELibIMF[i].MassMax) Ml = CELibIMF[i].MassMax;
            if(Mu < CELibIMF[i].MassMin) Mu = CELibIMF[i].MassMin;
            if(Ml < CELibIMF[i].MassMin) Ml = CELibIMF[i].MassMin;

            x[k] = 0.5*(Mu+Ml);
            y[k] = CELibIMF[i].IMFFunction(x[k]);

            if(y[k] == 0.e0){
                y[k] = 1.e-10;
            }

            if(x[k] > 0.e0) x[k] = log10(x[k]);
            if(y[k] > 0.e0) y[k] = log10(y[k]);
        }
        cpgsci(i+1);
        cpgsls(i+1);
        if(i==6) cpgsci(10);
        if(i==6) cpgsls(9);
        cpgline(Nbin-counter,x+counter,y+counter);


        char Name[MaxCharactersInLine];
        GetIMFName(i,Name);
        cpgsch(1.3);
        //float y_ = -2.8-0.14*i;
        float y_ = 0.2-0.24*i;
        float x_ = 1.3;
        cpgptxt(x_,y_,0.0,0.0,Name);
        float xl[2] = {0.8,1.2};
        float yl[2] = {y_+0.06,y_+0.06};
        cpgline(2,xl,yl);
        cpgsch(2);
    }

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Mass [M\\d\\(2281)\\u]","\\gc(log M)","");

    ClosePGPlot();

    return ;
}

void PlotIMFs(void){
    PlotIMFsMassFraction();
    PlotIMFsXi();
    return ;
}
