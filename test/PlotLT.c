#include "config.h"
#include "PGPlotOperations.h"
#include "../src/IMFFunctions.h"
#include "../src/IMFTypes.h"
#include "../src/LifeTime.h"
#include <cpgplot.h>

void PlotLT1(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/LT%d",ThisRun.OutDir,CELibRunParameters.LifeTimeType);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(8.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgswin(log10(0.3),log10(350),6,11);
    cpgslw(4);
    cpgbox("BCTSNL",0,0.,"BCTSNL",0.0,0.0);


    int Nbin = 300;
    double dmass;

    double Z[] = {0.0,0.0004,0.004,0.02,0.05};
    int NZ = sizeof(Z)/sizeof(double);

    int CI[] = {1,2,3,4,8};

    char Label[NZ][MaxCharactersInLine];
    sprintf(Label[0],"Z=0");
    sprintf(Label[1],"Z=0.0004");
    sprintf(Label[2],"Z=0.004");
    sprintf(Label[3],"Z=0.02");
    sprintf(Label[4],"Z=0.05");

    cpgsch(1.6);
    for(int i=0;i<NZ;i++){
        double Mmax,Mmin;
        if((CELibRunParameters.LifeTimeType == CELibLifeTime_LSF)&&(CELibRunParameters.PopIIILifeTime == 1)&&(i==0)){
            Mmax = CELibIMF[CELibIMF_Susa].MassMax;
        } else {
            Mmax = CELibIMF[CELibRunParameters.IMFType].MassMax;
        }
        Mmin = 0.6;
        dmass = (log10(Mmax)-log10(Mmin))/Nbin;


        float x[Nbin];
        float y[Nbin];

        int counter = 0; 
        for(int k=0;k<Nbin;k++){
            x[k] = pow(10.0,log10(Mmax)-dmass*(k+0.5));
            y[k] = CELibGetLifeTimeofStar(x[k],Z[i]);

            if(x[k] > 0.e0) x[k] = log10(x[k]);
            if(y[k] > 0.e0) y[k] = log10(y[k]);
        }
        //cpgsci(i+1);
        cpgsci(CI[i]);
        cpgsls(i+1);
        cpgline(Nbin-counter,x+counter,y+counter);

        float y_ = 10.5-0.35*i;
        cpgptxt(1.3,y_,0.0,0.0,Label[i]);
        float xl[2] = {0.9,1.2};
        float yl[2] = {y_+0.1,y_+0.1};
        cpgline(2,xl,yl);
    }
    cpgsch(2);

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Mass [M\\d\\(2281)\\u]","Lifetime [year]","");

    ClosePGPlot();

    return ;
}

void PlotLT0(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/LT%d",ThisRun.OutDir,CELibRunParameters.LifeTimeType);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(8.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgswin(log10(0.3),log10(350),6,11);
    cpgslw(4);
    cpgbox("BCTSNL",0,0.,"BCTSNL",0.0,0.0);


    int Nbin = 300;

    double Z[] = {0.0004,0.004,0.02,0.05};
    int NZ = sizeof(Z)/sizeof(double);

    char Label[NZ][MaxCharactersInLine];
    sprintf(Label[0],"Z=0.0004");
    sprintf(Label[1],"Z=0.004");
    sprintf(Label[2],"Z=0.02");
    sprintf(Label[3],"Z=0.05");

    int CI[] = {1,2,3,4,8};

    cpgsch(1.6);
    for(int i=0;i<NZ;i++){
        double Mmax = CELibIMF[CELibRunParameters.IMFType].MassMax;
        double Mmin = 0.6;
        double dmass = (log10(Mmax)-log10(Mmin))/Nbin;

        float x[Nbin];
        float y[Nbin];

        int counter = 0; 
        for(int k=0;k<Nbin;k++){
            x[k] = pow(10.0,log10(Mmax)-dmass*(k+0.5));
            y[k] = CELibGetLifeTimeofStar(x[k],Z[i]);

            if(x[k] > 0.e0) x[k] = log10(x[k]);
            if(y[k] > 0.e0) y[k] = log10(y[k]);
        }
        //cpgsci(i+1);
        cpgsci(CI[i]);
        cpgsls(i+1);
        cpgline(Nbin-counter,x+counter,y+counter);

        float y_ = 10.5-0.35*i;
        cpgptxt(1.3,y_,0.0,0.0,Label[i]);
        float xl[2] = {0.9,1.2};
        float yl[2] = {y_+0.1,y_+0.1};
        cpgline(2,xl,yl);
    }
    cpgsch(2);

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Mass [M\\d\\(2281)\\u]","Lifetime [year]","");

    ClosePGPlot();

    return ;
}

void PlotLT(void){

    if(CELibRunParameters.LifeTimeType == CELibLifeTime_Original){
        PlotLT0();
    } else if(CELibRunParameters.LifeTimeType == CELibLifeTime_LSF){
        PlotLT1();
    }
    return ;
}

void PlotMDying(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/MDying%d",ThisRun.OutDir,CELibRunParameters.LifeTimeType);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgswin(6,11,log10(0.3),log10(350));
    cpgslw(4);
    cpgbox("BCTSNL",0,0.,"BCTSNL",0.0,0.0);


    int Nbin = 300;

    double Z[] = {0.0,0.0004,0.004,0.02,0.05};
    int NZ = sizeof(Z)/sizeof(double);

    char Label[NZ][MaxCharactersInLine];
    sprintf(Label[0],"Z=0");
    sprintf(Label[1],"Z=0.0004");
    sprintf(Label[2],"Z=0.004");
    sprintf(Label[3],"Z=0.02");
    sprintf(Label[4],"Z=0.05");
    int CI[] = {1,2,3,4,8};

    cpgsch(1.6);
    for(int i=0;i<NZ;i++){
        double AgeMax,AgeMin;
        if((CELibRunParameters.LifeTimeType == CELibLifeTime_LSF)&&(CELibRunParameters.PopIIILifeTime == 1)&&(i==0)){
            AgeMin = CELibGetLifeTimeofStar(CELibIMF[CELibIMF_Susa].MassMax,Z[i]);
            AgeMax = CELibGetLifeTimeofStar(CELibIMF[CELibIMF_Susa].MassMin,Z[i]);
        } else {
            AgeMin = CELibGetLifeTimeofStar(CELibIMF[CELibRunParameters.IMFType].MassMax,Z[i]);
            AgeMax = CELibGetLifeTimeofStar(CELibIMF[CELibRunParameters.IMFType].MassMin,Z[i]);
        }

        double dage =  (log10(AgeMax)-log10(AgeMin))/Nbin;

        float x[Nbin];
        float y[Nbin];

        int counter = 0; 
        for(int k=0;k<Nbin;k++){
            x[k] = pow(10.0,dage*(k+0.5)+log10(AgeMin));
            y[k] = CELibGetDyingStellarMass(x[k],Z[i]);

            if(y[k] < 0.e0) continue;

            if(x[k] > 0.e0) x[counter] = log10(x[k]);
            if(y[k] > 0.e0) y[counter] = log10(y[k]);
            counter ++;
        }
        //cpgsci(i+1);
        cpgsci(CI[i]);
        cpgsls(i+1);
        cpgline(counter,x,y);

        float y_ = 2.2-0.20*i;
        cpgptxt(9,y_,0.0,0.0,Label[i]);
        float xl[2] = {8.4,8.9};
        float yl[2] = {y_+0.05,y_+0.05};
        cpgline(2,xl,yl);
    }

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.2,0.9);
    cpglab("Lifetime [year]","Mass [M\\d\\(2281)\\u]","");
    cpgsch(2);

    ClosePGPlot();

    return ;
}
