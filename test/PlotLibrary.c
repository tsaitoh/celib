#include "config.h"
#include "PGPlotOperations.h"
#include <cpgplot.h>

static double pCELibGetSNIaIntegrateRate(const double Age_in_Year, const double Metallicity){

    if(CELibRunParameters.SNIaType == CELibSNIaRateModelID_GR83){
        return CELibGetSNIaIntegratedRateGreggioRenzini(Age_in_Year,Metallicity);
    } else if(CELibRunParameters.SNIaType == CELibSNIaRateModelID_PowerLaw){
        return CELibGetSNIaIntegratedRatePowerLaw(Age_in_Year);
    } else if(CELibRunParameters.SNIaType == CELibSNIaRateModelID_PowerLawV13){
        return CELibGetSNIaIntegratedRateVogelsberger(Age_in_Year);
    } else {
        return 0.e0;
    }
}

void PlotLibrary(void){

    CELibSaveRunParameter();
    CELibRunParameters.SNIaNassociation = 1.0;

    double Logdt = (log10(ThisRun.LibTimeMax)-log10(ThisRun.LibTimeMin))/ThisRun.LibNBin;
    double LogTimeMin = log10(ThisRun.LibTimeMin);
    
    //int CI[] = {1,2,3,4,8,10,11,12,5,13,6,14};
    int CI[] = {1,2,3,4,8,10,11,6,5,12,13,14};

    for(int l=0;l<2;l++){
    //for(int l=0;l<1;l++){

        char fname[MaxCharactersInLine];
        Snprintf(fname,"%s/Library.%02d",ThisRun.OutDir,l);

        OpenPGPlot(fname,ThisRun.PlotMode);
        cpgpap(0.0,1.0);

        cpgsch(2.0);
        cpgsvp(0.2,0.9,0.2,0.9);

        cpgsls(1);
        cpgslw(4);
        cpgswin(log10(ThisRun.LibTimeMin),log10(ThisRun.LibTimeMax),-7,0);
        cpgbox("BCTSNL2",0,0.,"BCTSNL2",0.0,0.0);

        double Metallicity = ThisRun.LibMetallicity[l];
        //Metallicity = 0.01;

        double Age[ThisRun.LibNBin];
        double Energy[ThisRun.LibNBin];
        double EjectaMass[ThisRun.LibNBin];
        double Elements[CELibYield_Number][ThisRun.LibNBin];

        for(int i=0;i<ThisRun.LibNBin;i++){

            // fprintf(stderr,"// data generating %d \n",i);
            double time_min = pow(10.0,LogTimeMin+Logdt*i);
            double time_max = pow(10.0,LogTimeMin+Logdt*(i+1));

            Age[i] = time_max; 

            double mass_min = CELibGetDyingStellarMass(time_max,Metallicity);
            double mass_max = CELibGetDyingStellarMass(time_min,Metallicity);

            struct CELibStructFeedbackOutput SNII = CELibGetSNIIYieldsIntegratedInGivenMassRangeZ(Metallicity,mass_min,1000);
            struct CELibStructFeedbackOutput AGB = CELibGetAGBYieldsIntegratedInGivenMassRangeZ(Metallicity,fmin(mass_min,8),8);

            struct CELibStructFeedbackOutput SNIa = CELibGetSNIaFeedback((struct CELibStructFeedbackInput){
                    .Mass = 1.0,
                    .MassConversionFactor = 1.0,
                    .Metallicity = Metallicity,
                    });

            double SNIaRate = pCELibGetSNIaIntegrateRate(Age[i],Metallicity);
            SNIa.Energy *= SNIaRate;
            SNIa.EjectaMass *= SNIaRate;
            for(int k=0;k<CELibYield_Number;k++){
                SNIa.Elements[k] *= SNIaRate;
            }

            if(ThisRun.LibType == 0){
                Energy[i] = SNII.Energy+SNIa.Energy;
                EjectaMass[i] = SNII.EjectaMass+AGB.EjectaMass+SNIa.EjectaMass;
                for(int k=0;k<CELibYield_Number;k++){
                    Elements[k][i] = SNII.Elements[k]+AGB.Elements[k]+SNIa.Elements[k];
                }
            } else if(ThisRun.LibType == 1){
                Energy[i] = SNII.Energy;
                EjectaMass[i] = SNII.EjectaMass;
                for(int k=0;k<CELibYield_Number;k++){
                    Elements[k][i] = SNII.Elements[k];
                }
            } else if(ThisRun.LibType == 2){
                Energy[i] = SNIa.Energy;
                EjectaMass[i] = SNIa.EjectaMass;
                for(int k=0;k<CELibYield_Number;k++){
                    Elements[k][i] = SNIa.Elements[k];
                }
            } else if(ThisRun.LibType == 3){
                Energy[i] = AGB.Energy;
                EjectaMass[i] = AGB.EjectaMass;
                for(int k=0;k<CELibYield_Number;k++){
                    Elements[k][i] = AGB.Elements[k];
                }
            }
            Elements[CELibYield_Eu][i] = 0.e0;
        }

        float LogAge[ThisRun.LibNBin];
        float LogEnergy[ThisRun.LibNBin];
        float LogEjectaMass[ThisRun.LibNBin];
        float LogElements[CELibYield_Number][ThisRun.LibNBin];
        for(int i=0;i<ThisRun.LibNBin;i++){
            LogAge[i] = log10(Age[i]);
            if(Energy[i] > 0){
                LogEnergy[i] = log10(Energy[i]);
            } else {
                LogEnergy[i] = -20;
            }
            if(EjectaMass[i] > 0){
                LogEjectaMass[i] = log10(EjectaMass[i]);
            } else {
                LogEjectaMass[i] = -20;
            }
            for(int k=0;k<CELibYield_Number;k++){
                if(Elements[k][i] > 0){
                    LogElements[k][i] = log10(Elements[k][i]);
                } else {
                    LogElements[k][i] = -20;
                }
            }
        }
    
        cpgslw(4);
        int LineStyle[] = {1,2,3,4,5};
        for(int k=0;k<CELibYield_Number;k++){
            //cpgsci(k+1);
            cpgsci(CI[k]);
            int Line = k/3;
            cpgsls(LineStyle[Line]);
            cpgline(ThisRun.LibNBin,LogAge,LogElements[k]);
        }

        float y_start = -5.0;
        float y_step  = -0.5;
        float x_start = 8.5;
        float x_step  = 0.5;

        char Label[CELibYield_Number][MaxCharactersInLine] 
            ={"H","He","C","N","O","Ne","Mg","Si","S","Ca","Fe","Ni","Eu"};

        float x_[2] = {8.0,8.3};

        cpgslw(4);
        for(int i=0;i<CELibYield_Number-1;i++){
            //cpgsci(i+1);
            cpgsci(CI[i]);
            int Line = i/3;
            int Column = i%3;
            cpgtext(x_start+x_step*Column, y_start+y_step*Line,Label[i]);

            cpgsci(1);
            cpgsls(LineStyle[Line]);
            float y_[2] = {y_start+y_step*Line+0.2,y_start+y_step*Line+0.2};
            cpgline(2,x_,y_);
        }
        cpgslw(1);

        cpgsci(1);
        cpgsls(1);
        cpgslw(4);
        cpgsvp(0.2,0.9,0.22,0.9);
        cpglab("Age [yr]","Fraction","");

        ClosePGPlot();

        FILE *fp;
        char fname2[MaxCharactersInLine];
        Snprintf(fname2,"%s/Library.dat.%02d",ThisRun.OutDir,l);
        FileOpen(fp,fname2,"w");
        fprintf(fp,"#Age, Energy[erg], Total ejacta mass, H, He, C, N, O, Ne, Mg, Si, S, Ca, Fe, Ni, Eu\n");
        for(int i=0;i<ThisRun.LibNBin;i++){
            fprintf(fp,"%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n",
                    Age[i],Energy[i],EjectaMass[i],
                    Elements[CELibYield_H][i],Elements[CELibYield_He][i],
                    Elements[CELibYield_C][i],Elements[CELibYield_N][i],
                    Elements[CELibYield_O][i],Elements[CELibYield_Ne][i],
                    Elements[CELibYield_Mg][i],Elements[CELibYield_S][i],
                    Elements[CELibYield_Si][i],Elements[CELibYield_Ca][i],
                    Elements[CELibYield_Fe][i],Elements[CELibYield_Ni][i],
                    Elements[CELibYield_Eu][i]);
        }
        fclose(fp);
    }


    CELibLoadRunParameter();

    return ;
}
