#include "config.h"
#include "PGPlotOperations.h"
#include "../src/NSMRate.h"
#include "../src/NSMYields.h"
#include <cpgplot.h>

void PlotCumulativeNSMRate_i(const int ColorID, const int LineStyle){

    int Steps = 400;
    double HubbleTime = 1.38e10; // in year

    float x[Steps];
    float y[Steps];

    double dAge = log10(HubbleTime)/Steps;
    for(int i=0;i<Steps;i++){
        double Age = pow(10.0,dAge*(i+0.5));
        double Rate = CELibGetNSMIntegratedRatePowerLaw(Age);
        x[i] = log10(Age);
        if(Rate >0){
            y[i] = log10(Rate);
        } else {
            y[i] = -100;
        }
        // fprintf(stderr,"%d %g %g\n",i,x[i],y[i]);
    }


    cpgsci(ColorID);
    cpgsls(LineStyle);
    cpgline(Steps,x,y);


    return ;
}


void PlotNSMRate(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/NSMRate",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgslw(4);
    cpgswin(7,10.3,-7,-2.5);
    cpgbox("BCTSNL2",0,0.,"BCTSNL2",0.0,0.0);

    // cpgslw(2);
////////////////////////////////////////////////////
    CELibSetRunParameterNSMDTDPowerLawIndex(-1.0);
    CELibInitNSMYields();

    PlotCumulativeNSMRate_i(1,1);

    CELibSetRunParameterNSMDTDPowerLawIndex(-2.0);
    CELibInitNSMYields();

    PlotCumulativeNSMRate_i(2,2);

    CELibSetRunParameterNSMDTDPowerLawIndex(-0.5);
    CELibInitNSMYields();

    PlotCumulativeNSMRate_i(3,3);

    CELibSetRunParameterNSMDTDPowerLawIndex(-1.0);
    CELibSetRunParameterNSMDTDOffsetForPower(1.e+7);
    CELibInitNSMYields();

    PlotCumulativeNSMRate_i(4,4);

    CELibSetRunParameterNSMDTDPowerLawIndex(-1.0);
    CELibSetRunParameterNSMDTDOffsetForPower(1.e+9);
    CELibInitNSMYields();

    PlotCumulativeNSMRate_i(8,9);
////////////////////////////////////////////////////


    char label[][MaxCharactersInLine] ={
        "p\\dNSM\\u=-1",
        "p\\dNSM\\u=-2",
        "p\\dNSM\\u=-0.5",
        "p\\dNSM\\u=-1 (\\gt\\dNSM,min\\u=10\\u7\\dyr)",
        "p\\dNSM\\u=-1 (\\gt\\dNSM,min\\u=10\\u9\\dyr)"};
    int Colors[] = {1,2,3,4,8};
    int Lines[] = {1,2,3,4,9};
    cpgsch(1.5);
    for(int i=0;i<5;i++){
        cpgsci(Colors[i]);
        cpgsls(Lines[i]);
        float y_ = -2.8-0.30*i;
        cpgptxt(7.5,y_,0.0,0.0,label[i]);
        float xl[2] = {7.1,7.4};
        float yl[2] = {y_+0.05,y_+0.05};
        cpgline(2,xl,yl);
    }

    cpgsch(2);
    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Age [year]","Rate [M\\d\\(2281)\\u\\u-1\\d]","");

    ClosePGPlot();
}
