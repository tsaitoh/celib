#include "config.h"
#include "PGPlotOperations.h"
#include "../src/SNIIYields.h"
#include <cpgplot.h>

// #define WithLine 

static int TableID[] = {CELibYield_H,CELibYield_He,
    CELibYield_C, CELibYield_N, CELibYield_O,
    CELibYield_Ne,CELibYield_Mg,CELibYield_Si,
    CELibYield_S, CELibYield_Ca,CELibYield_Fe,CELibYield_Ni};

static void PlotXlabel(void){

    char label[][MaxCharactersInLine] ={"H","He","C","N","O","Ne","Mg","Si","S","Ca","Fe","Ni"};

    for(int i=0;i<12;i++){
        cpgptxt(i+1,-6.5,45,0.5,label[i]);
    }

    return; 
}

void PlotSNIIIntegratedYieldsInterpolated(char fname[]){

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgslw(4);
    cpgswin(0,13,-6,0);
    cpgbox("BCTS",0,0.,"BCTSNL2",0.0,0.0);

    int CI[] = {1,2,3,4,8};

    cpgsch(1.5);
    PlotXlabel();
    cpgsch(2);

#define SNIIYieldsPlotSize    (6)

    int PlotLength;
    double Z[SNIIYieldsPlotSize];
    char label[SNIIYieldsPlotSize][MaxCharactersInLine];
    if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_P98){
        Z[0] = 0.0004; Z[1] = 0.004; Z[2] = 0.008; Z[3] = 0.02; Z[4] = 0.05;
        sprintf(label[0],"Z=0.0004");
        sprintf(label[1],"Z=0.004");
        sprintf(label[2],"Z=0.008");
        sprintf(label[3],"Z=0.02");
        sprintf(label[4],"Z=0.05");
        PlotLength = 5;
    } else if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_N13){
        Z[0] = 0.0; Z[1] = 0.001; Z[2] = 0.004; Z[3] = 0.02; Z[4] = 0.05;
        sprintf(label[0],"Z=0");
        sprintf(label[1],"Z=0.001");
        sprintf(label[2],"Z=0.004");
        sprintf(label[3],"Z=0.02");
        sprintf(label[4],"Z=0.05");
        PlotLength = 5;
    }

    int TableSize = sizeof(TableID)/sizeof(int);
    for(int i=0;i<PlotLength;i++){

        float x_p[CELibYield_Number];
        float x_m[CELibYield_Number];
        float x_all[CELibYield_Number];
        float y_p[CELibYield_Number];
        float y_m[CELibYield_Number];
        float y_all[CELibYield_Number];

        int counter_p = 0;
        int counter_m = 0;
        int counter_all = 0;

        double tmpElements[CELibYield_Number];

        if(Z[i] <= CELibSNIIYieldsIntegrated[0].Metallicity){
            for(int k=0;k<CELibYield_Number;k++)
                tmpElements[k] = CELibSNIIYieldsIntegrated[0].Elements[k];
        } else if(Z[i] >= CELibSNIIYieldsIntegrated[CELibSNIIYields_Metallicity-1].Metallicity){
            for(int k=0;k<CELibYield_Number;k++)
                tmpElements[k] = CELibSNIIYieldsIntegrated[CELibSNIIYields_Metallicity-1].Elements[k];
        } else {
            int IndexMetallicity = 0;
            for(int k=1;k<CELibSNIIYields_Metallicity;k++){
                if(CELibSNIIYieldsIntegrated[k].Metallicity > Z[i]){
                    IndexMetallicity = k-1;
                    break;
                }
            }
            double MetallicityEdges[] = {CELibSNIIYieldsIntegrated[IndexMetallicity].Metallicity, 
                CELibSNIIYieldsIntegrated[IndexMetallicity+1].Metallicity};

            for(int k=0;k<CELibYield_Number;k++){
                double GradYield = (CELibSNIIYieldsIntegrated[IndexMetallicity+1].Elements[k]-
                        CELibSNIIYieldsIntegrated[IndexMetallicity].Elements[k])/
                            (MetallicityEdges[1]-MetallicityEdges[0]);
                tmpElements[k] = GradYield*(Z[i]-MetallicityEdges[0]) + 
                    CELibSNIIYieldsIntegrated[IndexMetallicity].Elements[k];
            }
        }

        double xshift = -0.2;
        double dx = 0.1;

        for(int k=0;k<CELibYield_Number;k++){

            double x = k+1;
            double y = tmpElements[k];

            if(y>0){
                x_p[counter_p] = x+i*dx+xshift;
                y_p[counter_p] = log10(y);
                counter_p ++;
            } else {
                x_m[counter_m] = x+i*dx+xshift;
                y_m[counter_m] = log10(fabs(y));
                counter_m ++;
            }

            x_all[counter_all] = x+i*dx+xshift;
            y_all[counter_all] = log10(fabs(y));
            counter_all ++;

        }

        //cpgsci(i+1);
        cpgsci(CI[i]);
        cpgsls(i+1);

        cpgsch(2.0);
        cpgpt(counter_p,x_p,y_p,4);
        cpgpt(counter_m,x_m,y_m,-8);
        cpgsfs(1);
#ifdef WithLine
        cpgline(counter_all-1,x_all,y_all);
#endif

#if 1
        cpgsch(1.3);
        float y_ = -0.5-0.3*i;
        cpgptxt(9.5,y_,0.0,0.0,label[i]);
#else
        cpgsch(1.5);
        float y_ = -0.7-0.4*i;
        cpgptxt(8.5,y_,0.0,0.0,label[i]);
        cpgpt1(7.5,y_+0.2,4);
#endif

        cpgsch(2.0);
        cpgpt1(9.0,y_+0.1,4);
    }

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.18,0.9,0.21,0.9);
    cpglab("Elements","Mass [M\\d\\(2281)\\u]","");

    ClosePGPlot();

    return ;
}

void PlotSNIIYields(void){

    CELibSaveRunParameter();

    char fname[MaxCharactersInLine];

    ///////////////
    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_P98);
    CELibRunParameters.SNIIYieldsModificationP98 = 0;
    CELibInit();
    Snprintf(fname,"%s/SNIIYieldsP98",ThisRun.OutDir);
    PlotSNIIIntegratedYieldsInterpolated(fname);

    ///////////////
    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_P98);
    CELibRunParameters.SNIIYieldsModificationP98 = 1;
    CELibInit();
    Snprintf(fname,"%s/SNIIYieldsP98mod",ThisRun.OutDir);
    PlotSNIIIntegratedYieldsInterpolated(fname);

    ///////////////
    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
    CELibSetRunParameterSNIIHyperNovaFraction(0);
    CELibSetRunParameterPopIIIIMF(0);
    CELibSetRunParameterPopIIISNe(1);
    CELibInit();
    Snprintf(fname,"%s/SNIIYieldsN13",ThisRun.OutDir);
    PlotSNIIIntegratedYieldsInterpolated(fname);

    ///////////////
    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
    CELibSetRunParameterSNIIHyperNovaFraction(0);
    CELibSetRunParameterPopIIIIMF(1);
    CELibSetRunParameterPopIIISNe(1);
    CELibInit();
    Snprintf(fname,"%s/SNIIYieldsN13PopIII",ThisRun.OutDir);
    PlotSNIIIntegratedYieldsInterpolated(fname);

    ///////////////
    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
    CELibSetRunParameterSNIIHyperNovaFraction(0.05);
    CELibSetRunParameterPopIIIIMF(0);
    CELibSetRunParameterPopIIISNe(1);
    CELibInit();
    Snprintf(fname,"%s/SNIIYieldsN13HN005",ThisRun.OutDir);
    PlotSNIIIntegratedYieldsInterpolated(fname);

    ///////////////
    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
    CELibSetRunParameterSNIIHyperNovaFraction(0.05);
    CELibSetRunParameterPopIIIIMF(1);
    CELibSetRunParameterPopIIISNe(1);
    CELibInit();
    Snprintf(fname,"%s/SNIIYieldsN13HN005PopIII",ThisRun.OutDir);
    PlotSNIIIntegratedYieldsInterpolated(fname);

    ///////////////
    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
    CELibSetRunParameterSNIIHyperNovaFraction(0.5);
    CELibSetRunParameterPopIIIIMF(0);
    CELibSetRunParameterPopIIISNe(1);
    CELibInit();
    Snprintf(fname,"%s/SNIIYieldsN13HN",ThisRun.OutDir);
    PlotSNIIIntegratedYieldsInterpolated(fname);

    ///////////////
    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
    CELibSetRunParameterSNIIHyperNovaFraction(0.5);
    CELibSetRunParameterPopIIIIMF(1);
    CELibSetRunParameterPopIIISNe(1);
    CELibInit();
    Snprintf(fname,"%s/SNIIYieldsN13HNPopIII",ThisRun.OutDir);
    PlotSNIIIntegratedYieldsInterpolated(fname);

    CELibLoadRunParameter();

    return ;
}


void CalcSNIIdata(void){

    CELibSaveRunParameter();

    FILE *fp;
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/SNIIEnergy.dat",ThisRun.OutDir);
    FileOpen(fp,fname,"w");

    fprintf(fp,"#################################\n");
    fprintf(fp,"#SNeII Energy, Return mass, etc.#\n");
    fprintf(fp,"#################################\n");

    for(int i=0;i<7;i++){
        fprintf(stderr,"#################################\n");
        CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_P98);
        CELibSetRunParameterIMFType(i);
        CELibInit();

        char IMFName[MaxCharactersInLine];
        CELibGetRunParameterIMFName(IMFName);

        fprintf(fp,"\n");
        fprintf(fp,"// IMF type %d\n",CELibRunParameters.IMFType);
        fprintf(fp,"// IMF Name: %s\n",IMFName);
        fprintf(fp,"// SNeII yields type %d\n",CELibRunParameters.SNIIYieldsTableID);

        for(int k=0;k<5;k++){
            fprintf(fp,"//\t Z = %g\n",CELibSNIIYieldsIntegrated[k].Metallicity);
            fprintf(fp,"//\t\t SNe Energy per Mass = %g\n",CELibSNIIYieldsIntegrated[k].Energy);
            fprintf(fp,"//\t\t SNe Return mass per Mass = %g\n",
                    CELibSNIIYieldsIntegrated[k].Msn+CELibSNIIYieldsIntegrated[k].Msw);
        }
        fflush(fp);
    }

    for(int i=0;i<7;i++){
        fprintf(stderr,"#################################\n");
        CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
        CELibSetRunParameterSNIIHyperNovaFraction(0);
        CELibSetRunParameterPopIIISNe(1);
        CELibSetRunParameterPopIIIIMF(1);
        CELibSetRunParameterIMFType(i);

        CELibInit();

        char IMFName[MaxCharactersInLine];
        CELibGetRunParameterIMFName(IMFName);

        fprintf(fp,"\n");
        fprintf(fp,"// IMF type %d\n",CELibRunParameters.IMFType);
        fprintf(fp,"// IMF Name: %s\n",IMFName);
        fprintf(fp,"// SNeII yields type %d\n",CELibRunParameters.SNIIYieldsTableID);
        fprintf(fp,"// SNeII Hypernova fraction %g\n",CELibRunParameters.SNIIHyperNovaFraction);

        for(int k=0;k<6;k++){
            fprintf(fp,"//\t Z = %g\n",CELibSNIIYieldsIntegrated[k].Metallicity);
            fprintf(fp,"//\t\t SNe Energy per Mass = %g\n",CELibSNIIYieldsIntegrated[k].Energy);
            fprintf(fp,"//\t\t SNe Return mass per Mass = %g\n",
                    CELibSNIIYieldsIntegrated[k].Msn+CELibSNIIYieldsIntegrated[k].Msw);
        }
        fflush(fp);
    }

    for(int i=0;i<7;i++){
        fprintf(stderr,"#################################\n");
        CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
        CELibSetRunParameterSNIIHyperNovaFraction(0.01);
        CELibSetRunParameterPopIIISNe(1);
        CELibSetRunParameterPopIIIIMF(1);
        CELibSetRunParameterIMFType(i);

        CELibInit();

        char IMFName[MaxCharactersInLine];
        CELibGetRunParameterIMFName(IMFName);


        fprintf(fp,"\n");
        fprintf(fp,"// IMF type %d\n",CELibRunParameters.IMFType);
        fprintf(fp,"// IMF Name: %s\n",IMFName);
        fprintf(fp,"// SNeII yields type %d\n",CELibRunParameters.SNIIYieldsTableID);
        fprintf(fp,"// SNeII Hypernova fraction %g\n",CELibRunParameters.SNIIHyperNovaFraction);

        for(int k=0;k<6;k++){
            fprintf(fp,"//\t Z = %g\n",CELibSNIIYieldsIntegrated[k].Metallicity);
            fprintf(fp,"//\t\t SNe Energy per Mass = %g\n",CELibSNIIYieldsIntegrated[k].Energy);
            fprintf(fp,"//\t\t SNe Return mass per Mass = %g\n",
                    CELibSNIIYieldsIntegrated[k].Msn+CELibSNIIYieldsIntegrated[k].Msw);
        }
        fflush(fp);
    }

    for(int i=0;i<7;i++){
        fprintf(stderr,"#################################\n");
        CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
        CELibSetRunParameterSNIIHyperNovaFraction(0.05);
        CELibSetRunParameterPopIIISNe(1);
        CELibSetRunParameterPopIIIIMF(1);
        CELibSetRunParameterIMFType(i);

        CELibInit();

        char IMFName[MaxCharactersInLine];
        CELibGetRunParameterIMFName(IMFName);

        fprintf(fp,"\n");
        fprintf(fp,"// IMF type %d\n",CELibRunParameters.IMFType);
        fprintf(fp,"// IMF Name: %s\n",IMFName);
        fprintf(fp,"// SNeII yields type %d\n",CELibRunParameters.SNIIYieldsTableID);
        fprintf(fp,"// SNeII Hypernova fraction %g\n",CELibRunParameters.SNIIHyperNovaFraction);

        for(int k=0;k<6;k++){
            fprintf(fp,"//\t Z = %g\n",CELibSNIIYieldsIntegrated[k].Metallicity);
            fprintf(fp,"//\t\t SNe Energy per Mass = %g\n",CELibSNIIYieldsIntegrated[k].Energy);
            fprintf(fp,"//\t\t SNe Return mass per Mass = %g\n",
                    CELibSNIIYieldsIntegrated[k].Msn+CELibSNIIYieldsIntegrated[k].Msw);
        }
        fflush(fp);
    }

    for(int i=0;i<7;i++){
        fprintf(stderr,"#################################\n");
        CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
        CELibSetRunParameterSNIIHyperNovaFraction(0.1);
        CELibSetRunParameterPopIIISNe(1);
        CELibSetRunParameterPopIIIIMF(1);
        CELibSetRunParameterIMFType(i);

        CELibInit();

        char IMFName[MaxCharactersInLine];
        CELibGetRunParameterIMFName(IMFName);

        fprintf(fp,"\n");
        fprintf(fp,"// IMF type %d\n",CELibRunParameters.IMFType);
        fprintf(fp,"// IMF Name: %s\n",IMFName);
        fprintf(fp,"// SNeII yields type %d\n",CELibRunParameters.SNIIYieldsTableID);
        fprintf(fp,"// SNeII Hypernova fraction %g\n",CELibRunParameters.SNIIHyperNovaFraction);

        for(int k=0;k<6;k++){
            fprintf(fp,"//\t Z = %g\n",CELibSNIIYieldsIntegrated[k].Metallicity);
            fprintf(fp,"//\t\t SNe Energy per Mass = %g\n",CELibSNIIYieldsIntegrated[k].Energy);
            fprintf(fp,"//\t\t SNe Return mass per Mass = %g\n",
                    CELibSNIIYieldsIntegrated[k].Msn+CELibSNIIYieldsIntegrated[k].Msw);
        }
        fflush(fp);
    }

    for(int i=0;i<7;i++){
        fprintf(stderr,"#################################\n");
        CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
        CELibSetRunParameterSNIIHyperNovaFraction(0.5);
        CELibSetRunParameterPopIIISNe(1);
        CELibSetRunParameterPopIIIIMF(1);
        CELibSetRunParameterIMFType(i);

        CELibInit();

        char IMFName[MaxCharactersInLine];
        CELibGetRunParameterIMFName(IMFName);

        fprintf(fp,"\n");
        fprintf(fp,"// IMF type %d\n",CELibRunParameters.IMFType);
        fprintf(fp,"// IMF Name: %s\n",IMFName);
        fprintf(fp,"// SNeII yields type %d\n",CELibRunParameters.SNIIYieldsTableID);
        fprintf(fp,"// SNeII Hypernova fraction %g\n",CELibRunParameters.SNIIHyperNovaFraction);

        for(int k=0;k<6;k++){
            fprintf(fp,"//\t Z = %g\n",CELibSNIIYieldsIntegrated[k].Metallicity);
            fprintf(fp,"//\t\t SNe Energy per Mass = %g\n",CELibSNIIYieldsIntegrated[k].Energy);
            fprintf(fp,"//\t\t SNe Return mass per Mass = %g\n",
                    CELibSNIIYieldsIntegrated[k].Msn+CELibSNIIYieldsIntegrated[k].Msw);
        }
        fflush(fp);
    }

    fclose(fp);

    CELibLoadRunParameter();
    CELibInit();

    return ;
}

#define PlotSNIIRateLogX (0)

void PlotSNIIRateForOneModel(void){
    
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/SNIIRate%d.%d",ThisRun.OutDir,
            CELibRunParameters.SNIIYieldsTableID,CELibRunParameters.IMFType);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);
    cpgsls(1);

    cpgslw(4);
#if PlotSNIIRateLogX
    cpgswin(-3,0,6,8);
    cpgbox("BCTSNL2",0,0.,"BCTSNL2",0.0,0.0);
#else
    cpgswin(0,1,6,8);
    cpgbox("BCTSN",0,0.,"BCTSNL2",0.0,0.0);
#endif

    char (*label)[MaxCharactersInLine];
    double *Z;
    char label_N13[][MaxCharactersInLine] ={"Z=0","Z=0.001","Z=0.004","Z=0.02","Z=0.05"};
    double Z_N13[] = {0,0.001,0.004,0.02,0.05};
    char label_P98[][MaxCharactersInLine] ={"Z=0.0004","Z=0.004","Z=0.008","Z=0.02","Z=0.05"};
    double Z_P98[] = {0.0004,0.004,0.008,0.02,0.05};

    if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_P98){
        label = label_P98;
        Z = Z_P98;
    } else if(CELibRunParameters.SNIIYieldsTableID == CELibSNIIYieldsTableID_N13){
        label = label_N13;
        Z = Z_N13;
    }

    int CI[] = {1,2,3,4,8};

    cpgsch(1.6);
    cpgslw(4);
    for(int i=0;i<5;i++){
#define TimeStep 1000
        float x[TimeStep];
        float y[TimeStep];

#define Rmin 1.e-4
#define Rmax 1


        int counter = 0;

#if PlotSNIIRateLogX
        double dR_log = (log10(Rmax)-log10(Rmin))/TimeStep;
#else
        double dR = 1.0/TimeStep;
#endif

        for(int k=0;k<TimeStep;k++){
#if PlotSNIIRateLogX
            double R = pow(10.0,log10(Rmin)+dR_log*(k+1));
#else
            double R = dR*(k+0.5);
#endif

            double Time = CELibGetSNIIExplosionTime(R,Z[i]);

            y[k] = Time;
            x[k] = R;

            // fprintf(stderr,"%g %g\n",Time,R);
            if(y[k] > 0.0){
#if PlotSNIIRateLogX
                x[counter] = log10(x[k]);
#else
                x[counter] = x[k];
#endif
                y[counter] = log10(y[k]);
                counter ++;
            }
        }

        //cpgsci(i+1);
        cpgsci(CI[i]);
        cpgsls(i+1);
        cpgline(counter,x,y);

#if PlotSNIIRateLogX
        cpgsch(1.6);
        float y_ = 7.8-0.08*i;
        cpgptxt(-2.2,y_,0.0,0.0,label[i]);
        float xl[2] = {-2.8,-2.3};
        float yl[2] = {y_+0.04,y_+0.04};
        cpgline(2,xl,yl);
#else
        cpgsch(1.6);
        float y_ = 7.8-0.1*i;
        cpgptxt(0.16,y_,0.0,0.0,label[i]);
        float xl[2] = {0.04,0.14};
        float yl[2] = {y_+0.03,y_+0.03};
        cpgline(2,xl,yl);
#endif

    }

    cpgsch(2);
    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Input","Time [year]","");
    cpgscf(1);

    ClosePGPlot();

    return ;
}

void PlotSNIIRate(void){

    CELibSaveRunParameter();
    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_N13);
    CELibInit();
    PlotSNIIRateForOneModel();

    CELibSetRunParameterSNIIYieldsTableID(CELibSNIIYieldsTableID_P98);
    CELibInit();
    PlotSNIIRateForOneModel();

    CELibLoadRunParameter();

    return ;
}


#include "../data/Portinari+1998/Portinari+1998_Struct.h"
#include "../data/Nomoto+2013/Nomoto+2013_Struct.h"

void PlotOandFeYields(void){

    /// Nomoto+2013
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/SNIIYieldsOFeN13",ThisRun.OutDir);
    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgswin(8,120,-2,1);
    cpgbox("BCTSN",0,0.,"BCTSNL2",0.0,0.0);


    for(int i=0;i<CELibSNIIYields_Metallicity_N13;i++){
        int counter = 0;
        for(int k=0;k<CELibSNIIYields_Mass_N13;k++){
            double O = CELibSNIIYieldsN13[i][k].Elements[CELibYield_O];
            if(O > 0.0){
                counter ++;
            }
        }
        dprint(counter);

        float Mass[counter];
        float O[counter];
        float Fe[counter];

        counter = 0;
        for(int k=0;k<CELibSNIIYields_Mass_N13;k++){
            double temp = CELibSNIIYieldsN13[i][k].Elements[CELibYield_O];
            if(temp > 0.0){
                Mass[counter] = CELibSNIIYieldsN13[i][k].Mass;
                O[counter] = log10(CELibSNIIYieldsN13[i][k].Elements[CELibYield_O]);
                Fe[counter] = log10(CELibSNIIYieldsN13[i][k].Elements[CELibYield_Fe]);

                fprintf(stderr,"%g %g %g\n",Mass[counter],O[counter],Fe[counter]);
                counter ++;
            }
        }
        
        dprint(counter);
        cpgsci(2);
        cpgline(counter,Mass,O);
        cpgsci(4);
        cpgline(counter,Mass,Fe);
        cpgsci(4);
    }

    cpgsch(2);
    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Mass [M\\d\\(2281)\\u]","O and Fe yields [M\\d\\(2281)\\u]","");
    cpgscf(1);

    ClosePGPlot();


    /// Portinari+1998
    Snprintf(fname,"%s/SNIIYieldsOFeP98",ThisRun.OutDir);
    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgswin(8,120,-2,1);
    cpgbox("BCTSN",0,0.,"BCTSNL2",0.0,0.0);


    for(int i=0;i<CELibSNIIYields_Metallicity_P98;i++){
        int counter = 0;
        for(int k=0;k<CELibSNIIYields_Mass_P98;k++){
            double O = CELibSNIIYieldsP98[i][k].Elements[CELibYield_O];
            if(O > 0.0){
                counter ++;
            }
        }
        dprint(counter);

        float Mass[counter];
        float O[counter];
        float Fe[counter];

        counter = 0;
        for(int k=0;k<CELibSNIIYields_Mass_P98;k++){
            double temp = CELibSNIIYieldsP98[i][k].Elements[CELibYield_O];
            if(temp > 0.0){
                Mass[counter] = CELibSNIIYieldsP98[i][k].Mass;
                O[counter] = log10(CELibSNIIYieldsP98[i][k].Elements[CELibYield_O]);
                Fe[counter] = log10(CELibSNIIYieldsP98[i][k].Elements[CELibYield_Fe]);

                fprintf(stderr,"%g %g %g\n",Mass[counter],O[counter],Fe[counter]);
                counter ++;
            }
        }
        
        dprint(counter);
        cpgsci(2);
        cpgline(counter,Mass,O);
        cpgsci(4);
        cpgline(counter,Mass,Fe);
        cpgsci(4);
    }

    cpgsch(2);
    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Mass [M\\d\\(2281)\\u]","O and Fe yields [M\\d\\(2281)\\u]","");
    cpgscf(1);

    ClosePGPlot();

    return ;
}



