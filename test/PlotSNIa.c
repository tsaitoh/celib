#include "config.h"
#include "PGPlotOperations.h"
#include "../src/SNIaYields.h"
#include "../data/Iwamoto+1999/Iwamoto+1999_Struct.h"
#include "../data/Travaglio+2004/Travaglio+2004_Struct.h"
#include "../data/Maeda+2010/Maeda+2010_Struct.h"
#include "../data/Seitenzahl+2013/Seitenzahl+2013_Struct.h"
#include <cpgplot.h>

static void PlotXlabel(void){

    char label[][MaxCharactersInLine] ={"H","He","C","N","O","Ne","Mg","Si","S","Ca","Fe","Ni"};

    for(int i=0;i<12;i++){
        cpgptxt(i+1,-7.5,45,0.5,label[i]);
    }

    return; 
}

void PlotSNIaYieldsI99(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/SNIaYieldsI99",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgslw(4);
    cpgswin(0,13,-7,0);
    cpgbox("BCTS",0,0.,"BCTSNL2",0.0,0.0);

    cpgsch(1.5);
    PlotXlabel();
    cpgsch(2);

    char label[][MaxCharactersInLine] ={"W7","W70","WDD1","WDD2","WDD3","CDD1","CDD2"};
    int CI[] = {1,2,3,4,8,10,15};

    //cpgslw(2);
    for(int i=0;i<7;i++){
        float x[CELibYield_Number];
        float y[CELibYield_Number];
        for(int k=0;k<CELibYield_Number;k++){
            x[k] = k+1;
            y[k] = CELibSNIaYieldsI99[i].Elements[k];
            if(y[k] > 0.e0) y[k] = log10(y[k]);
        }
        cpgsci(CI[i]);
        //cpgsci(i+1);
        //if(i+1==7) cpgsci(i+2);
        cpgsls(i+1);
        cpgline(CELibYield_Number-3,x+2,y+2);

        float y_ = -3.5-0.40*i;
        cpgptxt(8.5,y_,0.0,0.0,label[i]);
        float xl[2] = {7,8};
        float yl[2] = {y_+0.15,y_+0.15};
        cpgline(2,xl,yl);

    }

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Elements","Mass [M\\d\\(2281)\\u]","");
    cpgslw(1);

    ClosePGPlot();


    return ;
}

void PlotSNIaYieldsT04(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/SNIaYieldsT04",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgslw(4);
    cpgswin(0,13,-7,0);
    cpgbox("BCTS",0,0.,"BCTSNL2",0.0,0.0);

    cpgsch(1.5);
    PlotXlabel();
    cpgsch(2);

    char label[][MaxCharactersInLine] ={"W7","c3_2d_512","c3_3d_256","c3_3d_256","b5_3d_256","b30_3d_768"};
    int CI[] = {1,2,3,4,8,10,15};

    //cpgslw(2);
    for(int i=0;i<CELibSNIaYieldTableModelNumber_T04;i++){
        float x[CELibYield_Number];
        float y[CELibYield_Number];
        for(int k=0;k<CELibYield_Number;k++){
            x[k] = k+1;
            y[k] = CELibSNIaYieldsI99[i].Elements[k];
            if(y[k] > 0.e0) y[k] = log10(y[k]);
        }
        cpgsci(CI[i]);
        //cpgsci(i+1);
        //if(i+1==7) cpgsci(i+2);
        cpgsls(i+1);
        cpgline(CELibYield_Number-3,x+2,y+2);

        float y_ = -3.5-0.40*i;
        cpgptxt(7.3,y_,0.0,0.0,label[i]);
        float xl[2] = {6,7};
        float yl[2] = {y_+0.15,y_+0.15};
        cpgline(2,xl,yl);

    }

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Elements","Mass [M\\d\\(2281)\\u]","");

    cpgslw(1);
    ClosePGPlot();


    return ;
}

void PlotSNIaYieldsM10(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/SNIaYieldsM10",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgslw(4);
    cpgswin(0,13,-7,0);
    cpgbox("BCTS",0,0.,"BCTSNL2",0.0,0.0);

    cpgsch(1.5);
    PlotXlabel();
    cpgsch(2);

    char label[][MaxCharactersInLine] ={"W7","C-DEF","C-DDT","O-DDT"};
    int CI[] = {1,2,3,4,8,10,15};

    //cpgslw(2);
    for(int i=0;i<4;i++){
        float x[CELibYield_Number];
        float y[CELibYield_Number];
        for(int k=0;k<CELibYield_Number;k++){
            x[k] = k+1;
            y[k] = CELibSNIaYieldsM10[i].Elements[k];
            if(y[k] > 0.e0) y[k] = log10(y[k]);
        }
        //cpgsci(i+1);
        //if(i+1==7) cpgsci(i+2);
        cpgsci(CI[i]);
        cpgsls(i+1);
        cpgline(CELibYield_Number-3,x+2,y+2);

        float y_ = -3.5-0.40*i;
        cpgptxt(8.5,y_,0.0,0.0,label[i]);
        float xl[2] = {7,8};
        float yl[2] = {y_+0.15,y_+0.15};
        cpgline(2,xl,yl);

    }

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Elements","Mass [M\\d\\(2281)\\u]","");
    cpgslw(1);

    ClosePGPlot();


    return ;
}

void PlotSNIaYieldsS13(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/SNIaYieldsS13",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgslw(4);
    cpgswin(0,13,-7,0);
    cpgbox("BCTS",0,0.,"BCTSNL2",0.0,0.0);

    cpgsch(1.5);
    PlotXlabel();
    cpgsch(2);

    char label[][MaxCharactersInLine] ={"N1","N10","N40","N100","N300","N1600","N100_Z0.01"};
    //int TableID[] = {0,3,5,7,11,12,16};
    int TableID[] = {CELibSNIaYieldsTableModelID_S13_N1,CELibSNIaYieldsTableModelID_S13_N10,
                     CELibSNIaYieldsTableModelID_S13_N40,CELibSNIaYieldsTableModelID_S13_N100,
                     CELibSNIaYieldsTableModelID_S13_N300C,CELibSNIaYieldsTableModelID_S13_N1600,
                     CELibSNIaYieldsTableModelID_S13_N100Z001};
    int CI[] = {1,2,3,4,8,10,15};

    //cpgslw(4);
    for(int i=0;i<7;i++){
        float x[CELibYield_Number];
        float y[CELibYield_Number];
        fprintf(stderr,"Model Name %s\n",CELibSNIaYieldsS13[TableID[i]].Name);
        for(int k=0;k<CELibYield_Number;k++){
            x[k] = k+1;
            y[k] = CELibSNIaYieldsS13[TableID[i]].Elements[k];
            if(y[k] > 0.e0) y[k] = log10(y[k]);
        }
        cpgsci(CI[i]);
        // cpgsci(i+1);
        // if(i+1==7) cpgsci(i+2);
        cpgsls(i+1);
        cpgline(CELibYield_Number-3,x+2,y+2);

        float y_ = -3.5-0.40*i;
        cpgptxt(8.5-1.0,y_,0.0,0.0,label[i]);
        float xl[2] = {7-1.0,8-1.0};
        float yl[2] = {y_+0.15,y_+0.15};
        cpgline(2,xl,yl);

    }

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Elements","Mass [M\\d\\(2281)\\u]","");
    cpgslw(1);

    ClosePGPlot();


    return ;
}


/*
 * This function returns the metallicity dependent total return mass of an
 * element Index. This function uses the yield tables of N100, N100_Z0.5,
 * N100_Z0.1, N100_Z0.01. We assume that the metallicity ($^{22}$Ne) is the same
 * as the solar abundance ratio of Asplund et al. (2009).
 */
#define Z_solar (0.013)
static double pCELibSNIaYieldsGetMetalDependYield(const int Index, const double Metallicity){

    if(Metallicity <= 0.01*Z_solar){
        return CELibSNIaYieldsS13[16].Elements[Index]; 
    } else if(Metallicity >= Z_solar){
        return CELibSNIaYieldsS13[7].Elements[Index]; 
    } else  if(Metallicity < 0.1*Z_solar){
        // Use 16 and 15
        double Grad = (CELibSNIaYieldsS13[15].Elements[Index]-CELibSNIaYieldsS13[16].Elements[Index])/(0.1*Z_solar-0.01*Z_solar);
        return Grad*(Metallicity-0.01*Z_solar)+CELibSNIaYieldsS13[16].Elements[Index];
    } else  if(Metallicity < 0.5*Z_solar){
        // Use 15 and 14
        double Grad = (CELibSNIaYieldsS13[14].Elements[Index]-CELibSNIaYieldsS13[15].Elements[Index])/(0.5*Z_solar-0.1*Z_solar);
        return Grad*(Metallicity-0.1*Z_solar)+CELibSNIaYieldsS13[15].Elements[Index];
    } else  if(Metallicity < Z_solar){
        // Use 14 and 7
        double Grad = (CELibSNIaYieldsS13[7].Elements[Index]-CELibSNIaYieldsS13[14].Elements[Index])/(Z_solar-0.5*Z_solar);
        return Grad*(Metallicity-0.5*Z_solar)+CELibSNIaYieldsS13[14].Elements[Index];
    } 
    return 0.e0;
}


void PlotSNIaYieldsS13MetalDepend(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/SNIaYieldsS13MetalDepend",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgslw(4);
    cpgswin(0,13,-7,0);
    cpgbox("BCTS",0,0.,"BCTSNL2",0.0,0.0);

    cpgsch(1.5);
    PlotXlabel();
    cpgsch(2);

    char label[][MaxCharactersInLine] ={"Z=10\\u-4\\d","Z=5x10\\u-4\\d","Z=10\\u-3\\d","Z=5x10\\u-3\\d","Z=10\\u-2\\d","Z=5x10\\u-2\\d"};
    double Z[] = {1.e-4,5e-4,1.e-3,5e-3,1.e-2,5e-2};

    int CI[] = {1,2,3,4,8,10};

    cpgslw(4);
    for(int i=0;i<6;i++){
        float x[CELibYield_Number];
        float y[CELibYield_Number];
        fprintf(stderr,"Model Name Seitenzahl+2013, metal depend yields.\n");
        for(int k=0;k<CELibYield_Number;k++){
            x[k] = k+1;
            y[k] = pCELibSNIaYieldsGetMetalDependYield(k,Z[i]);
            if(y[k] > 0.e0) y[k] = log10(y[k]);
        }
        //cpgsci(i+1);
        cpgsci(CI[i]);
        if(i+1==7) cpgsci(i+2);
        cpgsls(i+1);
        cpgline(CELibYield_Number-3,x+2,y+2);

        float y_ = -3.5-0.55*i;
        cpgptxt(8.5-1.0,y_,0.0,0.0,label[i]);
        float xl[2] = {6.8-1.0,8-1.0};
        float yl[2] = {y_+0.15,y_+0.15};
        cpgline(2,xl,yl);

    }

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Elements","Mass [M\\d\\(2281)\\u]","");
    cpgslw(1);

    ClosePGPlot();


    return ;
}

void PlotSNIaYieldsS13MetalDependDiff(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/SNIaYieldsS13MetalDependDiff",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgslw(4);
    cpgswin(0,13,-0.023,+0.023);
    cpgbox("BCTS",0,0.,"BCTSN",0.0,0.0);

    cpgsch(1.5);
    char _label[][MaxCharactersInLine] ={"H","He","C","N","O","Ne","Mg","Si","S","Ca","Fe","Ni"};
    for(int i=0;i<12;i++){
        cpgptxt(i+1,-0.027,45,0.5,_label[i]);
    }
    cpgsch(2);

    char label[][MaxCharactersInLine] ={"Z=0.0001","Z=0.0005","Z=0.001\\d","Z=0.005\\d","Z=0.01","Z=0.05"};
    double Z[] = {0.0001,0.0005,0.001,0.005,0.01,0.05};

    int CI[] = {1,2,3,4,8,10};

    cpgslw(4);
    cpgsch(1.6);
    for(int i=0;i<6;i++){
        float x[CELibYield_Number];
        float y[CELibYield_Number];
        fprintf(stderr,"Model Name Seitenzahl+2013, metal depend yields.\n");
        for(int k=0;k<CELibYield_Number;k++){
            x[k] = k+1;
            y[k] = pCELibSNIaYieldsGetMetalDependYield(k,Z[i])-pCELibSNIaYieldsGetMetalDependYield(k,0.013);
        }
        //cpgsci(i+1);
        cpgsci(CI[i]);
        if(i+1==7) cpgsci(i+2);
        cpgsls(i+1);
        cpgline(CELibYield_Number-3,x+2,y+2);

        float y_ = 0.019-0.0025*i;
        cpgptxt(3.0-1.0,y_,0.0,0.0,label[i]);
        float xl[2] = {1.5-1.0,2.8-1.0};
        float yl[2] = {y_+0.001,y_+0.001};
        cpgline(2,xl,yl);

    }
    cpgsch(2);

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Elements","Mass [M\\d\\(2281)\\u]","");
    cpgslw(1);

    ClosePGPlot();


    return ;
}

void PlotSNIaYields4Models(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/SNIaYields4Models",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgslw(4);
    cpgswin(0,13,-7,0);
    cpgbox("BCTS",0,0.,"BCTSNL2",0.0,0.0);

    cpgsch(1.5);
    PlotXlabel();
    cpgsch(2);

    char label[][MaxCharactersInLine] ={"Iwamoto+99 W7","Travaglio+04","Maeda+10 O-DDT","Seitenzahl+13 N100"};
    char label2[][MaxCharactersInLine] ={"    b30_3d_768"};

    int CI[] = {1,2,3,4,8};

    cpgslw(4);
    cpgsch(1.4);
    for(int i=0;i<4;i++){
        float x[CELibYield_Number];
        float y[CELibYield_Number];
        if(i==0){
            fprintf(stderr,"Model Name %s\n",CELibSNIaYieldsI99[0].Name);
        } else if(i==1){
            fprintf(stderr,"Model Name %s\n",CELibSNIaYieldsT04[5].Name);
        } else if(i==2){
            fprintf(stderr,"Model Name %s\n",CELibSNIaYieldsM10[3].Name);
        } else if(i==3){
            fprintf(stderr,"Model Name %s\n",CELibSNIaYieldsS13[7].Name);
        }
        for(int k=0;k<CELibYield_Number;k++){
            x[k] = k+1;
            if(i==0){
                y[k] = CELibSNIaYieldsI99[0].Elements[k];
            } else if(i==1){
                y[k] = CELibSNIaYieldsT04[5].Elements[k];
            } else if(i==2){
                y[k] = CELibSNIaYieldsM10[3].Elements[k];
            } else if(i==3){
                y[k] = CELibSNIaYieldsS13[7].Elements[k];
            }
            if(y[k] > 0.e0) y[k] = log10(y[k]);
        }
        //cpgsci(i+1);
        cpgsci(CI[i]);
        if(i+1==7) cpgsci(i+2);
        cpgsls(i+1);
        cpgline(CELibYield_Number-3,x+2,y+2);

        float y_ = -4.5-0.45*i;
        if(i>1) y_ -= 0.45;
        cpgptxt(8.5-2.3,y_,0.0,0.0,label[i]);
        if(i == 1)
            cpgptxt(8.5-2.3,y_-0.45,0.0,0.0,label2[0]);

        float xl[2] = {7-2.3,8-2.3};
        float yl[2] = {y_+0.1,y_+0.1};
        cpgline(2,xl,yl);

    }
    cpgsch(2);

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.19,0.9,0.22,0.9);
    cpglab("Elements","Mass [M\\d\\(2281)\\u]","");
    cpgslw(1);

    ClosePGPlot();


    return ;
}

static void WriteSNIaYieldsXFe(void){

    FILE *fp;
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/SNIaYieldsXFe",ThisRun.OutDir);

    FileOpen(fp,fname,"w");

    // I99
    char Label[][MaxCharactersInLine] = {"H","He","C","N","O","Ne","Mg","S","Si","Ca","Fe","Ni"};
    for(int k=2;k<CELibYield_Number-1;k++){
        fprintf(fp,"%02s ",Label[k]);
    }
    fprintf(fp,"\n");
    for(int i=0;i<CELibSNIaYieldTableModelNumber_I99;i++){
        fprintf(fp,"#%s\n",CELibSNIaYieldsI99[i].Name);
        /*
        for(int k=2;k<CELibYield_Number-1;k++){
            fprintf(fp,"%06s ",Label[k]);
        }
        fprintf(fp,"\n");
        */
        for(int k=2;k<CELibYield_Number-1;k++){
            double XFe = CELibSNIaYieldsI99[i].Elements[k]/CELibSNIaYieldsI99[i].Elements[CELibYield_Fe];
            fprintf(fp,"%1.3g ",XFe);
        }
        fprintf(fp,"\n");
    }
    fprintf(fp,"\n");

    // T04
    for(int i=0;i<CELibSNIaYieldTableModelNumber_T04;i++){
        fprintf(fp,"#%s\n",CELibSNIaYieldsT04[i].Name);
        /*
        for(int k=2;k<CELibYield_Number-1;k++){
            fprintf(fp,"%06s ",Label[k]);
        }
        fprintf(fp,"\n");
        */
        for(int k=2;k<CELibYield_Number-1;k++){
            double XFe = CELibSNIaYieldsT04[i].Elements[k]/CELibSNIaYieldsT04[i].Elements[CELibYield_Fe];
            fprintf(fp,"%1.3g ",XFe);
        }
        fprintf(fp,"\n");
    }
    fprintf(fp,"\n");



    // M10
    for(int i=0;i<CELibSNIaYieldTableModelNumber_M10;i++){
        fprintf(fp,"#%s\n",CELibSNIaYieldsM10[i].Name);
        /*
        for(int k=2;k<CELibYield_Number-1;k++){
            fprintf(fp,"%06s ",Label[k]);
        }
        fprintf(fp,"\n");
        */
        for(int k=2;k<CELibYield_Number-1;k++){
            double XFe = CELibSNIaYieldsM10[i].Elements[k]/CELibSNIaYieldsM10[i].Elements[CELibYield_Fe];
            fprintf(fp,"%1.3g ",XFe);
        }
        fprintf(fp,"\n");
    }
    fprintf(fp,"\n");

    // S13
    for(int i=0;i<CELibSNIaYieldTableModelNumber_S13;i++){
        fprintf(fp,"#%s\n",CELibSNIaYieldsS13[i].Name);
        /*
        for(int k=2;k<CELibYield_Number-1;k++){
            fprintf(fp,"%06s ",Label[k]);
        }
        fprintf(fp,"\n");
        */
        for(int k=2;k<CELibYield_Number-1;k++){
            double XFe = CELibSNIaYieldsS13[i].Elements[k]/CELibSNIaYieldsS13[i].Elements[CELibYield_Fe];
            fprintf(fp,"%1.3g ",XFe);
        }
        fprintf(fp,"\n");
    }
    fprintf(fp,"\n");

    fclose(fp);
    return;
}

void PlotSNIaYields(void){

    PlotSNIaYieldsI99();
    PlotSNIaYieldsT04();
    PlotSNIaYieldsM10();
    PlotSNIaYieldsS13();
    PlotSNIaYieldsS13MetalDepend();
    PlotSNIaYieldsS13MetalDependDiff();
    PlotSNIaYields4Models();

    WriteSNIaYieldsXFe();

    return ;
}

void PlotCumulativeSNIaRate(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/SNIaRate",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgslw(4);
    cpgswin(7,10.3,-5,-2);
    cpgbox("BCTSNL2",0,0.,"BCTSNL2",0.0,0.0);

    int Steps = 400;
    double HubbleTime = 1.38e10; // in year

    float x[Steps];
    float yPL[Steps];
    float yGR0[Steps];
    float yGR1[Steps];

    double dAge = log10(HubbleTime)/Steps;
    for(int i=0;i<Steps;i++){
        double Age = pow(10.0,dAge*(i+0.5));

        double Mass = CELibGetDyingStellarMass(Age,0.e0);
        double RatePL = CELibGetSNIaIntegratedRatePowerLaw(Age);
        double RateGreggioRenzini0 = CELibGetSNIaIntegratedRateGreggioRenzini(Age,0.0);
        double RateGreggioRenzini1 = CELibGetSNIaIntegratedRateGreggioRenzini(Age,0.02);

        x[i] = log10(Age);
        if(RatePL>0){
            yPL[i] = log10(RatePL);
        } else {
            yPL[i] = -100;
        }

        if(RateGreggioRenzini0 >0){
            yGR0[i] = log10(RateGreggioRenzini0);
        } else {
            yGR0[i] = -100;
        }

        if(RateGreggioRenzini1 >0){
            yGR1[i] = log10(RateGreggioRenzini1);
        } else {
            yGR1[i] = -100;
        }


    }


    //cpgslw(2);
    cpgsci(1);
    cpgsls(1);
    cpgline(Steps,x,yPL);

    cpgsci(2);
    cpgsls(2);
    cpgline(Steps,x,yGR0);

    cpgsci(3);
    cpgsls(3);
    cpgline(Steps,x,yGR1);

    char label[][MaxCharactersInLine] ={"Power law","Greggio&Renzini 83(Z=0)","Greggio&Renzini 83(Z=0.02)"};
    //int Colors[] = {1,2,4};
    int Colors[] = {1,2,3};
    int Lines[] = {1,2,3};
    cpgsch(1.6);
    for(int i=0;i<3;i++){
        cpgsci(Colors[i]);
        cpgsls(Lines[i]);
        float y_ = -2.2-0.17*i;
        cpgptxt(7.6,y_,0.0,0.0,label[i]);
        float xl[2] = {7.2,7.5};
        float yl[2] = {y_+0.05,y_+0.05};
        cpgline(2,xl,yl);
    }

    cpgsch(2);
    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Age [year]","Rate [M\\d\\(2281)\\u\\u-1\\d]","");

    ClosePGPlot();

    return ;
}


void PlotCumulativeSNIaRateWithV13(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/SNIaRateFull",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(0.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgsls(1);
    cpgslw(4);
    cpgswin(7,10.3,-5,-2);
    cpgbox("BCTSNL2",0,0.,"BCTSNL2",0.0,0.0);

    int Steps = 400;
    double HubbleTime = 1.38e10; // in year

    float x[Steps];
    float yPL[Steps];
    float yV13[Steps];
    float yGR0[Steps];
    float yGR1[Steps];

    double dAge = log10(HubbleTime)/Steps;
    for(int i=0;i<Steps;i++){
        double Age = pow(10.0,dAge*(i+0.5));

        double Mass = CELibGetDyingStellarMass(Age,0.e0);
        double RatePL = CELibGetSNIaIntegratedRatePowerLaw(Age);
        double RateV13 = CELibGetSNIaIntegratedRateVogelsberger(Age);
        double RateGreggioRenzini0 = CELibGetSNIaIntegratedRateGreggioRenzini(Age,0.0);
        double RateGreggioRenzini1 = CELibGetSNIaIntegratedRateGreggioRenzini(Age,0.02);

        x[i] = log10(Age);
        if(RatePL>0){
            yPL[i] = log10(RatePL);
        } else {
            yPL[i] = -100;
        }
        if(RateV13>0){
            yV13[i] = log10(RateV13);
        } else {
            yV13[i] = -100;
        }

        if(RateGreggioRenzini0 >0){
            yGR0[i] = log10(RateGreggioRenzini0);
        } else {
            yGR0[i] = -100;
        }

        if(RateGreggioRenzini1 >0){
            yGR1[i] = log10(RateGreggioRenzini1);
        } else {
            yGR1[i] = -100;
        }


    }


    cpgslw(4);
    cpgsci(1);
    cpgsls(1);
    cpgline(Steps,x,yPL);

    cpgsci(2);
    cpgsls(9);
    cpgline(Steps,x,yV13);

    cpgsci(3);
    cpgsls(2);
    cpgline(Steps,x,yGR0);

    cpgsci(4);
    cpgsls(3);
    cpgline(Steps,x,yGR1);

    char label[][MaxCharactersInLine] ={"Power law","Power law (V13)","Greggio&Renzini 83(Z=0)","Greggio&Renzini 83(Z=0.02)"};
    int Colors[] = {1,2,3,4};
    int Lines[] = {1,9,2,3};
    cpgsch(1.6);
    for(int i=0;i<4;i++){
        cpgsci(Colors[i]);
        cpgsls(Lines[i]);
        float y_ = -2.2-0.17*i;
        cpgptxt(7.6,y_,0.0,0.0,label[i]);
        float xl[2] = {7.2,7.5};
        float yl[2] = {y_+0.05,y_+0.05};
        cpgline(2,xl,yl);
    }

    cpgsch(2);
    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Age [year]","Rate [M\\d\\(2281)\\u\\u-1\\d]","");

    ClosePGPlot();

    return ;
}


