#include "config.h"
#include "PGPlotOperations.h"
#include <cpgplot.h>

static void PlotStarbyStarEventTime(void){

    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/StarbyStarEventTime",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(8.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgswin(log10(0.3),log10(350),6,11);
    cpgslw(4);
    cpgbox("BCTSNL",0,0.,"BCTSNL",0.0,0.0);

    double Z[] = {0,0.0004,0.004,0.02,0.05};
    int CI[] = {1,2,3,4,8};

    char Label[5][MaxCharactersInLine];
    sprintf(Label[0],"Z=0");
    sprintf(Label[1],"Z=0.0004");
    sprintf(Label[2],"Z=0.004");
    sprintf(Label[3],"Z=0.02");
    sprintf(Label[4],"Z=0.05");

    cpgsch(1.6);
    for(int k=0;k<5;k++){
        double MassMin = 0.6;
        double MassMax = 120;
        if(k==0) MassMax = 300;
        int Nbin = 100;
        double dMass = (log10(MassMax)-log10(MassMin))/((double)Nbin);
        float MassArray[Nbin];
        float LifeTimeArray[Nbin];
        for(int i=0;i<Nbin;i++){
            double Mass = pow(10.0,log10(MassMin)+dMass*(i+0.5));
        
            struct CELibStructNextEventTimeStarbyStarInput Input;
            Input.InitialMass_in_Msun = Mass;
            Input.Metallicity = Z[k];

            double LifeTime = CELibGetNextEventTimeStarbyStar(Input,CELibFeedbackType_SNII);
        
            // fprintf(stderr,"%g %g\n",Mass,LifeTime);

            MassArray[i] = log10(Mass);
            if(LifeTime > 0){
                LifeTimeArray[i] = log10(LifeTime);
            } else {
                LifeTimeArray[i] = 0;
            }
        }

        cpgsci(CI[k]);
        cpgsls(k+1);
        cpgline(Nbin,MassArray,LifeTimeArray);
        float y_ = 10.5-0.35*k;
        cpgptxt(1.3,y_,0.0,0.0,Label[k]);
        float xl[2] = {0.9,1.2};
        float yl[2] = {y_+0.1,y_+0.1};
        cpgline(2,xl,yl);
    }
    cpgsch(2);

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Mass [M\\d\\(2281)\\u]","Lifetime [yr]","");

    ClosePGPlot();

    return ;
}

static void PlotStarbyStarYields(void){

    double Z[] = {0.0,0.0004,0.004,0.02,0.05};
    int NZ = sizeof(Z)/sizeof(double);

    for(int l=0;l<NZ;l++){
        char fname[MaxCharactersInLine];
        Snprintf(fname,"%s/StarbyStarYields.%d",ThisRun.OutDir,l);

        OpenPGPlot(fname,ThisRun.PlotMode);
        cpgpap(8.0,1.0);

        cpgsch(2.0);
        cpgsvp(0.2,0.9,0.2,0.9);

        cpgswin(0.5,3.0,-6,2.5);
        cpgslw(4);
        cpgbox("BCTSNL",0,0.,"BCTSNL",0.0,0.0);


        double MassMin = 0.1;
        double MassMax = 1000;
        int Nbin = 100;
        double dMass = (log10(MassMax)-log10(MassMin))/((double)Nbin);
        float MassArray[Nbin];
        float Elements[CELibYield_Number][Nbin];
        for(int i=0;i<Nbin;i++){
            double Mass = pow(10.0,log10(MassMin)+dMass*(i+0.5));
        
            double OriginalElements[CELibYield_Number];
            CELibSetPrimordialMetallicity(Mass,OriginalElements);

            struct CELibStructFeedbackStarbyStarOutput Star = 
                CELibGetFeedbackStarbyStar((struct CELibStructFeedbackStarbyStarInput){
                        .Mass = Mass,
                        .Metallicity = Z[l],
                        .MassConversionFactor = 1.0,
                        .Elements = OriginalElements,
                        .noPopIII = 0,
                        },CELibFeedbackType_SNII);

            MassArray[i] = log10(Mass);

            if(Star.Elements[5] > 0.0){
                for(int k=0;k<CELibYield_Number-1;k++){
                    Elements[k][i] = log10(Star.Elements[k]);
                }
            } else {
                for(int k=0;k<CELibYield_Number-1;k++){
                    Elements[k][i] = -100;
                }
            }
        }


        cpgslw(4);
        for(int k=0;k<CELibYield_Number-1;k++){
            cpgsci(k+1);
            cpgline(Nbin,MassArray,Elements[k]);
        }

        cpgsci(1);
        cpgsls(1);
        cpgsvp(0.2,0.9,0.23,0.9);
        cpglab("Mass [M\\d\\(2281)\\u]","Ys [M\\d\\(2281)\\u]","");

        ClosePGPlot();
    }


    /// noPopIIIflag
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/StarbyStarYields.noPopIII",ThisRun.OutDir);

    OpenPGPlot(fname,ThisRun.PlotMode);
    cpgpap(8.0,1.0);

    cpgsch(2.0);
    cpgsvp(0.2,0.9,0.2,0.9);

    cpgswin(0.5,3.0,-6,2.5);
    cpgslw(4);
    cpgbox("BCTSNL",0,0.,"BCTSNL",0.0,0.0);


    double MassMin = 0.1;
    double MassMax = 1000;
    int Nbin = 100;
    double dMass = (log10(MassMax)-log10(MassMin))/((double)Nbin);
    float MassArray[Nbin];
    float Elements[CELibYield_Number][Nbin];
    for(int i=0;i<Nbin;i++){
        double Mass = pow(10.0,log10(MassMin)+dMass*(i+0.5));
    
        double OriginalElements[CELibYield_Number];
        CELibSetPrimordialMetallicity(Mass,OriginalElements);

        struct CELibStructFeedbackStarbyStarOutput Star = 
            CELibGetFeedbackStarbyStar((struct CELibStructFeedbackStarbyStarInput){
                    .Mass = Mass,
                    .Metallicity = 0.0,
                    .MassConversionFactor = 1.0,
                    .Elements = OriginalElements,
                    .noPopIII = 1,
                    },CELibFeedbackType_SNII);

        MassArray[i] = log10(Mass);

        if(Star.Elements[5] > 0.0){
            for(int k=0;k<CELibYield_Number-1;k++){
                Elements[k][i] = log10(Star.Elements[k]);
            }
        } else {
            for(int k=0;k<CELibYield_Number-1;k++){
                Elements[k][i] = -100;
            }
        }
    }


    cpgslw(4);
    for(int k=0;k<CELibYield_Number-1;k++){
        cpgsci(k+1);
        cpgline(Nbin,MassArray,Elements[k]);
    }

    cpgsci(1);
    cpgsls(1);
    cpgsvp(0.2,0.9,0.23,0.9);
    cpglab("Mass [M\\d\\(2281)\\u]","Ys [M\\d\\(2281)\\u]","");

    ClosePGPlot();


    return ;
}

void PlotStarbyStar(void){
    
    PlotStarbyStarEventTime();
    PlotStarbyStarYields();

    return ;
}
