/* RandomNumberGenerator.c */
void InitializeRandomGenerator(unsigned long int RandomSeed);
void AllocateRandomGenerator(void);
void ResetRandomSeedForRandomGenerator(unsigned long int RandomSeed);
