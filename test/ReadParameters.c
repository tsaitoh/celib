#include "config.h"

/*! \file ReadParameters.c
 * \brief Functions for reading a parameter file. 
 */

/*!
 * Read parameters.
 */
enum{
    IP_OutDir,

    IP_PlotMode,

    IP_NSample,
    IP_InitialMass,
    IP_Metallicity,

    IP_SNIIYieldType,

    IP_NSNIa,
    IP_NNSM,

    IP_PopIIIIMF,
    IP_PopIIISNe,

    IP_TStart,
    IP_TEnd,
    IP_dt,

    IP_SSPMassMin,
    IP_SSPMassMax,
    IP_SSPNMassBin,
    IP_SSPMetallicity,

    IP_SSPZMin,
    IP_SSPZMax,
    IP_SSPNZBin,

    IP_LibTime,
    IP_LibTimeStep,
    IP_LibNBin,
    IP_LibType,
    IP_LibTimeMin,
    IP_LibTimeMax,
    IP_LibMetalicity_00,
    IP_LibMetalicity_01,

    IP_OZTime,
    IP_OZMgas,
    IP_OZFinalMstar,
    IP_OZSFRType,
    IP_OZExpScale,
    IP_OZTimeBin,
    IP_OZPlotType,

    IP_OZUseSNII,
    IP_OZUseSNIa,
    IP_OZUseAGB,
    IP_OZUseNSM,

    IP_OZFeHMin,
    IP_OZFeHMax,
    IP_OZFeHBin,
    IP_OZMassMin,
    IP_OZMassMax,
    IP_OZFracMin,
    IP_OZFracMax,
    IP_OZAgeMin,
    IP_OZAgeMax,
    IP_OZAlphaFeMin,
    IP_OZAlphaFeMax,

    IP_OZMstarMin,
    IP_OZMstarMax,
    IP_OZ12OHMin,
    IP_OZ12OHMax,

    IP_OZShowEuFe,
    IP_OZPlotTen,

    IP_OZPlotStep,


    IP_RunPlotIMF,
    IP_RunPlotLT,
    IP_RunPlotMDying,
    IP_RunWriteSNIIdata,
    IP_RunPlotSNIIYields,
    IP_RunPlotSNIIRate,
    IP_RunPlotSNIaYields,
    IP_RunPlotSNIaRate,
    IP_RunPlotAGBReturnMassFraction,
    IP_RunPlotAGBYields,
    IP_RunPlotNSMRate,
    IP_RunPlotLibrary,
    IP_RunPlotFB,
    IP_RunPlotOneZone,
    IP_RunPlotOneZoneMultiModels,
    IP_RunPlotOneZoneMultiModelsMassEvolution,
    IP_RunPlotOneZoneMultiModelsAGBComparison,
    IP_RunPlotOneZoneMultiModelsMetalDistribution,
    IP_RunPlotOneZoneMultiModelsCComparison,
    IP_RunPlotOneZoneMultiModelsNiCompasiron,
    IP_RunPlotOneZoneMultiModelsMgCompasiron,
    IP_RunPlotOneZoneMultiModelsEuComparison,
    IP_RunPlotStarbyStar,

    IP_Size,
};

/*!
 * Data type.
 */
enum{
    TypeInt,    //!< Integer type.
    TypeDouble, //!< Double type.
    TypeChar,   //!< Char type.
};

/*!
 * Definision of the structure for the parameter reading.
 */
struct StructParameters{
    int Type;                         //!< Type of data. This should be TypeInt, TypeDouble or TypeChar.
    void *Pointer;                    //!< Pointer to the variable which will be stored the parameter.
    char Label[MaxCharactersInLine];  //!< Label of the data.
    char Value[MaxCharactersInLine];  //!< Original data.
    int CallCount;                    //!< Total number of counts. If this does not have 1, this means something are incorrect in the parameter file.
};

#define SetParameter(_Tag,_Type,_Pointer,_Label) \
    Params[_Tag].Type = _Type; \
    Params[_Tag].Pointer = _Pointer; \
    snprintf(Params[_Tag].Label,MaxCharactersInLine,_Label); \
    Params[_Tag].CallCount = 0; 

 
/*!
 * The way of reading and setting data is defined in this function.
 */
static void SetParameterData(struct StructParameters Params[]){

    SetParameter(IP_NSample,TypeInt,&ThisRun.NSample,"NSample");
    SetParameter(IP_OutDir,TypeChar,ThisRun.OutDir,"OutDir");
    SetParameter(IP_PlotMode,TypeInt,&ThisRun.PlotMode,"PlotMode");
    SetParameter(IP_InitialMass,TypeDouble,&ThisRun.InitialMass,"InitialMass");
    SetParameter(IP_Metallicity,TypeDouble,&ThisRun.Metallicity,"Metallicity");

    SetParameter(IP_SNIIYieldType,TypeInt,&ThisRun.SNIIYieldType,"SNIIYieldType");
    SetParameter(IP_NSNIa,TypeInt,&ThisRun.NSNIa,"NSNIa");
    SetParameter(IP_NNSM,TypeInt,&ThisRun.NNSM,"NNSM");
    SetParameter(IP_PopIIIIMF,TypeInt,&ThisRun.PopIIIIMF,"PopIIIIMF");
    SetParameter(IP_PopIIISNe,TypeInt,&ThisRun.PopIIISNe,"PopIIISNe");
 
    SetParameter(IP_TStart,TypeDouble,&ThisRun.TEnd,"TStart");
    SetParameter(IP_TEnd,TypeDouble,&ThisRun.TEnd,"TEnd");
    SetParameter(IP_dt,TypeDouble,&ThisRun.dt,"dt");
 
    SetParameter(IP_SSPMassMin,TypeDouble,&ThisRun.SSPMassMin,"SSPMassMin");
    SetParameter(IP_SSPMassMax,TypeDouble,&ThisRun.SSPMassMax,"SSPMassMax");
    SetParameter(IP_SSPNMassBin,TypeInt,&ThisRun.SSPNMassBin,"SSPNMassBin");
    SetParameter(IP_SSPMetallicity,TypeDouble,&ThisRun.SSPMetallicity,"SSPMetallicity");
 
    SetParameter(IP_SSPZMin,TypeDouble,&ThisRun.SSPZMin,"SSPZMin");
    SetParameter(IP_SSPZMax,TypeDouble,&ThisRun.SSPZMax,"SSPZMax");
    SetParameter(IP_SSPNZBin,TypeInt,&ThisRun.SSPNZBin,"SSPNZBin");

    SetParameter(IP_LibTime,TypeDouble,&ThisRun.LibTime,"LibTime");
    SetParameter(IP_LibTimeStep,TypeDouble,&ThisRun.LibTimeStep,"LibTimeStep");
    SetParameter(IP_LibNBin,TypeInt,&ThisRun.LibNBin,"LibNBin");
    SetParameter(IP_LibType,TypeInt,&ThisRun.LibType,"LibType");
    SetParameter(IP_LibTimeMin,TypeDouble,&ThisRun.LibTimeMin,"LibTimeMin");
    SetParameter(IP_LibTimeMax,TypeDouble,&ThisRun.LibTimeMax,"LibTimeMax");
    SetParameter(IP_LibMetalicity_00,TypeDouble,ThisRun.LibMetallicity,"LibMetallicity_00");
    SetParameter(IP_LibMetalicity_01,TypeDouble,ThisRun.LibMetallicity+1,"LibMetallicity_01");

    SetParameter(IP_OZTime,TypeDouble,&ThisRun.OZTime,"OZTime");
    SetParameter(IP_OZMgas,TypeDouble,&ThisRun.OZMgas,"OZMgas");
    SetParameter(IP_OZFinalMstar,TypeDouble,&ThisRun.OZFinalMstar,"OZFinalMstar");
    SetParameter(IP_OZSFRType,TypeInt,&ThisRun.OZSFRType,"OZSFRType");
    SetParameter(IP_OZExpScale,TypeDouble,&ThisRun.OZExpScale,"OZExpScale");
    SetParameter(IP_OZTimeBin,TypeInt,&ThisRun.OZTimeBin,"OZTimeBin");
    SetParameter(IP_OZPlotType,TypeInt,&ThisRun.OZPlotType,"OZPlotType");
 
    SetParameter(IP_OZUseSNII,TypeInt,&ThisRun.OZUseSNII,"OZUseSNII");
    SetParameter(IP_OZUseSNIa,TypeInt,&ThisRun.OZUseSNIa,"OZUseSNIa");
    SetParameter(IP_OZUseAGB,TypeInt,&ThisRun.OZUseAGB,"OZUseAGB");
    SetParameter(IP_OZUseNSM,TypeInt,&ThisRun.OZUseNSM,"OZUseNSM");
 
    SetParameter(IP_OZFeHMin,TypeDouble,&ThisRun.OZFeHMin,"OZFeHMin");
    SetParameter(IP_OZFeHMax,TypeDouble,&ThisRun.OZFeHMax,"OZFeHMax");
    SetParameter(IP_OZFeHBin,TypeInt,&ThisRun.OZFeHBin,"OZFeHBin");
    SetParameter(IP_OZMassMin,TypeDouble,&ThisRun.OZMassMin,"OZMassMin");
    SetParameter(IP_OZMassMax,TypeDouble,&ThisRun.OZMassMax,"OZMassMax");
    SetParameter(IP_OZFracMin,TypeDouble,&ThisRun.OZFracMin,"OZFracMin");
    SetParameter(IP_OZFracMax,TypeDouble,&ThisRun.OZFracMax,"OZFracMax");
    SetParameter(IP_OZAgeMin,TypeDouble,&ThisRun.OZAgeMin,"OZAgeMin");
    SetParameter(IP_OZAgeMax,TypeDouble,&ThisRun.OZAgeMax,"OZAgeMax");
    SetParameter(IP_OZAlphaFeMin,TypeDouble,&ThisRun.OZAlphaFeMin,"OZAlphaFeMin");
    SetParameter(IP_OZAlphaFeMax,TypeDouble,&ThisRun.OZAlphaFeMax,"OZAlphaFeMax");
 
    SetParameter(IP_OZMstarMin,TypeDouble,&ThisRun.OZMstarMin,"OZMstarMin");
    SetParameter(IP_OZMstarMax,TypeDouble,&ThisRun.OZMstarMax,"OZMstarMax");
    SetParameter(IP_OZ12OHMin,TypeDouble,&ThisRun.OZ12OHMin,"OZ12OHMin");
    SetParameter(IP_OZ12OHMax,TypeDouble,&ThisRun.OZ12OHMax,"OZ12OHMax");

    SetParameter(IP_OZShowEuFe,TypeInt,&ThisRun.OZShowEuFe,"OZShowEuFe");
    SetParameter(IP_OZPlotTen,TypeInt,&ThisRun.OZPlotTen,"OZPlotTen");
 
    SetParameter(IP_OZPlotStep,TypeInt,&ThisRun.OZPlotStep,"OZPlotStep");

    SetParameter(IP_RunPlotIMF,TypeInt,&ThisRun.RunPlotIMF,"RunPlotIMF");
    SetParameter(IP_RunPlotLT,TypeInt,&ThisRun.RunPlotLT,"RunPlotLT");
    SetParameter(IP_RunPlotMDying,TypeInt,&ThisRun.RunPlotMDying,"RunPlotMDying");
    SetParameter(IP_RunWriteSNIIdata,TypeInt,&ThisRun.RunWriteSNIIdata,"RunWriteSNIIdata");
    SetParameter(IP_RunPlotSNIIYields,TypeInt,&ThisRun.RunPlotSNIIYields,"RunPlotSNIIYields");
    SetParameter(IP_RunPlotSNIIRate,TypeInt,&ThisRun.RunPlotSNIIRate,"RunPlotSNIIRate");
    SetParameter(IP_RunPlotSNIaYields,TypeInt,&ThisRun.RunPlotSNIaYields,"RunPlotSNIaYields");
    SetParameter(IP_RunPlotSNIaRate,TypeInt,&ThisRun.RunPlotSNIaRate,"RunPlotSNIaRate");
    SetParameter(IP_RunPlotAGBReturnMassFraction,TypeInt,
            &ThisRun.RunPlotAGBReturnMassFraction,"RunPlotAGBReturnMassFraction");
    SetParameter(IP_RunPlotAGBYields,TypeInt,&ThisRun.RunPlotAGBYields,"RunPlotAGBYields");
    SetParameter(IP_RunPlotNSMRate,TypeInt,&ThisRun.RunPlotNSMRate,"RunPlotNSMRate");
    SetParameter(IP_RunPlotLibrary,TypeInt,&ThisRun.RunPlotLibrary,"RunPlotLibrary");
    SetParameter(IP_RunPlotFB,TypeInt,&ThisRun.RunPlotFB,"RunPlotFB");

    SetParameter(IP_RunPlotOneZone,TypeInt,&ThisRun.RunPlotOneZone,"RunPlotOneZone");
    SetParameter(IP_RunPlotOneZoneMultiModels,TypeInt,&ThisRun.RunPlotOneZoneMultiModels,"RunPlotOneZoneMultiModels");
    SetParameter(IP_RunPlotOneZoneMultiModelsMassEvolution,TypeInt,
            &ThisRun.RunPlotOneZoneMultiModelsMassEvolution,"RunPlotOneZoneMultiModelsMassEvolution");
    SetParameter(IP_RunPlotOneZoneMultiModelsAGBComparison,TypeInt,
            &ThisRun.RunPlotOneZoneMultiModelsAGBComparison,"RunPlotOneZoneMultiModelsAGBComparison");
    SetParameter(IP_RunPlotOneZoneMultiModelsMetalDistribution,TypeInt,
            &ThisRun.RunPlotOneZoneMultiModelsMetalDistribution,"RunPlotOneZoneMultiModelsMetalDistribution");
    SetParameter(IP_RunPlotOneZoneMultiModelsCComparison,TypeInt,
            &ThisRun.RunPlotOneZoneMultiModelsCComparison,"RunPlotOneZoneMultiModelsCComparison");
    SetParameter(IP_RunPlotOneZoneMultiModelsNiCompasiron,TypeInt,
            &ThisRun.RunPlotOneZoneMultiModelsNiCompasiron,"RunPlotOneZoneMultiModelsNiCompasiron");
    SetParameter(IP_RunPlotOneZoneMultiModelsMgCompasiron,TypeInt,
            &ThisRun.RunPlotOneZoneMultiModelsMgCompasiron,"RunPlotOneZoneMultiModelsMgCompasiron");
    SetParameter(IP_RunPlotOneZoneMultiModelsEuComparison,TypeInt,
            &ThisRun.RunPlotOneZoneMultiModelsEuComparison,"RunPlotOneZoneMultiModelsEuComparison");

    SetParameter(IP_RunPlotStarbyStar,TypeInt,&ThisRun.RunPlotStarbyStar,"RunPlotStarbyStar");
    //dprint(IP_Size);
    //dprint(IP_RunPlotOneZoneMultiModelsMassEvolution);
    return ;
}

static void ReadAndSetParameters(struct StructParameters Params[], char FileName[]){

    // Read the parameter file.
    FILE *fp;
    char Line[MaxCharactersInLine];
    int countline = 0;
    FileOpen(fp,FileName, "r");
    while(fgets(Line, MaxCharactersInLine, fp) != NULL){
        countline ++;

        // Ignore unnecessary lines.
        if(Line[0] == '#') continue;
        if(Line[0] == '%') continue;
        if(Line[0] == '\n') continue;
        if(Line[0] == ' ') continue;

        // Data
        char Data1[MaxCharactersInLine];
        char Data2[MaxCharactersInLine];
        char Data3[MaxCharactersInLine];
        sscanf(Line,"%s %s %s",Data1,Data2,Data3);

        // Convert and set data
        for(int i=0;i<IP_Size;i++){
            if(strcmp(Params[i].Label,Data1) == 0){
                if(Params[i].Type == TypeInt){
                    *((int *)(Params[i].Pointer)) = atoi(Data2);
                    Params[i].CallCount ++;
                }
                if(Params[i].Type == TypeDouble){
                    *((double *)(Params[i].Pointer)) = atof(Data2);
                    Params[i].CallCount ++;
                }
                if(Params[i].Type == TypeChar){
                    snprintf(Params[i].Pointer,MaxCharactersInLine,"%s",Data2);
                    Params[i].CallCount ++;
                }
                strcpy(Params[i].Value,Data2);
            }
        }
    }
    fclose(fp);

    // Check incorrect data.
    int IncorrectData = 0;
    for(int i=0;i<IP_Size;i++){
        if(Params[i].CallCount == 0){
            fprintf(stderr,"%s is not defined.\n",Params[i].Label);
            IncorrectData ++;
        }
        if(Params[i].CallCount > 1){
            fprintf(stderr,"%s is muliply-detected.\n",Params[i].Label);
            IncorrectData ++;
        }
    }
    if(IncorrectData > 0){
        fprintf(stderr,"Parameter file has problems.\n");
        fflush(NULL);
        exit(EXIT_FAILURE);
    }

    return ;
}

/*!
 * This function updates parameteres based on dependencies. 
 */
static void UpdateParameters(void){
    
    return ;
}

/*!
 * This function writes all parameters in a file "parameter-log.txt" in the
 * output directory defined "OutDir".
 */
static void WriteParameters(struct StructParameters Param[]){

    MakeDir(ThisRun.OutDir);

    FILE *fp;
    char FileName[MaxCharactersInLine];
    Snprintf(FileName,"%s/parameter-log.txt",ThisRun.OutDir);
    FileOpen(fp,FileName,"w");
    for(int i=0;i<IP_Size;i++){
        fprintf(fp,"%-30s %s\n",Param[i].Label,Param[i].Value);
    }
    fclose(fp);

    return ;
}

/*!
 * This function reads a parameter file of which name is defined in FileName and
 * then sets parameters.
 */
void ReadParameters(char FileName[]){

    struct StructParameters Params[IP_Size];
    memset(Params,0,sizeof(struct StructParameters)*IP_Size);

    // Set input data styles.
    SetParameterData(Params);

    // Read and set parameters.
    ReadAndSetParameters(Params,FileName);

    // Update parameters if necessary.
    UpdateParameters();

    // Write log.
    WriteParameters(Params);

    return ;
}
