#include "config.h"
#include <sys/stat.h>

bool CheckFile(const char FileName[]){
    FILE *fp;
    fp = fopen(FileName,"r");
    if(fp == NULL){
        return false;
    }
    fclose(fp);
    return true;
}

bool CheckDir(const char DirName[]){
    FILE *fp;
    fp = fopen(DirName,"r");
    if(fp == NULL){
        return false;
    }
    fclose(fp);
    return true;
}

void MakeDir(const char DirName[]){

    if(CheckDir(DirName) == false){
        int checkflag = mkdir(DirName,0755);
        if(checkflag < 0){
            fprintf(stderr,"Directory [%s] creation error.\n",DirName);
            assert(checkflag>=0);
            //exit(EXIT_FAILURE);
        } else {
            fprintf(stderr,"Directory [%s] is created.\n",DirName);
        }
    }

    return ;
}
