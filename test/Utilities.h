#ifndef	__CELIB_UTILITIES_H_INCLUDED__
#define	__CELIB_UTILITIES_H_INCLUDED__

_Bool CheckFile(const char FileName[]);
_Bool CheckDir(const char DirName[]);
void MakeDir(const char DirName[]);

#endif // __CELIB_UTILITIES_H_INCLUDED__
