#include "config.h"
/*! \file Vars.c
 * \brief Structures used in the simulation is defined.
 */

struct StructThisRun ThisRun;
gsl_rng *RandomGenerator;

