#pragma once 

/*! @struct StructThisRun
 * A structure which contains all of run status.
 */
extern struct StructThisRun{

    char OutDir[MaxCharactersInLine];   //!< Data output directory.

    int PlotMode; //!< 0,1,2 mean that the screen is xserve, eps, and png, repsectively.

    int NSample; //!< Number of samples in the run. 
    double InitialMass; //!< InitialMass of a SSP particle.
    double Metallicity; //!< InitialMass of a SSP particle.

    int SNIIYieldType; //!< Type of SNII yield table.

    int NSNIa; //!< Size of SNIa assocication.
    int NNSM;  //!< Size of NSM assocication.

    int PopIIIIMF;  //!< Use PopIII IMF for PopIII SNe II
    int PopIIISNe;  //!< PopIII yield for SNe II

    double TStart; //!< Simulation start time.
    double TEnd;   //!< Simulation end time.
    double dt;   //!< Simulation timestep


    // For SNII 
    double SSPMassMin; //!< Minimum mass of SSP particle.
    double SSPMassMax; //!< Maximum mass of SSP particle.
    int SSPNMassBin;   //!< Mass bin for SNII test.
    double SSPMetallicity; //!< Metallicity of SSP particle.

    double SSPZMin; //!< Minimum metallicity of SSP particle.
    double SSPZMax; //!< Maximum metallicity of SSP particle.
    int SSPNZBin;   //!< Metallicity bin for SNII test.

    // For Library
    double LibTime; //!<
    double LibTimeStep; //!<
    int LibNBin; //!<
    int LibType; //!<
    double LibTimeMin; //!<
    double LibTimeMax; //!<
    double LibMetallicity[2]; //!<


    // For OneZone
    double OZTime; //!<
    double OZMgas; //!<
    double OZFinalMstar; //!<
    int OZSFRType; //!<
    double OZExpScale; //!<
    int OZTimeBin; //!<
    int OZPlotType; //!<

    int OZUseSNII; //!<
    int OZUseSNIa; //!<
    int OZUseAGB; //!<
    int OZUseNSM; //!<

    double OZFeHMin; //!<
    double OZFeHMax; //!<
    int OZFeHBin; //!<
    double OZMassMin; //!<
    double OZMassMax; //!<
    double OZFracMin; //!<
    double OZFracMax; //!<
    double OZAgeMin; //!<
    double OZAgeMax; //!<
    double OZAlphaFeMin; //!<
    double OZAlphaFeMax; //!<

    double OZMstarMin; //!<
    double OZMstarMax; //!<
    double OZ12OHMin; //!<
    double OZ12OHMax; //!<

    int OZShowEuFe; //!<
    int OZPlotTen; //!<

    int OZPlotStep; //!<
    
    int RunPlotIMF; //!<
    int RunPlotMDying; //!<
    int RunPlotLT; //!<
    int RunWriteSNIIdata; //!<
    int RunPlotSNIIYields; //!<
    int RunPlotSNIIRate; //!<
    int RunPlotSNIaYields; //!<
    int RunPlotSNIaRate; //!<
    int RunPlotAGBReturnMassFraction; //!<
    int RunPlotAGBYields; //!<
    int RunPlotNSMRate; //!<
    int RunPlotLibrary; //!<
    int RunPlotFB; //!<
    int RunPlotOneZone; //!<
    int RunPlotOneZoneMultiModels; //!<
    int RunPlotOneZoneMultiModelsMassEvolution; //!<
    int RunPlotOneZoneMultiModelsAGBComparison; //!<
    int RunPlotOneZoneMultiModelsMetalDistribution; //!<
    int RunPlotOneZoneMultiModelsCComparison; //!<
    int RunPlotOneZoneMultiModelsNiCompasiron; //!<
    int RunPlotOneZoneMultiModelsMgCompasiron; //!<
    int RunPlotOneZoneMultiModelsEuComparison; //!<
    int RunPlotStarbyStar; //!<

} ThisRun;


extern gsl_rng *RandomGenerator;

