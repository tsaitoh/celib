#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <assert.h>
#include "../src/Astro.h"
#include "../src/CELib.h" 
#include "../src/Utilities.h"
#include "ReadParameters.h"
#include <gsl/gsl_rng.h>
#include "Vars.h"
#include "palett.h"


