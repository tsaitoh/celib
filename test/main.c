#include "config.h"
#include "ReadParameters.h"
#include "RandomNumberGenerator.h"
#include "PlotIMFs.h"
#include "PlotLT.h"
#include "PlotSNII.h"
#include "PlotSNIa.h"
#include "PlotAGB.h"
#include "PlotFB.h"
#include "PlotNSM.h"
#include "PlotLibrary.h"
#include "PlotStarbyStar.h"
#include "OneZone.h"

static void Plots(void){

    if(ThisRun.RunPlotIMF){
        PlotIMFs();
    }

    if(ThisRun.RunPlotLT){
        PlotLT();
    }

    if(ThisRun.RunPlotMDying){
        PlotMDying();
    }

    if(ThisRun.RunWriteSNIIdata){
        CalcSNIIdata();
    }

    if(ThisRun.RunPlotSNIIYields){
        PlotSNIIYields();
    }

    if(ThisRun.RunPlotSNIIRate){
        PlotSNIIRate();
    }

    if(ThisRun.RunPlotSNIaYields){
        PlotSNIaYields();
    }

    if(ThisRun.RunPlotSNIaRate){
        PlotCumulativeSNIaRate();
        PlotCumulativeSNIaRateWithV13();
    }

    if(ThisRun.RunPlotAGBReturnMassFraction){
        PlotAGBReturnMassFraction();
    }

    if(ThisRun.RunPlotAGBYields){
        PlotAGBIntegratedYields();
    }

    if(ThisRun.RunPlotNSMRate){
        PlotNSMRate();
    }

    if(ThisRun.RunPlotLibrary){
        PlotLibrary();
    }

    if(ThisRun.RunPlotFB){
        PlotFB();
    }

    if(ThisRun.RunPlotOneZone){
        OneZoneModel();
    }

    if(ThisRun.RunPlotOneZoneMultiModels){
        OneZoneModelMultiModels();
    }

    if(ThisRun.RunPlotOneZoneMultiModelsMassEvolution){
        OneZoneModelMultiModelsMassEvolution();
    }

    if(ThisRun.RunPlotOneZoneMultiModelsMetalDistribution){
        OneZoneModelMultiModelsMetalDistribution();
    }

    if(ThisRun.RunPlotOneZoneMultiModelsAGBComparison){
        OneZoneModelMultiModelsAGBComparison();
    }

    if(ThisRun.RunPlotOneZoneMultiModelsCComparison){
        OneZoneModelMultiModelsCComparison();
    }

    if(ThisRun.RunPlotOneZoneMultiModelsNiCompasiron){
        OneZoneModelMultiModelsNiComparison();
    }

    if(ThisRun.RunPlotOneZoneMultiModelsMgCompasiron){
        OneZoneModelMultiModelsMgComparison();
    }

    if(ThisRun.RunPlotOneZoneMultiModelsEuComparison){
        OneZoneModelMultiModelsEuComparison();
    }

    if(ThisRun.RunPlotStarbyStar){
        PlotStarbyStar();
    }

    return ;
}


int main(int argc, char *argv[]){

    // Read and set parameters.
    if(argc > 1){
        ReadParameters(argv[1]);
    } else {
        ReadParameters("./param.txt");
    }

    CELibShowVersion();
    CELibShowCurrentStatus();

    CELibSetRunParameterTestMode(false);
    CELibSetRunParameterIMFType(CELibIMF_Chabrier);
    CELibSetRunParameterSNIIYieldsTableID(ThisRun.SNIIYieldType);
    CELibSetRunParameterSNIaType(CELibSNIaRateModelID_PowerLaw);
    CELibSetRunParameterSNIaYieldsTableID(CELibSNIaYieldsTableID_S13);
    CELibSetRunParameterSNIaYieldsTableModelID(CELibSNIaYieldsTableModelID_S13_N100);
    CELibSetRunParameterSNIaNassociation(ThisRun.NSNIa);
    CELibSetRunParameterAGBYieldsTableID(CELibAGBYieldsTableID_K10D14);
    CELibSetRunParameterPopIIISNe(ThisRun.PopIIISNe);
    CELibRunParameters.PopIIILifeTime = 1;
    CELibSetRunParameterLifeTimeType(CELibLifeTime_LSF);
    // CELibRunParameters.PopIIILifeTime = 0;
    // CELibSetRunParameterLifeTimeType(CELibLifeTime_Original);

    // CELibRunParameters.IntegrationSteps = 100;


    CELibShowCurrentStatus();
    CELibInit();
    CELibShowCurrentStatus();

    Plots();

    return EXIT_SUCCESS;
}
